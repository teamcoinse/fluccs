
use Switch;

#find source through ant
sub find_source{
	my $result = shift @_;
	my @result = split /\n/, $result;
	my $source_dir = undef;

	foreach my $line (@result){
		if ($line =~ m/\[echo\]\s+/){
			$source_dir = $';
			chomp $source_dir;
			last;
		}
	}
	
	return $source_dir;
}

#return the prefix of a given project
sub getPrefix{
	@_ == 1 or die $ARG_ERROR;
	my $pid = shift @_;
	my $prefix = undef;
	switch ($pid) {
		case "Lang" { 
			$prefix = "org.apache.commons.lang";
		}
		case "Math" {
			$prefix = "org.apache.commons.math";
		}
		case "Time" {
			$prefix = "org.joda.time";
		}
		case "Closure" {
			$prefix = "com.google";
		}
		case "Chart" {
			$prefix = "org.jfree.chart";
		}
		case "Mockito" {
			$prefix = "org.mockito";
		}
	}
	return $prefix;
}

sub srcdir_from_ant{
        @_ == 1 or die $ARG_ERROR;
        my $pid = shift @_;
	my $cmd = undef;
	my $source_dir = undef;
        switch ($pid){
        	case "Lang" { 
			$cmd = "ant get-source-home";
		 	my $result = `$cmd`;
		 	$source_dir = find_source($result);
		 	return $source_dir;
		}
                case "Math" {
			$cmd = "ant get-source-home";
                 	 my $result = `$cmd`;
	 	  	print "result $result\n";
                  	$source_dir = find_source($result);
                  	return $source_dir;
		}
                case "Time" {
                	$cmd = "ant get-source-home";
                 	my $result = `$cmd`;
                	$source_dir = find_source($result);
                	return $source_dir;
		}
                case "Chart" { 
                	$cmd = "ant get-source-home";
                	my $result = `$cmd`;
                	$source_dir = find_source($result);
                	return $source_dir;
		}
                case "Closure" {
                	$cmd = "ant get-source-home";
                	my $result = `$cmd`;
                	$source_dir = find_source($result);
                	return $source_dir;
		}
		case "Mockito" {
			$cmd = "ant get-source-home";
			my $result = `$cmd`;
			$source_dir = find_source($result);
			return $source_dir;
		}
        }

        die "wrong pid $pid\n";
}
1;
