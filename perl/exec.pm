#Subroutine(function) for executing a given command

sub _execute{
	@_ == 2 or die "wrong number of arguments";
	my ($cmd, $which) = @_;
        print $cmd."\n";
	my $out = undef;
	if ($which eq "gp" ){
		$out = `$cmd`;
		print $out;
	}
	else{
		if ($which eq "gather"){
			$out = `$cmd`;
		}
		else{
			$out = qx($cmd 2>&1);
		}
		print $out;
		my $ret = $?;
		if ($ret == -1){ die("failed to execute $cmd $out"); }
	}
}

1;
