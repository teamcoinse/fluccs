
use strict;
use warnings;
use File::Spec;
my $ARG_ERROR = "wrong number of arguments";
my $checkout_list_dir = File::Spec->catfile($ENV{'D4J_HOME'}, "framework/bin/fluccs/checkout");

#Preprocess(i.e. moving xml file to prevent them from modified after checkout) and Checkout to buggy version
sub Checkout{
	@_ == 4 or die $ARG_ERROR;
	my ($pid, $bid, $checkout_ver, $work_dir) = @_;
	my $cmd = undef;

	my $checkoutDir = File::Spec->catfile($ENV{'D4J_HOME'}, "framework/bin/fluccs/checkout");
	$cmd = "$checkoutDir/prep.before.modified.sh";
	`$cmd`;
	checkout($checkout_ver, $work_dir, $pid);
	$cmd = "$checkoutDir/prep.after.modified.sh";
	`$cmd`;		
}

#Return file list that are needed to be moved
sub get_file_list{
	@_ == 2 or die $ARG_ERROR;
	my ($pid, $bid) = @_;
	my @file_list = ();
	my $handler = undef;
	my $target_file = File::Spec->catfile($checkout_list_dir, "checkout.list");

	open($handler, "<", $target_file) or die "CANNOT OPEN FILE $target_file";
	while(my $line = <$handler>){
		chomp $line;
		@file_list = (@file_list, $line);
	}
	close($handler);

	return @file_list;	
}

#checkout to the given version on the given workdir
sub checkout{
	@_ == 3 or die $ARG_ERROR;
	my ($revision, $work_dir, $pid) = @_;
	if ($pid eq 'Chart'){
		`rm -Rf $work_dir`;
		my $svn_dir = File::Spec->catfile($ENV{'D4J_HOME'}, "framework/bin/fluccs/checkout/Chart/svnChart");
		`cp -Rf $svn_dir $work_dir`; 
	}

	chdir($work_dir);
	my $cmd = "git checkout -f $revision";
	my $ret = exec_cmd($cmd);
	if ($ret == 0){
		print "fail to checkout..\n";
	}
	
	return 1;
}

#execute command
sub exec_cmd{
        @_ == 1 or die $ARG_ERROR;
        my $cmd = shift @_;
        my $temp =`$cmd`;
        my $ret = $?>>8;
        #handling for grep command : case no line were selected
        if ($cmd =~ m/grep/ and $ret == 1){
                return 2;
        }
        unless (!$ret){#ret == 1 or == 2(for grep)
                return 0;
        }
        return 1;
}

