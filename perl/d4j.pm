use File::Spec;

#return revision ids for buggy and fixed version
#if $pid is chart(jfreechart), then should return matching revision id for git
sub get_revision{
	@_ == 3 or "Wrong parameters: @_. need values for pid, bid and a current working directory";
	my ($pid, $bid, $work_dir)= @_;
	#setting directory
	my $base_dir = $ENV{'D4J_HOME'};
	my $commit_db="$base_dir/framework/projects/$pid/commit-db";

	#find revision of bug
	my $commit_line=`grep "^$bid," $commit_db` or die "Can't execute grep for commit-db";
	my $bug_fix_vers, $bug_ver, $fix_ver;

	if ($commit_line =~ m/,/){
        	$bug_fix_vers=$';
	}
	else{
        	print "matched version for the bug $bid is not in commit db";
        	exit 1;
	}
	#parse and get bug and fix commit version
	if ($bug_fix_vers =~ m/,/){
        	$bug_ver=$`;
        	$fix_ver=$';
        	chomp $fix_ver;
	}
	else{
        	print "error during retrieving bug version from commit db";
        	exit 1;
	}

	#if the $pid is 'Chart'(svn), should change it to a matching git revision id
	if ($pid eq 'Chart'){
		$bug_ver = find_matched_gitRev($bug_ver, $work_dir);
		
	}

	return ($bug_ver, $fix_ver);

}

#This function is called only if a current target project is 'jfreechart'
#Look into the $svn_dir-a directory for svn version of jfreechart- and if there is no $svn_dir, then make it.
sub find_matched_gitRev{
	@_ == 2 or die "Wrong parameters: @_. need values for svn revision id and a current working directory";
	my ($svn_rev, $work_dir) = @_;
	my $cmd = undef;
	my $svn_dir = File::Spec->catfile($ENV{'D4J_HOME'}, "framework/bin/fluccs/checkout/Chart/svnChart");

	my $svn_url = "$ENV{'D4J_HOME'}/";
	my $current_dir = getcwd();
	#if $svn_dir does not exists, create one
	if (not -d $svn_dir){
		`mkdir $svn_dir; cd $svn_dir; git init`;
		$svn_url = get_svn_url($work_dir);		
		`git svn init $svn_url --stdlayout --prefix=svn/`;
		`git svn fetch`;
	}

	#go to $svn_dir and find a matched git revision id for a given $svn_rev
	chdir($svn_dir);
	$cmd = "git svn find-rev r$svn_rev";
	my $git_rev = undef;
	my $result = `$cmd`;
	if (length($result) == 0){
		print "There is no matching git rev id for svn rev id $svn_rev";
		exit 1;
	}
	chomp $result;
	chdir($current_dir);
	return $result;
}

#get svn url for a given directory
sub get_svn_url{
	my $work_dir = shift @_;
	my $svn_url = `svn info $work_dir --show-item repos-root-url`;
	chomp $svn_url;
	return $svn_url;
}


sub write_config{
	my($pid, $bid) = @_;
	my $handler;
	open($handler,">",".defects4j.config") or die "CANNOT OPEN FILE";
	print $handler "pid=".$pid."\n";
	print $handler "vid=".$bid."b";
	close($handler);
}

1;
