use File::Spec;

#return the class list of project 
#if $base_dir/classes.list file exists, read from this file.
sub class_list{
	my ($src_home, $base_dir) = @_;
	my @classes = undef;
	my $handler = undef;
	if (-e File::Spec->catfile($base_dir, "classes.list")){
		@classes = ();
		$handler = undef;
		open($handler, "<", File::Spec->catfile($base_dir, "classes.list"))
			or die "CANNOT OPEN FILE";
		while(my $class=<$handler>){
			chomp $class;
			push @classes, $class;
		}
		close($handler);
	}
	else{
		my $cmd="find $src_home -type f -name \\*.java";
	        my $result=`$cmd`;
        	my @classes= split /\n/, $result;
		open($handler, ">", File::Spec->catfile($base_dir, "classes.list"));
	        foreach my $index (0 .. $#classes){
        	        $classes[$index] =~ /$src_home\//;
                	my $class= $';
	                $class =~ /\.java/;
        	        $class=$`;
                	$class =~ s/\//\./g;
	                $classes[$index]=$class;
			print $handler "$class\n";
       		}
		close($handler);
	}
        return @classes;
}

1;
