#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------

use strict;
use warnings;
use Cwd;
use Getopt::Long;
use File::Spec;
use FindBin;
use lib "$FindBin::Bin/..";
use perl::write_target_prop;

my ($pid, $base_dir, $use_cobertura);
GetOptions ( "p=s" => \$pid,
        "w=s" => \$base_dir,
	"c=s" => \$use_cobertura) 
or die ("Error in command line arguments\n");

my $current_dir = getcwd();#current working directory
chdir($base_dir);

my $handler = undef;
my $test_class = undef;
open ($handler,"<","testclasses_list.txt");
$test_class = <$handler>;
chomp $test_class;
close($handler);

my $test_case = undef;
my @test_cases = ();
open($handler,"<","testcase_list_$test_class.txt");
$test_case = <$handler>;
chomp $test_case;
push @test_cases, $test_case;
close($handler);

my $classlst_file = undef;

if ($use_cobertura){
	$classlst_file = ensure_class_lst_exist("classes.list", $base_dir);
}
else { $use_cobertura = 0;}

write_random_test_result($pid, $test_class, $use_cobertura, $classlst_file, @test_cases);

#go back to original directory
chdir($current_dir);

#ensure the existence of a class list file: check and generate it if not 
sub ensure_class_lst_exist{
	my ($file_name, $work_dir) = @_;
	if (not (-e File::Spec->catfile($work_dir, $file_name))){
		my $python_dir = File::Spec->catfile($ENV{'D4J_HOME'}, "framework/bin/fluccs/python");
		my $cmd = "python $python_dir/write_classes.py -workdir $work_dir";
		my $ret = `$cmd`;
		if (!$ret) { die("failed to execute $cmd");}
	}
	return File::Spec->catfile($work_dir, $file_name); 
}

#tests the all testcases for the test class
#the result will be reflected right after the test
#inputs are project id, test class, and testcases list which contained all testcases of the test class
sub write_random_test_result{
	my ($pid, $test_class, $use_cobertura, $classlst_file, @test_cases) = @_;
	my $cmd = undef;
	my $handler = undef;
	my $log = undef;

	foreach my $test_case (@test_cases){
		if ($use_cobertura){
			$cmd = "defects4j-fluccs 0 coverage -t ".$test_class."::".$test_case." -i $classlst_file -R 1 -M 0 -P";
			$log = `$cmd`;
			$cmd = "mv coverage.xml test_classes/$test_class-$test_case.xml";
			$log = `$cmd`;
		}
		else{
			write_target($base_dir, $test_class,$test_case);
			$cmd = "ant jacoco.coverage.filter";
			$log = `$cmd`;
			$cmd="ant jacoco.report";
			$log = `$cmd`;
		}
	}

   return;
}

