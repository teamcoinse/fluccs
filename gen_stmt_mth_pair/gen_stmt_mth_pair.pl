#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------

use strict;
use warnings;
use Cwd;
use File::Spec;
use Getopt::Long;
use FindBin;
use lib "$FindBin::Bin/..";
use perl::extract_class;
use perl::extracting_mth_stmt;
use perl::project;

#Extracing statement info with method 
my ($pid, $bid, $base_dir, $result_dir, $use_cobertura);
GetOptions ( "p=s" => \$pid, 
	     "b=s" => \$bid,
	     "w=s" => \$base_dir,
	     "r=s" => \$result_dir,
	     "c=s" => \$use_cobertura) 
or die("Error in command line arguments\n");

my $current_dir = getcwd();#current working directory
chdir($base_dir);

my $src_dir = srcdir_from_ant($pid);
if (index($src_dir, $base_dir) == -1){
	$src_dir = $base_dir."/".$src_dir;
}

my $tc_coverage_home = File::Spec->catfile($base_dir, 'test_classes');
my @classes = class_list($src_dir, $base_dir);
my $coverage_file = get_random_covg_file();
my %stmt_infos_hash;
if ($use_cobertura){
	%stmt_infos_hash = extract_mth_stmt_cobertura($coverage_file, @classes);
}
else{
	%stmt_infos_hash = extract_mth_stmt($coverage_file, @classes);
}
write_stmt_mth();

#go back to original directory
chdir($current_dir);

sub get_random_covg_file{
	opendir(DIR, $tc_coverage_home) or die $!;
	my $file = undef;
	while ($file = readdir(DIR)){
		if ($file =~ /.+\.xml$/){
			last;
		}
	}
	closedir(DIR);
	my $random_covg_file = "$tc_coverage_home/".$file;
	return $random_covg_file;
}

sub write_stmt_mth{
	my $handler;
	#my $stmt_mth_result = "$base_dir/output/stmt_mth_".$pid."_".$bid.".csv";
	my $stmt_mth_result = File::Spec->catfile($result_dir, "$pid/stmt_mth_".$pid."_".$bid.".csv");
	open($handler, ">", $stmt_mth_result);
	foreach my $class (keys %stmt_infos_hash){
		foreach my $stmt (@{$stmt_infos_hash{$class}}){
			print $handler "$class,".$stmt."\n";
		}
	}
	close($handler);
}
