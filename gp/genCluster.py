import os, sys
from sklearn.cluster import KMeans, SpectralClustering

def genKMeansClustering(datas, k):
	"""
	"""
	print "In K-means clustering"
	print datas.shape
	#predict cluster index for each data
	kmeans = KMeans(n_clusters = k, init = 'k-means++').fit(datas)

	return kmeans
