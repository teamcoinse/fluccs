import os, sys, csv


#default feature set
features = ["ochiai", "jaccard", "gp13", "wong1", "wong2", "wong3", "tarantula", "ample",\
	"RussellRao", "SorensenDice", "Kulczynski1", "SimpleMatching", "M1", "RogersTanimoto",\
	"Hamming", "Ochiai2", "Hamann", "dice", "Kulczynski2", "Sokal", "M2", "Goodman",\
	"Euclid", "Anderberg", "Zoltar", "ER1a", "ER1b", "ER5a", "ER5b", "ER5c", "gp02", "gp03",\
	"gp19", "churn", "max_age", "min_age", "num_args", "num_vars", "b_length", "loc"]

cudaFileDir = os.path.dirname(os.path.abspath(__file__))

#write function declaration part with function name funcName
def writeDeclaration(funcName):
        declarationLines = []
	declarationLine = "__global__ void " + str(funcName) + "\n"
        declarationLines.append(declarationLine)
	return declarationLines


#write parameter part of the evaluation function
def writeParameters(parameters):
	parameterLines = ["(\n"]

	for (typeName, parameterName) in parameters:
		parameterLines.append(typeName + " " + parameterName + "," + "\n")

	parameterLines.append("float* susp")
	parameterLines.append(")\n")

	return parameterLines


#write body part of the indivNumth evaluation function
#indivNumth should be integer
def writeBody(indivNumth):
	indivId = "1234"
	if (indivNumth < 10):#1-9
		indivId += "0" + str(indivNumth)
	else:
		indivId += str(indivNumth)

	bodyLines = ["{\n"]

	tidAssignment = "const int tid = blockIdx.x * blockDim.x + threadIdx.x;\n"
	bodyLines.append(tidAssignment)
	suspAssignment = "susp[tid] = -(" + indivId + ");\n"
	bodyLines.append(suspAssignment)

	bodyLines.append("}\n")

	return bodyLines


#write evalution function for indivNumth individual
#indivNumth should be between 1 and (population size + best indivs)
def writeEvaluate(indivNumth, parameters):
	lines = []
	funcName = "evaluate_" + str(indivNumth)
	lines = writeDeclaration(funcName)
	lines.extend(writeParameters(parameters))
	lines.extend(writeBody(indivNumth))

	return lines

#ParameterNames is a list of parameter names
def getParameters(ParameterNames):
	parameters = zip(["float*"] * len(ParameterNames), ParameterNames)
	return parameters


def getBasicFunctions(fileName = None):
        global cudaFileDir
        if (fileName is None):
                fileName = os.path.join(cudaFileDir, "kernel_ops.cuda")
	lines = open(fileName).readlines()
	return lines


#write a kernel file using ParameterNames as function parameters
#numOfIndividuals is a number of evaluation function to be written, which is same with
#the inidividuals that are compiled together
def writeKernel(ParameterNames, numOfIndividuals, kernelFileName, dest):
        global cudaFileDir
	if (dest not in kernelFileName):
		kernelFileName = os.path.join(dest, kernelFileName)
	handler = open(kernelFileName, "w")

	lines = getBasicFunctions()
	parameters = getParameters(ParameterNames)

	for indivNumth in range(numOfIndividuals):
		lines.extend(writeEvaluate(indivNumth + 1, parameters))
                lines.append("\n");

        for line in lines:
                handler.write(line)

	handler.close()





