import numpy as np
import random
import re

def extractSpecificPart(dataArr, labels, target_label):
	"""
	extract the parts of *dataArr* whose label is same with *target_label*
	"""
	extracted = []
	for (adata, label) in zip(dataArr, labels):
		if (label == target_label):
			extracted.append(adata)
		
	extracted_arr = np.asarray(extracted)
	return extracted_arr


def divideBasedOnLabel(dataFileNameLst, labels):
	"""
	divide data set(dataFileNameLst) based on a given *labels*
	"""
	unique_labels = list(set(labels))
	divided_data_set = {label:[] for label in unique_labels}
	
	for (dataFileName, label) in zip(dataFileNameLst, labels):
		divided_data_set[label].append(dataFileName)

	return divided_data_set


def divideIntoMajorMinor(dataFileNameLst, labels):
	"""
	divide data files in dataFileNameLst into minor (label 0) and major (label 1) set
	"""
	divided_data_set = divideBasedOnLabel(dataFileNameLst, labels)
	majorLst = divided_data_set[1]
	minorLst = divided_data_set[0]

	return (majorLst, minorLst)


def divideIntoTrainingTesting(dataFileSet, ratio = 0.8):
	"""
	divide data files in dataFileSet into two set using ratio
	"""
	
	numOfTotalDataFiles = len(dataFileSet)
	numOfTrainingDataFiles = np.int32(np.floor(len(dataFileSet) * ratio))
	training_set = random.sample(dataFileSet, numOfTrainingDataFiles)
	test_set = list(set(dataFileSet) - set(training_set))
	return (training_set, test_set)


def divideForCrossProject(dataFileSet, projects = None):
	"""
	divide data files in *dataFileSet* into cross-project (one project for training and others for test)
	"""
	if (projects is None):
		projects = ["Lang", "Math", "Time", "Closure", "Chart", "Mockito"]

	cross_project_datafile_set = {project:[[],[]] for project in projects}

	for datafile in dataFileSet:
		match = re.search("([^\/]+)_[0-9]*.dat", datafile)
		if (bool(match)):
			in_this_project = match.group(1)
			#training
			cross_project_datafile_set[in_this_project][0].append(datafile)
			for other_project in projects:
				if (other_project != in_this_project):
					cross_project_datafile_set[other_project][1].append(datafile)

	
	return cross_project_datafile_set

						


		


