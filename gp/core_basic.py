import os, sys
import argparse
import pickle
import random
import re
import core
import numpy as np
import cross_validation
import evalTool
import utils4dataset
import classifier_cluster

parent_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir)
sys.path.insert(0, os.path.join(parent_dir, 'feature'))
import features

def evaluate(evaluation_set, datadir, dest, formula, featureInst,
		tiebreak, cudaFiledir, usegpu, postfix = "GP"):
	"""
	Evaluated the generated formula with a given evaluation data set.
	Result files will be ends with "postfix.(ranks|faults).csv"
	"""
	evalTool.evaluate(evaluation_set, formula, dest, postfix, tiebreak, featureInst, cudaFiledir, use_gpu = usegpu)


def init(datadir, pairFile = None, pairId = 0, clusterInfoFile = None, k = None, training_project = None, defined_features = None, num_of_fold = 10):
	"""
	Intialization method
	"""
	#get pair_data_lst which contains a pair of training and test data file IDs
	if (training_project):
		datafiles = [os.path.join(datadir, datafile) for datafile in os.listdir(datadir)]
		cross_project_datafile_set = utils4dataset.divideForCrossProject(datafiles)
		training_set = cross_project_datafile_set[training_project][0]
		evaluation_set = cross_project_datafile_set[training_project][1]
	else:
		pair_data_lst = cross_validation.get_cross_validation(datadir, pairFile, num_of_fold)
		training_set = pair_data_lst[str(pairId)][0]
		evaluation_set = pair_data_lst[str(pairId)][1]

	featureInst = features.Features(clusterInfoFile, k, defined_features)

	return (training_set, evaluation_set, featureInst)


def divide_data_set(data_set, dataPerCluster, numOfCluster = 2):
	"""
	divide data set into numOfCluster clusters
	"""
	list_of_divided_set = [[] for _ in range(numOfCluster)]
	for file_name in data_set:
		file_id = re.search("([^\/]*)\.dat", file_name).group(1)
		label = classifier_cluster.getLabel(dataPerCluster, numOfCluster, file_id)

		list_of_divided_set[np.int32(label)].append(file_name)
	return list_of_divided_set


def train_and_test(datadir, training_set, evaluation_set, featureInst,
	dest, pairFile, pairId, usegpu, cudaFiledir,
	maxTreeDepth, minTreeDepth, initMaxTreeDepth,
	with_valid = False, resultId = "0", postfix = "GP", tiebreak = "max", k = None, clusterInfoFile = None):
	"""
	train the formula using a five training set (unser datadir) and test(evaluate) it
	"""
	#generate formula directory
	formula_dest = os.path.join(dest, "formula")
	if (not os.path.exists(formula_dest)):
		os.mkdir(formula_dest)

	#generate FL formula
	if (with_valid):
		numOfTrain = len(training_set)
		numOfTrain_train = np.int32(np.floor(numOfTrain * 0.8))
		training_train_set = random.sample(training_set, numOfTrain_train)
		training_valid_set = list(set(training_set) - set(training_train_set))

		result_formula = core.experiment(datadir, formula_dest, resultId, training_train_set, training_valid_set, usegpu,
			cudaFiledir, featureInst, clusterInfoFile, k, maxTreeDepth, minTreeDepth, initMaxTreeDepth)
	else:
		result_formula = core.experiment(datadir, formula_dest, resultId, training_set, evaluation_set, usegpu,
			cudaFiledir, featureInst, clusterInfoFile, k, maxTreeDepth, minTreeDepth, initMaxTreeDepth)

	print "the result formula is " + result_formula

	print "evaluation start"
	rank_dest = os.path.join(dest, "rank")
	if (not os.path.exists(rank_dest)):
		os.mkdir(rank_dest)

	evaluate(evaluation_set, datadir, rank_dest, result_formula, featureInst, tiebreak, cudaFiledir, usegpu, postfix)

	return result_formula


def main_major_top_bottom_minor(datadir, dest, pairFile, pairId, usegpu, cudaFiledir,
	maxTreeDepth, minTreeDepth, initMaxTreeDepth,
	with_valid = False, resultId = "0", postfix = "GP", tiebreak = "max", label_dir = None, k = None, clusterInfoFile = None, num_of_fold = 10):
	"""
	FLUCCS with the data set which composes of four parts with different characteristics: mib, mit, mab, mat
	DOES NOT USE ANYMORE (NOT FULLY COMPLETED)
	WILL BE COMBINED WITH main_major_minor
	"""
	numOfCluster = 4
	(training_set, evaluation_set, featureInst) = init(datadir, pairFile, pairId, clusterInfoFile, k, num_of_fold = num_of_fold)
	dataPerCluster = classifier_cluster.getDataPerCluster(label_dir, numOfCluster)

	#divide data set into quarter
	#mib = minor bottom, mit = minor top, mab = major bottom, mat = major top
	(mib_training_set, mit_training_set, mab_training_set, mat_training_set) = divide_data_set(training_set, dataPerCluster, numOfCluster)
	(mib_test_set, mit_test_set, mab_test_set, mat_test_set) = divide_data_set(evaluation_set, dataPerCluster, numOfCluster)

	#combine two major sets to a single set
	major_training_set = mat_training_set
	major_training_set.extend(mab_training_set)
	major_training_set.extend(mit_training_set)

	major_test_set = mat_test_set
	major_test_set.extend(mab_test_set)
	major_test_set.extend(mit_test_set)

	#for major
	major_dest = os.path.join(dest, "major")
	os.mkdir(major_dest)
	train_and_test(datadir, major_training_set, major_test_set, featureInst,
		major_dest, pairFile, pairId, usegpu, cudaFiledir,
		maxTreeDepth, minTreeDepth, initMaxTreeDepth,
		with_valid, resultId, postfix, tiebreak, k, clusterInfoFile)

	#for top minor
#	minor_top_dest = os.path.join(dest, "minor_top")
#	os.mkdir(minor_top_dest)
#	train_and_test(datadir, mit_training_set, mit_test_set, featureInst,
#		minor_top_dest, pairFile, pairId, usegpu, cudaFiledir,
#		maxTreeDepth, minTreeDepth, initMaxTreeDepth,
#		with_valid, resultId, postfix, tiebreak, k, clusterInfoFile)

	#for top bottom
	minor_bottom_dest = os.path.join(dest, "minor_bottom")
	os.mkdir(minor_bottom_dest)
	train_and_test(datadir, mib_training_set, mib_test_set, featureInst,
		minor_bottom_dest, pairFile, pairId, usegpu, cudaFiledir,
		maxTreeDepth, minTreeDepth, initMaxTreeDepth,
		with_valid, resultId, postfix, tiebreak, k, clusterInfoFile)



def main_major_minor(datadir, dest, pairFile, pairId, usegpu, cudaFiledir,
	maxTreeDepth, minTreeDepth, initMaxTreeDepth,
	with_valid = False, resultId = "0", postfix = "GP", tiebreak = "max", label_dir = None, k = None, clusterInfoFile = None, cluster_for_minor = False,
	onlyTop10 = False, corrType = "Pear", usepval = True, numOfMinorCluster = 5, which_cluster = "kmeans", num_of_fold = 10):
	"""
	FLUCCS with the data set which composes of two parts with different characteristics: Major and Minor data set
	DOES NOT USE ANYMORE (NOT FULLY COMPLETED)
	WILL BE COMBINED WITH main_major_minor
	"""

	(training_set, evaluation_set, featureInst) = init(datadir, pairFile, pairId, clusterInfoFile, k, num_of_fold = num_of_fold)

	dataPerCluster = classifier_cluster.getDataPerCluster(label_dir, 2)
	(minor_training_data_set, major_training_data_set) = divide_data_set(training_set, dataPerCluster)
	(minor_test_data_set, major_test_data_set) = divide_data_set(evaluation_set, dataPerCluster)

	#for major
	major_dest = os.path.join(dest, "major")
	os.mkdir(major_dest)
	train_and_test(datadir, major_training_data_set, major_test_data_set, featureInst,
		major_dest, pairFile, pairId, usegpu, cudaFiledir,
		maxTreeDepth, minTreeDepth, initMaxTreeDepth,
		with_valid, resultId, postfix, tiebreak, k, clusterInfoFile)

	#for minor
	minor_dest = os.path.join(dest, "minor")
	os.mkdir(minor_dest)
	if (not cluster_for_minor):
		train_and_test(datadir, minor_training_data_set, minor_test_data_set, featureInst,
			minor_dest, pairFile, pairId, usegpu, cudaFiledir,
			maxTreeDepth, minTreeDepth, initMaxTreeDepth,
			with_valid, resultId, postfix, tiebreak, k, clusterInfoFile)
	else:
		#exploit values of argument *onlyTop10*, *corrType*, *usepval*, *which_cluster*
		minor_trainingDataArr = classifier_cluster.getDataForClassifyAndCluster(datadir, minor_training_data_set, onlyTop10, corrType, usepval, featureInst.getSuspFeatureSize())
		(cluster_handler, cluster_labels_for_train) = classifier_cluster.generateCluster(minor_trainingDataArr, numOfMinorCluster, which_cluster)
		k_clustered_train_data = utils4dataset.divideBasedOnLabel(minor_training_data_set, cluster_labels_for_train)
		print "clustere training data:"
		print k_clustered_train_data

		cluster_dir = os.path.join(minor_dest, "cluster")
		if (not os.path.exists(cluster_dir)):
			os.mkdir(cluster_dir)

		#set valType using *usepval*
		if (usepval):
			valType = "pval"
		else:
			valType = "coeff"

		pickle.dump(cluster_handler, open(os.path.join(cluster_dir, resultId + "." + corrType + "." + valType + "." + str(pairId) + "." + which_cluster + ".dat"), "w"))

		formula_dest = os.path.join(minor_dest, "formula")
		if (not os.path.exists(formula_dest)):
			os.mkdir(formula_dest)

		#get test data array formed for clustering using minor_test_data_set
		minor_testDataArr = classifier_cluster.getDataForClassifyAndCluster(datadir, minor_test_data_set, onlyTop10, corrType, usepval, featureInst.getSuspFeatureSize())
		#get cluster label for test data
		cluster_labels_for_eval = cluster_handler.predict(minor_testDataArr)
		#divide test data based on the predicted label *cluster_labels_for_eval*
		k_clustered_eval_data = utils4dataset.divideBasedOnLabel(minor_test_data_set, cluster_labels_for_eval)
		#prepare test(evaluation) data --> divide test data to *numOfMinorCluster* clusters
		print "clustered test data:"
		print k_clustered_eval_data

		minor_resultId = resultId + "." + corrType + "." + valType + "." + str(pairId) + ".minor"
		formulae_for_minor = dict()
		#generate formula for each cluster in minor data set
		for cluster_label in k_clustered_train_data.keys():
			aresultId = minor_resultId + "." + str(numOfMinorCluster) + "." + str(cluster_label)
			curr_training_set = k_clustered_train_data[cluster_label]

			#train only if there is matching(same clustering label) test data set
			if (cluster_label in k_clustered_eval_data.keys()):
				curr_test_set = k_clustered_eval_data[cluster_label]
				aformula = core.experiment(datadir, formula_dest, aresultId, curr_training_set, curr_test_set, usegpu,
					cudaFiledir, featureInst, clusterInfoFile, k,
					maxTreeDepth, minTreeDepth, initMaxTreeDepth)
				formulae_for_minor[cluster_label] = aformula
				print "the result formula for minor with cluster label " + str(cluster_label) + " is " + aformula
			else:
				print "test data for pair id " + str(pairId) + " does not have any data clustered as " + str(cluster_label)

		print "evaluation start"
		for cluster_label in k_clustered_eval_data.keys():
			cluster_label_minor_dest = os.path.join(minor_dest, str(cluster_label))
			os.mkdir(cluster_label_minor_dest)
			evalTool.evaluate(k_clustered_eval_data[cluster_label], formulae_for_minor[cluster_label], cluster_label_minor_dest, postfix, tiebreak, featureInst, cudaFiledir, use_gpu = usegpu)



def main(datadir, dest, pairFile, pairId, usegpu, cudaFiledir,
	maxTreeDepth, minTreeDepth, initMaxTreeDepth,
	with_valid = False, resultId = "0", postfix = "GP", tiebreak = "max", k = None, clusterInfoFile = None,
	training_project = None, only_susp = False, num_of_fold = 10):
	"""
	Using dat files under a given datadir, train the ranking model and test them
	"""

	if (only_susp):
		defined_features = (["ochiai", "jaccard", "gp13", "wong1", "wong2", "wong3",\
				"tarantula", "ample", "RussellRao", "SorensenDice", "Kulczynski1",\
				"SimpleMatching", "M1", "RogersTanimoto", "Hamming", "Ochiai2",\
				"Hamann", "dice", "Kulczynski2", "Sokal", "M2", "Goodman",\
				"Euclid", "Anderberg", "Zoltar", "ER1a", "ER1b", "ER5a", "ER5b",\
				"ER5c", "gp02", "gp03", "gp19"], [])
	else:
		defined_features = None

	#initialization
	(training_set, evaluation_set, featureInst) = init(datadir, pairFile, pairId, clusterInfoFile, k, training_project, defined_features, num_of_fold)
	#train the formula and test(evaluate) it
	train_and_test(datadir, training_set, evaluation_set, featureInst,
		dest, pairFile, pairId, usegpu, cudaFiledir,
		maxTreeDepth, minTreeDepth, initMaxTreeDepth,
		with_valid, resultId, postfix, tiebreak, k, clusterInfoFile)


if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("-datadir", action = "store", default = None, help = "a directory where data files are stored")
	parser.add_argument("-dest", action = "store", default = ".", help = "a dierctory where result files(each for ranks of all candidate elements and ranks of faulty elements")
	parser.add_argument("-pairFile", action = "store", default = None, help = "a file where pairs of training and test data for 10 fold cross validation are written")
	parser.add_argument("-pairId", action = "store", default = 0, type = int, help = "i.e training and test data for pairId'th fold")
	parser.add_argument("-usegpu", action = "store", default = 1, type = int, help = "1 if using gpu else 0")
	parser.add_argument("-tiebreak", action = "store", default = "max", help = "tie breaking rule")
	parser.add_argument("-cudaFiledir", action = "store", default = os.path.dirname(os.path.abspath(__file__)), help = "a directory where cuda related files are stored")
	parser.add_argument("-postfix", action = "store", default = "GP", help = "a postfix for ranking result files")
	parser.add_argument("-maxTreeDepth", action = "store", default = 8, type = int)
	parser.add_argument("-minTreeDepth", action = "store", default = 1, type = int)
	parser.add_argument("-initMaxTreeDepth", action = "store", default = 6, type = int)
	parser.add_argument("-resultId", action = "store", default = "0")
	parser.add_argument("-with_valid", action = "store", default = 0, type = int) # DO NOT USE YET
	parser.add_argument("-which_main", action = "store", default = "main", help = "a name of main fuction to execute") # DO NOT USE YET
	parser.add_argument("-label_dir", action = "store", default = None) # DO NOT USE YET
	parser.add_argument("-cluster_for_minor", action = "store", type = int, default = 0) # DO NOT USE YET
	parser.add_argument("-onlyTop10", action = "store", default = 0, type = int) # DO NOT USE YET
	parser.add_argument("-corrType", action = "store", default = "Pear", help = "Pear, Spera") # DO NOT USE YET
	parser.add_argument("-usepval", action = "store", default = 0, type = int) # DO NOT USE YET
	parser.add_argument("-numOfMinorCluster", action = "store", default = 5, type = int) # DO NOT USE YET
	parser.add_argument("-which_cluster", action = "store", default = "kmeans") # DO NOT USE YET
	parser.add_argument("-training_project", action = "store", default = None, help = "Lang, Math, Time, Closure, Chart, Mockito")
	parser.add_argument("-only_susp", action = "store", default = 0, type = int)
	#for using clustered feature set (clusterInfoFile, k)
	parser.add_argument("-clusterInfoFile", action = "store", default = None, help = "a file which constain cluster information") # DO NOT USE YET
	parser.add_argument("-k", action = "store", default = 1, type = int, help = "a k value which is used in PCA") # DO NOT USE YET
	parser.add_argument("-num_of_fold", action = "store", default = 10, type = int, help = "a number of folds for cross-fold validation")
#	parser.add_argument("-seed", action = "store", default = 0, type = int)

	args = parser.parse_args()
	_with_valid = bool(args.with_valid)

#	np.random.seed(args.seed)
#	random.seed(args.seed)

	if (args.which_main == "main"):
		main(args.datadir, args.dest, args.pairFile, args.pairId, args.usegpu, args.cudaFiledir, args.maxTreeDepth, args.minTreeDepth, args.initMaxTreeDepth, with_valid = _with_valid, resultId = args.resultId, training_project = args.training_project, postfix = args.postfix, only_susp = bool(args.only_susp), clusterInfoFile = args.clusterInfoFile, k = args.k, num_of_fold = args.num_of_fold)
	elif (args.which_main == "main_major_minor"):
		main_major_minor(args.datadir, args.dest, args.pairFile, args.pairId, args.usegpu, args.cudaFiledir, args.maxTreeDepth, args.minTreeDepth, args.initMaxTreeDepth, with_valid = _with_valid, resultId = args.resultId, label_dir = args.label_dir, cluster_for_minor = bool(args.cluster_for_minor), onlyTop10 = bool(args.onlyTop10), corrType = args.corrType, usepval = args.usepval, numOfMinorCluster = args.numOfMinorCluster, which_cluster = "kmeans", num_of_fold = args.num_of_fold)
	elif (args.which_main == "main_major_top_bottom_minor"):#pairFile should be None...
		main_major_top_bottom_minor(args.datadir, args.dest, args.pairFile, args.pairId, args.usegpu, args.cudaFiledir, args.maxTreeDepth, args.minTreeDepth, args.initMaxTreeDepth, with_valid = _with_valid, resultId = args.resultId, label_dir = args.label_dir, num_of_fold = args.num_of_fold)
	else:
		print "There is no function named " + args.which_main
		sys.exit()


