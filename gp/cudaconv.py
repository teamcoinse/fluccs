#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------
import pickle, sys, os
from deap import gp, tools, creator, base
import numpy

import pycuda.autoinit
import pycuda.driver as drv
from pycuda.compiler import SourceModule



#Generate argument dictionary(global arg_dict): key: argument(feature)_number, value: feature
def make_arg_dict(features):
	arg_dict = dict()
	for (argNumth, feature) in enumerate(features):
		arg_dict["ARG" + str(argNumth)] = feature + "[tid]"

	return arg_dict


#Convert the structure of individual, which is a tree, into String
def getCLExpression(individual, arg_dict, begin = 0):

	str_buf = ""
	sub_form = individual.searchSubtree(begin)
	start = sub_form.start
	stop = sub_form.stop
	length = stop - start

	is_leaf = True if (length == 1) else False
	#print str(individual[begin].arity) + "," + str(individual[begin].name)

	if not is_leaf:
		op = str(individual[begin].name)
		begin += 1
		if (op != "gp_sqrt" and op != "gp_neg"):
			(l_child_buf, begin) = getCLExpression(individual, arg_dict, begin)
			(r_child_buf, begin) = getCLExpression(individual, arg_dict, begin)
			if (op == "gp_div"):
				str_buf = op + "(" + l_child_buf + "," + r_child_buf + ")"
			else:
				if (op == "gp_add"):
					op = "+"
				elif (op == "gp_sub"):
					op = "-"
				elif (op == "gp_mul"):
					op = "*"
				str_buf = "(" + l_child_buf + " " + op + " " + r_child_buf + ")"
		else:
			(child_buf, begin) = getCLExpression(individual, arg_dict, begin)
			if (op == "gp_neg"):
				op = "-"
			str_buf = op + "(" + child_buf + ")"

	else:#is leaf
		terminal = str(individual[begin].name)
		if (terminal != "1.0"):
			terminal = arg_dict[terminal]
		begin += 1
		str_buf = terminal

	return (str_buf, begin)


#CUDATool
#initiate, load, compile, compute using GPGPU
class CUDATool:
	def __init__(self, featureIndice = None, kernelFileName = None):
		self.local_workgroup_size = 512
		self.gpu_list = []
		self.grid_length = []
		self.padded_length = []
		self.length = []
		self.result_list = []
		self.kernelFileName = kernelFileName
		self.mod = None
		if (featureIndice is None):
			#default self.featureIndice is 1-40
			self.featureIndice = range(1, 41)
		else:
			self.featureIndice = featureIndice

	def set_KernelFileName(self, kernelFileName):
		self.kernelFileName = kernelFileName


	def set_FeatureIndice(self, featureIndice):
		self.featureIndice = featureIndice


	def empty_list(self):
		self.gpu_list = []; self.grid_length = []; self.padded_length = []; self.length = []; self.result_list = []

	#load cuda kernel file
	def loadkernels(self, individuals=None):
		cudaFileDir = os.path.dirname(os.path.abspath(__file__))
		if (cudaFileDir not in self.kernelFileName):
			self.kernelFileName = os.path.join(cudaFileDir, self.kernelFileName)

		template = "".join(open(self.kernelFileName, "r").readlines())
		if individuals:
			for i in range(len(individuals)):
				cltemp = str(individuals[i])
				if (i < 9):
					template = template.replace("12340" + str(i+1), cltemp)
				else:
					template = template.replace("1234" + str(i+1), cltemp)

		return template

	#Copy entire data to gpu before starting the computation(evaluation of individual with current data)
	#featureIndice is a list of index for the target features
	def mem_cp(self, datas):
		self.empty_list()
		for (count, data) in enumerate(datas):
			self.gpu_list.append(list())
			(fault_index, vectors) = data
			length = 0
			length = len(vectors)
			padded_length = (length / self.local_workgroup_size + 1) * self.local_workgroup_size
			grid_x = padded_length / self.local_workgroup_size

			for featureIdx in self.featureIndice:
				featureVecs = numpy.zeros(padded_length, dtype=numpy.float32)
				featureVecs[:length] = [numpy.float32(v[featureIdx]) for v in vectors]
				self.gpu_list[count].append(drv.mem_alloc(featureVecs.nbytes))
				drv.memcpy_htod(self.gpu_list[count][-1], featureVecs)
			sps = numpy.array([0.0] * padded_length, dtype=numpy.float32)
			self.result_list.append(drv.mem_alloc(sps.nbytes))
			drv.memcpy_htod(self.result_list[-1], sps)

			self.grid_length.append(grid_x)
			self.padded_length.append(padded_length)
			self.length.append(length)


	#Compile evaluation functions for entire individual set - each represented as evaluate_(the order of individual)
	#This is to reduce the compile time by compiling only once instead of compiling whenever evaluation is needed
	def Compile(self, individuals):
		kernel_code = self.loadkernels(individuals)
		self.mod = SourceModule(kernel_code)

	#Core function: compute score for current given data
	def core(self, num, ind_index):
		sps = numpy.array([0.0] * self.padded_length[num], dtype=numpy.float32)

		individual_kernel = self.mod.get_function("evaluate_" + str(ind_index + 1))

		#access gpu data for num current data
		kernelExecution = "individual_kernel("
		for gpuVecsIdx in range(len(self.featureIndice)):
			kernelExecution += "self.gpu_list[num][" + str(gpuVecsIdx) + "],"
		kernelExecution += "self.result_list[num], block=(512, 1, 1), grid=(self.grid_length[num], 1))"
                eval(kernelExecution)

		#individual_kernel(self.gpu_list[num][0], self.gpu_list[num][1], self.gpu_list[num][2], self.gpu_list[num][3], self.gpu_list[num][4], self.gpu_list[num][5], self.gpu_list[num][6], self.gpu_list[num][7], self.gpu_list[num][8], self.gpu_list[num][9], self.gpu_list[num][10], self.gpu_list[num][11], self.gpu_list[num][12], self.gpu_list[num][13], self.gpu_list[num][14], self.gpu_list[num][15], self.gpu_list[num][16], self.gpu_list[num][17], self.gpu_list[num][18], self.gpu_list[num][19], self.gpu_list[num][20], self.gpu_list[num][21], self.gpu_list[num][22], self.gpu_list[num][23], self.gpu_list[num][24], self.gpu_list[num][25], self.gpu_list[num][26], self.gpu_list[num][27], self.gpu_list[num][28], self.gpu_list[num][29], self.gpu_list[num][30], self.gpu_list[num][31], self.gpu_list[num][32], self.gpu_list[num][33], self.gpu_list[num][34], self.gpu_list[num][35], self.gpu_list[num][36], self.gpu_list[num][37], self.gpu_list[num][38], self.gpu_list[num][39], self.result_list[num], block=(512, 1, 1), grid=(self.grid_length[num], 1))

		drv.memcpy_dtoh(sps, self.result_list[num])
		sps = sps[:self.length[num]]

		return sps


