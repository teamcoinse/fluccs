#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------

import os
import csv
import numpy as np

def gen_cross_validation(data_files, pair_file = "pair.txt", num_of_fold = 10):
	folds = {str(idx):[] for idx in range(num_of_fold)}
	fold_size = np.floor(len(data_files)/float(num_of_fold))
	print "Fold size for " + str(num_of_fold) + " cross fold validation is " + str(fold_size)

	fold_num = 0
	for (idx, afile) in enumerate(data_files):
		if ((idx + 1) % fold_size > 0 and fold_num < num_of_fold - 1):
			folds[str(fold_num)].append(afile)
		else:
			if (fold_num < num_of_fold - 1):
				fold_num += 1
			folds[str(fold_num)].append(afile)

        paired_folds = {}
	csvWriter = csv.writer(open(pair_file, "w"))
	for fold_id in range(num_of_fold):
		csvWriter.writerow([str(fold_id)])
		others = []
		for _fold_id in range(num_of_fold):
			if (fold_id != _fold_id):
				others.extend(folds[str(_fold_id)])

		csvWriter.writerow(others)
		csvWriter.writerow(folds[str(fold_id)])
                paired_folds[str(fold_id)] = (others, folds[str(fold_id)])
	return paired_folds


#Get file name for pair_file, contains information about the pair, and data directory
#Return data with ten cross validation structure
def get_cross_validation(data_dir, pair_file = None, num_of_fold = 10):
	pair_datas = dict()

	if (pair_file is None):
		files = [afile for afile in os.listdir(data_dir)]
		pair_datas = gen_cross_validation(files, num_of_fold = num_of_fold)
	else:
		handler = open(pair_file, "r"); count = 0; key = ""
		while (True):
			line = handler.readline()
			if not line: break
			line.rstrip()
			data = line.split(",")
			if (count == 0):
				key = data[0].rstrip(); count += 1
			elif (count == 1):
				temp = []
				for e in data:
					line = e.rstrip()
					temp.append(os.path.join(data_dir, line))
				pair_datas[key] = [temp]; count += 1
			else:
				temp = []
				for e in data:
					line = e.rstrip()
					temp.append(os.path.join(data_dir, line))
				pair_datas[key].append(temp)
				count = 0

		handler.close()

	return pair_datas


