import os, sys
import numpy as np
import csv
import re
import argparse
import pickle
import itertools
from scipy.stats import rankdata
#
import computeCorr
import genClassifier
import genCluster

features = ["ochiai", "jaccard", "gp13", "wong1", "wong2", "wong3", "tarantula", "ample", "RussellRao",\
	"SorensenDice", "Kulczynski1", "SimpleMatching", "M1", "RogersTanimoto", "Hamming", "Ochiai2",\
	"Hamann", "dice", "Kulczynski2", "Sokal", "M2", "Goodman", "Euclid", "Anderberg", "Zoltar",\
	"ER1a", "ER1b", "ER5a", "ER5b", "ER5c", "gp02", "gp03", "gp19"]


#return pval if usepval is True(using pvalue) else return rho(using coefficient)
getPvalOrCoeff = lambda ((usepval, pval, rho)): pval if (usepval) else rho

countFeatures = lambda(featureRange): featureRange[1] - featureRange[0] if (featureRange is not None) else len(features)


def loadData(datadir, fileName, top10 = False, featureRange = None):
	fileId = re.search("([^\/]*)\.dat", fileName).group(1)
	if (datadir not in fileName):
		fileName = os.path.join(datadir, fileName)

	(faultIndice, tempDatas) = pickle.load(open(fileName))
	faultIndice = np.float32(faultIndice)
	#exclude code and change metrics --> will always included
	if (featureRange is None):
		datas = np.asarray([np.float32(adata[1:34]) for adata in tempDatas]).transpose(1,0)
	else:
		firstIdx, lastIdx = featureRange
		datas = np.asarray([np.float32(adata[firstIdx:lastIdx]) for adata in tempDatas]).transpose(1,0)

	if (top10):
		top10datas = list()
		for data in datas:
			temp = list()
			ranks = rankdata(data)
			for (idx, rank) in enumerate(ranks):
				if (rank >= len(data) - 10):
					temp.append(data[idx])
				else:
					temp.append(1.0)
			top10datas.append(np.asarray(temp))
		datas = np.asarray(top10datas)

	#label(1:faulty, 0:non_faulty)
	#not used while generating classifier
	#labels = [1 if (idx in faultIndice) else 0 for idx in range(len(tempDatas))]

	#return (fileId, datas, labels)
	return (fileId, datas)


def getOnlyUpperTriangle(datas):
	only_upper_tri = list()
	numOfFeatures = datas.shape[0]
	if (datas.shape[1] != numOfFeatures):
		print "A given argument datas should be diagnoal"
		sys.exit()

	for idx in range(numOfFeatures):
		only_upper_tri.extend(list(datas[idx][idx:numOfFeatures]))

	return only_upper_tri



def genCorrMatrix(data, corrType, usepval, numOfFeatures):
	"""
	generate a matrix which contains values for correlation bewteen features
		:numOfFeatures X numOfFeatures matrix will be returned
	"""
	global getPvalOrCoeff

	if (usepval):
		featureCorrMatrix = np.zeros((numOfFeatures, numOfFeatures), dtype = np.float32)
	else:
		featureCorrMatrix = np.ones((numOfFeatures, numOfFeatures), dtype = np.float32)

	data_w_idx = zip(range(data.shape[0]), data)
	combs = itertools.combinations(data_w_idx, 2)

	for ((aidx, afeature), (bidx, bfeature)) in combs:
		if (corrType == "Pear"):
			(rho, pval) = computeCorr.computeSpearmanCorr(afeature, bfeature)
		elif (corrType == "Spear"):
			(rho, pval) = computeCorr.computePearsonCorr(afeature, bfeature)
		else:
			print "Wrong correlation type: " + corrType
			sys.exit()

		featureCorrMatrix[aidx][bidx] = getPvalOrCoeff((usepval, pval, rho))

	return featureCorrMatrix


def getDataForClassifyAndCluster(datadir, dataFileSet, onlyTop10, corrType, usepval, numOfFeatures):
	"""
	return data array for classifying or clustering
	"""
	dataLst = []
	for dataFile in dataFileSet:
		(_, data) = loadData(datadir, dataFile, onlyTop10)
		featureCorrMatrix = genCorrMatrix(data, corrType, usepval, numOfFeatures)
		upper_tri_corr_mat = getOnlyUpperTriangle(featureCorrMatrix)
		dataLst.append(upper_tri_corr_mat)
	
	dataArr = np.asarray(dataLst)
	return dataArr


def generateClassifier(trainingDataSet, trainingLabelSet, testDataSet, testLabelSet, whichClassifier, numOfCluster, seed):
	"""
	generate a classifier (specified by whichClassifier)
	and return the classifier with its f1 score
	"""
	if (whichClassifier == "rf"):
		(clf, f1) = genClassifier.applyRandomForest(trainingDataSet, trainingLabelSet, testDataSet, testLabelSet, numOfCluster, seed)
	elif (whichClassifier == "adab"):
		(clf, f1) = genClassifier.applyAdaboost(trainingDataSet, trainingLabelSet, testDataSet, testLabelSet, numOfCluster, seed)
	elif (whichClassifier == "gp"):
		(clf, f1) = genClassifier.applyGaussianProcess(trainingDataSet, trainingLabelSet, testDataSet, testLabelSet, numOfCluster, seed)
	elif (whichClassifier == "knn"):
		(clf, f1) = genClassifier.applyKNearestNeighbors(trainingDataSet, trainingLabelSet, testDataSet, testLabelSet, numOfCluster)
	else:
		print "Wrong classifier type: " + whichClassifier
		sys.exit()

	return (clf, f1)


def generateCluster(dataSet, k, whichCluster):
	"""
	apply *whichCluster* cluster to *dataSet*
	the result is a list of cluster label for each data in a given *dataSet*	
	"""

	if (whichCluster == "kmeans"):
		kmeans = genCluster.genKMeansClustering(dataSet, k)
		cluster_labels = kmeans.predict(dataSet)
	else:
		print "Wrong cluster type: " + whichCluster
		sys.exit()
	
	return (kmeans, cluster_labels)


def getDataPerCluster(labeldir, numOfCluster):
	dataPerCluster = genClassifier.getInWhichCluster(labeldir, numOfCluster)
	return dataPerCluster


def getLabel(dataPerCluster, numOfCluster, fileId):
	for idx in range(numOfCluster):
		if (genClassifier.inThisCluster(dataPerCluster[idx], fileId)):
			return idx
		else:
			continue
	return None

def get_final_data_and_label_arr(datadir, fileNames, numOfCluster, dataPerCluster, corrType, onlyTop10, usepval, featureRange):
	global features, countFeatures

	data_lst = list()
	label_lst = list()

	numOfFeatures = countFeatures(featureRange)

	for fileName in fileNames:
		(fileId, dataArr) = loadData(datadir, fileName, onlyTop10, featureRange)
		featureCorrMatrix = genCorrMatrix(dataArr, corrType, usepval, numOfFeatures)
		upper_tri_corr_mat = getOnlyUpperTriangle(featureCorrMatrix)
		
		data_lst.append(upper_tri_corr_mat)
		#should get label
		label = getLabel(dataPerCluster, numOfCluster, fileId)
		if (label is None):
			print fileId + " is not in any of the cluster"
			sys.exit()
		
		label_lst.append(label)

	data_arr = np.asarray(data_lst)
	label_arr = np.asarray(label_lst)

	return (data_arr, label_arr)


def main(datadir, fileNames, corrType, usepval, onlyTop10, labeldir,
		numOfCluster = 2, whichClassifier = "rf", iterIdx = 10, featureRange = None):

	#generate a dictionary which contains information about which fault belongs to which division fold
	dataPerCluster = getDataPerCluster(labeldir, numOfCluster)

	#generate data set and label for them
	#fileNames = os.listdir(datadir)

	entireFeatureCorrArr = None; entireLabelArr = None
	trainingDataArr = None; trainingLabelArr = None
	testDataArr = None; testLabelArr = None
	with_fixed_train_test = False

	if (type(fileNames) == tuple):
		with_fixed_train_test = True
		(training_files, test_files) = fileNames
		(trainingDataArr, trainingLabelArr) = get_final_data_and_label_arr(datadir, training_files, numOfCluster, dataPerCluster, corrType, onlyTop10, usepval, featureRange)
		(testDataArr, testLabelArr) = get_final_data_and_label_arr(datadir, test_files, numOfCluster, dataPerCluster, corrType, onlyTop10, usepval, featureRange)
	else:
		(entireLabelList, entireLabelArr) = get_final_data_and_label_arr(datadir, fileNames, numOfCluster, dataPerCluster, corrType, onlyTop10, usepval, featureRange)


	#iterate iterIdx times and pick the one with median/best performance
	clfWithF1Scores = list()
	for idx in range(iterIdx):
		if (not with_fixed_train_test):
			((trainingDataArr, trainingLabelArr), (testDataArr, testLabelArr)) = genClassifier.getTrainingAndTestingSet(entireFeatureCorrArr, entireLabelArr)
		
		#generate classifier
		(clf, f1) = generateClassifier(trainingDataArr, trainingLabelArr, testDataArr, testLabelArr, whichClassifier, numOfCluster, idx)
		clfWithF1Scores.append((clf, f1))

	#sorted in descending order
	sortedClfWithF1Scores = sorted(clfWithF1Scores, key = lambda v: v[1], reverse = True)

	if (with_fixed_train_test):
		targetIndx = 0
	else:
		#targetIndx = 0
		targetIndx = len(sortedClfWithF1Scores) / 2

	(resultClf, resultF1Score) = sortedClfWithF1Scores[targetIndx]
	print "The f1 score for the selected: ", resultF1Score

	return (resultClf, resultF1Score, dataPerCluster)










