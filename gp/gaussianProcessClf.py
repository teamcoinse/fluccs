import os, sys
import csv, pickle
import numpy as np
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF


class GaussianProcessClf:
	def __init__(self, kernel_type = "anisotropic", random_state = None, dim = 40):
		if (kernel_type is None):
			self.kernel = None
		else:
			self.kernel = self.get_kernel(kernel_type, dim) 
		
		self.random_state = random_state
		self.clf = None	


	def get_length_scale(self, kernel_type, dim):
		if (kernel_type == "anisotropic"):
			return [1.0] * np.int32(dim)
		elif (kernel_type == "isotropic"):
			return 1.0
		else:
			print "Wrong kernel type: " + kernel_type
			print "Should be either 'anisotropic' or 'isotropic'"
			sys.exit()


	def get_kernel(self, kernel_type, dim):
		self.length_scale = self.get_length_scale(kernel_type, dim)
		self.kernel = 1.0 * RBF(length_scale = self.length_scale)


	def generateClassifier(self):
		if (self.kernel is None):
			self.clf = GaussianProcessClassifier()
		else:
			self.clf = GaussianProcessClassifier(kernel = self.kernel, random_state = self.random_state)
		return self.clf

