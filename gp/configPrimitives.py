"""
Primitive set configuration
"""

from deap import gp
import gpOperators

#add operator primitives to a given pset
def addOperators(pset):
	pset.addPrimitive(getattr(gpOperators, "gp_add"), 2)
	pset.addPrimitive(getattr(gpOperators, "gp_mul"), 2)
	pset.addPrimitive(getattr(gpOperators, "gp_sub"), 2)
	pset.addPrimitive(getattr(gpOperators, "gp_div"), 2)
	pset.addPrimitive(getattr(gpOperators, "gp_div"), 2)
	pset.addPrimitive(getattr(gpOperators, "gp_neg"), 1)
	
	return pset

#add terminals to a given pset
def addTerminals(pset):
	pset.addTerminal(1.0)
	return pset

def configPrimitiveSet(features):
	print features
	pset = gp.PrimitiveSet("main", len(features))
	pset = addOperators(pset)
	for (argNumth, feature) in enumerate(features):
		pset.renameArguments(**{"ARG" + str(argNumth):feature})

	return pset

