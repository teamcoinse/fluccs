#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------

import pickle, math, sys, os, random, numpy
from deap import gp, tools, creator, base, algorithms
import operator
import cross_validation
import time
import csv
import re
import argparse
from scipy.stats import rankdata
from gpOperators import *
import writeKernelFile
import configPrimitives

parent_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir)
sys.path.insert(0, os.path.join(parent_dir, 'python'))
sys.path.insert(0, os.path.join(parent_dir, 'feature'))
import parser_check
import features

training_set = None
toolbox = None
training_datas = []
current_datas = []
indice_of_current_datas = []

seed = None
arg_dict = dict()

cudatool = None
kernelFileName = None
usegpu = None
cudaFileDir = None
individuals = list()

#instance of class features
featureInst = None

#TO FOCUSE ON MINOR
black_list = None
num_of_continuous_loc = None
enough = 0
min_training_size = 0


def init(_training_set, _usegpu, _cudaFileDir, _enough = 0, _featureInst = None, _clusterInfoFile = None, _k = None, focus_on_minor = False):
	"""
	Initialize global variables
	"""
	global training_set, featureInst, cudaFileDir, cudatool, arg_dict, kernelFileName, usegpu, num_of_continuous_loc, enough, black_list

        print "in init"
	if (focus_on_minor):
		black_list = []
		enough = _enough
	usegpu = _usegpu
        if (_featureInst is None):
	    featureInst = features.Features(clusterInfoFile = _clusterInfoFile, k = _k)
        else:
            featureInst = _featureInst

        training_set = _training_set
	if (focus_on_minor): 
		num_of_continuous_loc = {idx:0 for idx in range(len(training_set))}

	#if using gpu, then import cudaconv and generate instance of it(cudatool)
	if (bool(usegpu)):
		cudaFileDir = _cudaFileDir
                #make import cudaconv to global
		globals()['cudaconv'] = __import__('cudaconv')
		if (not featureInst.isClustered):
			kernelFileName = os.path.join(cudaFileDir, "kernel_template_basic.cuda")
		else:
			kernelFileName = os.path.join(cudaFileDir, "kernel_template_F_" + str(featureInst.getSize()) + ".cuda")

		writeKernelFile.writeKernel(featureInst.getAllFeatures(), 48, kernelFileName, cudaFileDir)

		cudatool = cudaconv.CUDATool(featureIndice = range(1, featureInst.getSize() + 1), kernelFileName = kernelFileName)

		arg_dict = cudaconv.make_arg_dict(featureInst.getAllFeatures())



def rank(re_vector):
	global seed
	return ranking_w_seed.rank(re_vector, ties="random", decreasing=True, seed = seed)


#return list of individual done with clexpression processing
#this method is executed only if usegpu == 1
def get_individual_list(population):
	global arg_dict
	individuals = list()
	
	for ind in population:
		(temp, _) = cudaconv.getCLExpression(ind, arg_dict)
		individuals.append(temp)

	return individuals

#evaluate individual without using gpu
def eval_wo_gpu(individual, current_data):
	global featureInst

	(_, vectors) = current_data
	length = len(vectors)
	sps = numpy.array([0.0] * length, dtype=numpy.float32)

	featureNames = featureInst.getAllFeatures()
	_features = dict()

	for (featureIdx, featureName) in enumerate(featureNames):
		_features[featureName] = [numpy.float32(v[featureIdx + 1]) for v in vectors]
		individual = individual.replace(featureName, "_features['" + featureName + "'][idx]")

	#compute suspicousness score for each data row(per method) in current_data
	for idx in range(length):
		sps[idx] = - eval(individual)

	return sps

#Evaluate individual having ind_index(individual index)
def eval_func(ind_index):
	global usegpu, cudatool, toolbox, current_datas, individuals
	global indice_of_current_datas, min_training_size, num_of_continuous_loc, black_list, enough
	fitness_values = []


	for i in range(len(current_datas)):
		if (bool(usegpu)):
			core_values = cudatool.core(i, ind_index)
		else:
#			print "====="
#			print ind_index
#			print individuals[ind_index]
#			print "===="
#			print i
#			print "+++++"
			core_values = eval_wo_gpu(individuals[ind_index], current_datas[i])
		#core_ranks = rank(core_values) #rank with random-tie-breaker
		core_ranks = rankdata(core_values, method = "max")
		core_rank = []
		(fault_index, _ ) = current_datas[i]

		for fault in fault_index:
			core_rank.append(normalise_rank(core_ranks, int(fault)))

		min_core_rank = min(core_rank)
		#check whether this fault should be in black list
		#because the faulty element(s) of this data has(have) been already localized more than threshold
		#this operation is proceeded only if the size of remaining training set is larger than minimum training set size
		if (black_list is not None):
			num_of_continuous_loc[indice_of_current_datas[i]] += 1
			if (num_of_continuous_loc[indice_of_current_datas[i]] >= enough
				and len(training_set) - 1 >= min_training_size):	
				black_list.append(indice_of_current_datas[i])
		###

		fitness = numpy.float32(min_core_rank)		
		fitness_values.append(fitness)

	#Logging
	print "average ",individuals[ind_index], numpy.average(fitness_values)
	return (numpy.average(fitness_values),)


#Return percent value list for ranks
def normalise_rank(ranks, index):
	return numpy.float32(ranks[index]) / numpy.float32(len(ranks)) * 100

#Write final results of test data
def write_result(result, dest, best_fit, result_id):
	result_filename = result_id + ".result.csv"
	if (not os.path.exists(dest)):
		os.mkdir(dest)
	csvWriter = csv.writer(open(os.path.join(dest, result_filename), "w"))
	csvWriter.writerow([result, best_fit])


def training(datadir, maxTreeDepth, minTreeDepth, initMaxTreeDepth, seed = None):
	global training_set, training_datas

	print "in training"
	for training_file in training_set:
		fileName = training_file
		if (datadir not in training_file):
			fileName = os.path.join(datadir, fileName)
		training_datas.append(pickle.load(open(fileName,"r")))

	best = evolve(maxTreeDepth, minTreeDepth, initMaxTreeDepth, seed)
	print "candidates: "
	print best
	return best


def evaluateResult(datadir, dest, evaluation_set, result_id, best):
	"""
	evaluate using data files in evaluation_set.
	among candidate formulae in best, the one which has the best performance with evaluation data set
	will be chosen as the final result.
	final result (final formula) will be written with file specified with result_id under dest directory
	"""

	global individuals, usegpu, training_set
	global cudaFileDir, kernelFileName, featureInst

        #get data for evaluation
        evaluation_datas = []
        for evaluation_file in evaluation_set:
		fileName = evaluation_file
		if (datadir not in evaluation_file):
			fileName = os.path.join(datadir, fileName)
                evaluation_datas.append(pickle.load(open(fileName)))

	cand_values = dict()
	if (bool(usegpu)):
		eval_cudatool = cudaconv.CUDATool(featureIndice = range(1, featureInst.getSize() + 1),
						kernelFileName = kernelFileName)
		individuals = get_individual_list(best)
		eval_cudatool.Compile(individuals)
		eval_cudatool.mem_cp(evaluation_datas)
	else:
		individuals = [str(indv) for indv in best]

	#compute fitness for each candidate formula in best
	for (ind_index, cand) in enumerate(best):
		fitness_values = []
		for i in range(len(evaluation_datas)):
			#where gpu is used
			if (bool(usegpu)):
				core_values = eval_cudatool.core(i, ind_index)
			else:
				core_values = eval_wo_gpu(individuals[ind_index], evaluation_datas[i])

			core_ranks = rankdata(core_values, method = "max")
			norm_core_ranks = []
			(fault_index, _) = evaluation_datas[i]
			for fault in fault_index:
				norm_core_ranks.append(normalise_rank(core_ranks, int(fault)))
			fitness = numpy.float32(min(norm_core_ranks))
			fitness_values.append(fitness)
		cand_values[str(cand)] = numpy.average(fitness_values)

	#find out the formula with the best fitness
	result_form = ""; best_fit = -1
	for key in cand_values.keys():
		if (best_fit == -1):
			best_fit = cand_values[key]; result_form = key
			continue
		else:
			if (best_fit > cand_values[key]):
				best_fit = cand_values[key]; result_form = key

	#logging
	result = (result_form, list(training_set))
	print "\nThe best individual "
	print "\t\t expressin : " + str(result_form)
	print "\t\t fitness   : " + str(best_fit)
	print str(result)
	print "evaultaion set:"
	print list(evaluation_set)

	#write down the best formula
	write_result(str(result_form), dest, str(best_fit), result_id)

	return result_form


def experiment(data_dir, dest, result_id, _training_set, evaluation_set, _usegpu, _cudaFileDir,
            _featureInst, _clusterInfoFile, _k, maxTreeDepth, minTreeDepth, initMaxTreeDepth, enough = 0, focus_on_minor = False, seed = None):
	"""
	"""
        print "in experiemnt"
        print _training_set
        print evaluation_set
        init(_training_set, _usegpu, _cudaFileDir, enough, _featureInst, _clusterInfoFile, _k, focus_on_minor)

	best = training(data_dir, maxTreeDepth, minTreeDepth, initMaxTreeDepth, seed)
	resultFormula = evaluateResult(data_dir, dest, evaluation_set, result_id, best)
	return resultFormula


def select_training_set(num_of_training):
	"""
	"""
	global training_datas, black_list, indice_of_current_datas

	indice_to_training_datas = range(0, len(training_datas))
	if (black_list is not None):
		without_black_list = list(set(indice_to_training_datas) - set(black_list))
	
		curr_training_index = random.sample(without_black_list, num_of_training)
		#set the list of training index as global one
		indice_to_training_datas = curr_training_index
	else:
		curr_training_index = random.sample(indice_to_training_datas, num_of_training)	

	return curr_training_index


#get initial population to apply gp and number of generations ngen
#apply elitism --> previous best solutions can remain to next generation population
#Return population after ngen generation of evolution
def gp_w_elit(population, ngen):
	global usegpu, toolbox, training_set, current_datas, training_datas, cudatool, individuals, min_training_size

	print "in gp_w_elit"
	stats = tools.Statistics(lambda ind: ind.fitness.values[0])
	stats.register("average", numpy.mean)
	stats.register("max", numpy.max)
	stats.register("min", numpy.min)


	current_datas = []
	training_range = len(training_set); sample_size = 0
	if (training_range >= 30):
		sample_size = 30
	else:
		sample_size = training_range
	#set minimum required size of the training sest
	min_training_size = sample_size

	#training_index = random.sample(list(range(training_range)), sample_size)
	training_index = select_training_set(sample_size)

	for index in training_index:
		current_datas.append(training_datas[index])

	#if using gpu, copyt current_datas to gpu memory
	#get inidividual list and compile them beforehand
	if (bool(usegpu)):
		cudatool.mem_cp(current_datas)

		individuals = get_individual_list(population)
		cudatool.Compile(individuals)
	else:
		individuals = [str(indv) for indv in population]

	fits = toolbox.map(toolbox.evaluate, range(len(individuals)))
	for fit, ind in zip(fits, population):
		ind.fitness.values = fit

	current_bests = tools.HallOfFame(8)
	current_bests.update(population)

	print "init pop completed"

	for i in range(ngen):
		current_datas = []
	
		#training_index = random.sample(list(range(training_range)), sample_size)
		training_index = select_training_set(sample_size)

		#set current data
		for index in training_index:
			current_datas.append(training_datas[index])

		print "Check"
		print sample_size
		print training_index
		print training_set
		print len(training_set)
		#if using gpu, copy current data to gpu
		if (bool(usegpu)):
			cudatool.mem_cp(current_datas)

		#logging
		print "current training data set for generation "
		tr_list = ""
		for index in training_index:
			 tr_list += "," + str(list(training_set)[index])

                print tr_list[1:]
		print "\n"

		offspring = algorithms.varAnd(population, toolbox, cxpb = 1.0, mutpb = 0.1)
		if (bool(usegpu)):
			individuals = get_individual_list(offspring + list(current_bests))
			cudatool.Compile(individuals)
		else:
			individuals = [str(indv) for indv in offspring + list(current_bests)]

		#apply toolbox.evaluate to every individuals in offspring --> set of fitness values is returned
		fits = toolbox.map(toolbox.evaluate, range(len(offspring)))
		for fit, ind in zip(fits, offspring):
			ind.fitness.values = fit

		#reevalute fitness of current population's bests using training set which is used for making offspring
		fits = toolbox.map(toolbox.evaluate, [len(offspring) + v for v in range(8)])
		for fit, ind in zip(fits, current_bests):
			ind.fitness.values = fit
		bests = list(current_bests)

		#select next population between offsrping and eight best individual
		population = toolbox.select(offspring + bests, len(population))
		#update current best for this new population
		current_bests.update(population)

		#Logging current status the population
		stats_result = stats.compile(population)
		#print "=================Statistics================="
		print "\t\tGeneration " + str(i) + ": " + str(stats_result["max"]) + "(MAX), " + str(stats_result["average"]) + "(AVG), " + str(stats_result["min"]) + "(MIN)"

	return population

#Evolve and return the best individual after GP with 100 generation
#Use of seed is only required if random-tie-breaking is needed; if not MAX-tie breaking is default
#and thus no need for seed(None)
def evolve(maxTreeDepth, minTreeDepth, initMaxTreeDepth, current_seed):
	global toolbox, seed, featureInst, cudatool, cudaFileDir
	#initialize seed for current GP
	seed = current_seed

	print "in evolve"

	#Setting parameter(feature) for evolving: pset == feature set
	pset = configPrimitives.configPrimitiveSet(featureInst.getAllFeatures())

	if ("FitnessMin" in globals()):
		del creator.FitnessMin
	creator.create("FitnessMin", base.Fitness, weights = (-1.0,))
	if ("Individual" in globals()):
		del creator.Individual
	creator.create("Individual", gp.PrimitiveTree, fitness = creator.FitnessMin, pset = pset)

	toolbox = base.Toolbox()

	toolbox.register("expr", gp.genHalfAndHalf, pset = pset, min_ = minTreeDepth, max_ = initMaxTreeDepth)
	toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.expr)
	toolbox.register("population", tools.initRepeat, list, toolbox.individual, n = 2)

	toolbox.register("compile", gp.compile, pset = pset)
	toolbox.register("evaluate", eval_func)

	#selBest is for the elitism
	toolbox.register("select", tools.selBest)
	toolbox.register("mate", gp.cxOnePoint)
	toolbox.register("mutate", gp.mutUniform, expr = toolbox.expr, pset = pset)

	toolbox.decorate("mate", gp.staticLimit(key = operator.attrgetter("height"), max_value = maxTreeDepth))
	toolbox.decorate("mutate", gp.staticLimit(key = operator.attrgetter("height"), max_value = maxTreeDepth))

	#Default setting is population = 40, generation = 100, best = 8
	#generate initial population
	pop = toolbox.population(n=8)
	pop = gp_w_elit(pop, 2)

	#HallOfFame contains the best inidividual that ever lived in the population during the evolution
	best = tools.HallOfFame(8)
	#8 best
	best.update(pop)

#	Logging
#	print "\nThe best individual "
#	print "\t\t expressin : " + str(best[0])
#	print "\t\t fitness   : " + str(best[0].fitness.values[0])

	return best


#WILL NOT BE USED
if __name__ == "__main__":
	#Parse arguments from command line
	parser = argparse.ArgumentParser()
	parser.add_argument("-datadir", action = "store", default = "Final_data_w_norm")
	parser.add_argument("-dest", action = "store", default = "output")
	parser.add_argument("-resultid", action = "store", default = "0", help = "id of the result: result_id.result.csv")
	parser.add_argument("-pairFile", action = "store", default = None, help = "a name of file where pairs of data files for cross-validation are written")
	parser.add_argument("-pairid", action = "store", default = None)
	parser.add_argument("-seed", action = "store", default = None)
	parser.add_argument("-maxTreeDepth", action = "store", default = 8, type = int, help = "maximum depth of the tree")
	parser.add_argument("-minTreeDepth", action = "store", default = 1, type = int, help = "minimum depth of the tree")
	parser.add_argument("-initMaxTreeDepth", action = "store", default = 6, type = int, help = "maximum depth of tree for the initialization: SHOULD BE SMALLER OR EQUAL TO 'maxTreeDepth'")
	parser.add_argument("-usegpu", action = "store", default = 0, type = int, help = "0 if not using gpu and 1 if using gpu")
	parser.add_argument("-clusterInfoFile", action = "store", default = None, help = "a file which contains information about feature cluster based on correlation between them")
	parser.add_argument("-cudaFileDir", action = "store", default = os.path.dirname(os.path.abspath(__file__)), help = "a directory where cuda related files are stored")
	parser.add_argument("-k", action = "store", type = int, default = "k in PCA")

	args = parser.parse_args()
	parser_check.argument_check(parser, ["pair id"], [args.pairid])
	usegpu = args.usegpu

	t = time.time()
	print "start"
	print t

	#instansitate class features -> featureInst
	featureInst = features.Features(clusterInfoFile = args.clusterInfoFile, k = args.k)

	#if using gpu, then import cudaconv and generate instance of it(cudatool)
	if (bool(usegpu)):
		cudaFileDir = args.cudaFileDir
		import cudaconv
		if (not featureInst.isClustered):
			kernelFileName = os.path.join(cudaFileDir, "kernel_template_basic.cuda")
		else:
			kernelFileName = os.path.join(cudaFileDir, "kernel_template_F_" + str(featureInst.getSize()) + ".cuda")
		writeKernelFile.writeKernel(featureInst.getAllFeatures(), 48, kernelFileName, args.cudaFileDir)
		cudatool = cudaconv.CUDATool(featureIndice = range(1, featureInst.getSize() + 1), kernelFileName = kernelFileName)


	experiment(args.datadir, args.dest, args.resultid, args.pairFile, args.pairid, args.maxTreeDepth, args.minTreeDepth, args.initMaxTreeDepth, args.seed)

	t2 = time.time()
	print "end"
	print t2
	print "total: "
	print "{0} seconds".format(t2 -t)
