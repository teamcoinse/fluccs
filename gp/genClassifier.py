import os, sys
import csv, pickle
import random
import numpy as np
import re
import argparse
from sklearn import preprocessing
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import AdaBoostClassifier, RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score
 
import gaussianProcessClf

getInSameClusterLabelFileName = lambda numOfCluster: str(numOfCluster) + ".diff.NumOfSuccess.dat"
getInWhichClusterLabelFileName = lambda (numOfCluster, idx): "Fault.in." + str(numOfCluster) + "." + str(idx) + ".csv"
getDataFileName = lambda corrType: corrType + ".cosine.sim.csv"

inThisCluster = lambda dataLst, dataId: True if (dataId in dataLst) else False

def apply_min_max_scaler(datas):
	"""
	apply min_max_scaler: datas should be an array
	"""
	minVal = -1.; maxVal = 1.
	
	data_scaled = np.zeros(datas.shape, dtype = np.float32)
	for idx in range(datas.shape[0]):
		norm_data = (datas[idx] - minVal) / (maxVal - minVal)
		data_scaled[idx] = norm_data

#	datas_as_2d = np.reshape(datas, (-1, 1))
#	min_max_scaler = preprocessing.MinMaxScaler()
#	datas_scaled = min_max_scaler.fit_transform(datas_as_2d).flatten()
	return data_scaled


def getInSameClusterLabel(labeldir, numOfCluster):
	"""
	read a file where lables(0 or 1) are written.
	label is 1 if they belong to the same division and 0 if not
	return label as 1-d array
	"""
	global getInSameClusterLabelFileName

	labelFileName = os.path.join(labeldir, getInSameClusterLabelFileName(numOfCluster))
	labelArray = pickle.load(open(labelFileName)).flatten()
	return labelArray


def getDataNameInCluster(fileName):
	csvReader = csv.reader(open(fileName))
	dataLst = [row[0] for row in csvReader]
	return dataLst


def getInWhichCluster(labeldir, numOfCluster):
	"""
	return a dictionary whose key is cluster id and value is id for each data(fautl) belongs to the cluster
	"""

	global getInWhichClusterLabelFileName

	dataPerCluster = dict()
	for idx in range(numOfCluster):
		labelFileName = os.path.join(labeldir, getInWhichClusterLabelFileName((numOfCluster, idx)))
		dataLst = getDataNameInCluster(labelFileName)
		dataPerCluster[idx] = dataLst
	
	
	return dataPerCluster


def loadDatas(datadir, fileName):
	"""
	read data from a specified fileName and return data as 1-d arrray
	"""
	if (datadir not in fileName):
		fileName = os.path.join(datadir, fileName)

	flatten_datas = np.asarray([np.float32(row) for row in csv.reader(open(fileName))]).flatten()
	print flatten_datas.shape
	datas = np.asarray([np.asarray([val, np.power(val, 2)]) for (idx, val) in enumerate(flatten_datas)])
	print datas.shape
#	datas = np.asarray([np.float32(row) for row in csv.reader(open(fileName))])
#	datas = datas.flatten()
	return datas


def loadFeatureData(datadir, fileName):
	"""
	Load feature data
	"""
	if (datadir not in fileName):
		fileName = os.path.join(datadir, fileName)

	datas = np.asarray([np.float32(row) for row in csv.reader(open(fileName))])
	numOfFeatures = datas.shape[0]

	#get upper triangle part of datas
	#triu_datas = np.triu(datas)
	only_upper_tri = list()
	for aidx in range(numOfFeatures):
		only_upper_tri.extend(list(datas[aidx][aidx:numOfFeatures]))

	return only_upper_tri



def getTrainingAndTestingSet(datas, labels):
	"""
	return training(0.8) and test(0.2) data set
	"""
	numOfDatas = len(labels)
	numOfTraining = np.int32(numOfDatas * 0.8)
	numOfTest = numOfDatas - numOfTraining

	training_indice = random.sample(range(numOfDatas), numOfTraining)
	test_indice = list(set(range(numOfDatas)) - set(training_indice))
	trainingDataSet = []; trainingLabelSet = []
	testDataSet = []; testLabelSet = []
	for idx in range(numOfDatas):
		if (idx in training_indice):
			trainingDataSet.append(datas[idx])
			trainingLabelSet.append(labels[idx])
		else:
			testDataSet.append(datas[idx])
			testLabelSet.append(labels[idx])

#	trainingDataSet = np.reshape(trainingDataSet, (-1, 1))
	trainingDataSet = np.asarray(trainingDataSet)
	trainingLabelSet = np.asarray(trainingLabelSet)
#	testDataSet = np.reshape(testDataSet, (-1, 1))
	testDataSet = np.asarray(testDataSet)
	testLabelSet = np.asarray(testLabelSet)
	return ((trainingDataSet, trainingLabelSet), (testDataSet, testLabelSet))
		

def applyGaussianProcess(trainingDataSet, trainingLabelSet, testDataSet, testLabelSet, numOfCluster, seed, kernel_type = "anisotropic"):
	"""
	apply gaussian process
	kernel_type can be either anisotropic or isotropic
	"""
	kernel_type = "isotropic"
	gpClfInst = gaussianProcessClf.GaussianProcessClf(kernel_type = kernel_type, random_state = seed)
	clf = gpClfInst.generateClassifier()
	clf.fit(trainingDataSet, trainingLabelSet)
	
	predicts = clf.predict(testDataSet)
	
	acc = accuracy_score(testLabelSet, predicts)
	prec = precision_score(testLabelSet, predicts, average = "macro")
	recall = recall_score(testLabelSet, predicts, average = "macro")
	f1 = f1_score(testLabelSet, predicts, average = "weighted")

	print "accuracy: " + str(acc)
	print "precision: " + str(prec)
	print "recall: " + str(recall)
	print "f1: " + str(f1)

	return (clf, f1)


def applyKNearestNeighbors(trainingDataSet, trainingLabelSet, testDataSet, testLabelSet, numOfCluster):
	"""
	apply random forest
	"""
	clf = KNeighborsClassifier(n_neighbors = 5, weights = 'uniform', algorithm = 'auto',
				leaf_size = 30, p = 2, metric = 'minkowski', metric_params = None)
	clf.fit(trainingDataSet, trainingLabelSet)
	
	predicts = clf.predict(testDataSet)
	
	acc = accuracy_score(testLabelSet, predicts)
	prec = precision_score(testLabelSet, predicts, average = "macro")
	recall = recall_score(testLabelSet, predicts, average = "macro")
	f1 = f1_score(testLabelSet, predicts, average = "macro")
	
	print "accuracy: " + str(acc)
	print "precision: " + str(prec)
	print "recall: " + str(recall)
	print "f1: " + str(f1)
	
	return (clf, f1)


def applyRandomForest(trainingDataSet, trainingLabelSet, testDataSet, testLabelSet, numOfCluster, seed):
	"""
	apply random forest
	"""
	clf = RandomForestClassifier(n_estimators = 30, max_depth = None, min_samples_split = 2, random_state = seed)
	clf.fit(trainingDataSet, trainingLabelSet)
	
	predicts = clf.predict(testDataSet)
	
	acc = accuracy_score(testLabelSet, predicts)
	prec = precision_score(testLabelSet, predicts, average = "macro")
	recall = recall_score(testLabelSet, predicts, average = "macro")
	f1 = f1_score(testLabelSet, predicts, average = "macro")

	print "accuracy: " + str(acc)
	print "precision: " + str(prec)
	print "recall: " + str(recall)
	print "f1: " + str(f1)	
	
	return (clf, f1)

def applyAdaboost(trainingDataSet, trainingLabelSet, testDataSet, testLabelSet, numOfCluster, seed):
	"""
	apply adaboost classifier to a given datas
	use a default values except n_estimators and algorithm:
		base_estimator: decision tree classifier
		n_estimators: 200 instead of 5
		learning_rate: 1
		algorithm: SAMME instead of SAMME.R {SAMME, SAMME.R}
		random_state: int
	"""

	clf = AdaBoostClassifier(n_estimators = 30, algorithm = "SAMME", random_state = seed)
	clf.fit(trainingDataSet, trainingLabelSet)

	predicts = clf.predict(testDataSet)
	#predicts = np.asarray(list(predicts))
	acc = accuracy_score(testLabelSet, predicts)
	prec = precision_score(testLabelSet, predicts, average = "macro")
	recall = recall_score(testLabelSet, predicts, average = "macro")
	f1 = f1_score(testLabelSet, predicts, average = "macro")

	print "accuracy: " + str(acc)
	print "precision: " + str(prec)
	print "recall: " + str(recall)
	print "f1: " + str(f1)
	
	return (clf, f1)
	

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("-datadir", action = "store", default = None)
	parser.add_argument("-labeldir", action = "store", default = None)
	parser.add_argument("-dest", action = "store", default = None)
	parser.add_argument("-numOfCluster", action = "store", default = 2, type = int)
	parser.add_argument("-corrType", action = "store", default = "Pear", help = "Pear, Spear")	
	args = parser.parse_args()
	
	print args.datadir
	print args.corrType
	print "======================================"


	dataPerCluster = getInWhichCluster(args.labeldir, args.numOfCluster)

	entireDatas = list(); entireLabels = list()
	fileNames = os.listdir(args.datadir)
	for fileName in fileNames:
		match = re.search("([^\.]+)." + args.corrType + ".feature.pvals.csv", fileName)
		if (match):
			fileId = match.group(1)
		else:
			continue
		datas = loadFeatureData(args.datadir, fileName)	
		entireDatas.append(datas)
		#get label of current data
		label = None
		for idx in range(args.numOfCluster):
			if (inThisCluster(dataPerCluster[idx], fileId)):
				label = idx
			else:
				continue
		if (label is None):
			print fileId + " is not in any of the clusters"
			sys.exit()
		entireLabels.append(label)
	
	entireDatas = np.asarray(entireDatas)
	entireLabels = np.asarray(entireLabels)

	((trainingDataSet, trainingLabelSet), (testDataSet, testLabelSet)) = getTrainingAndTestingSet(entireDatas, entireLabels)
	print "==RANDOMFOREST=="
	applyRandomForest(trainingDataSet, trainingLabelSet, testDataSet, testLabelSet, args.numOfCluster)
	print "==ADABOOST=="
	applyAdaboost(trainingDataSet, trainingLabelSet, testDataSet, testLabelSet, args.numOfCluster)

#	dataFileName = getDataFileName(args.corrType)
#	datas = loadDatas(args.datadir, dataFileName)
	#normalizedDatas = apply_min_max_scaler(datas)

#	labels = getLabel(args.labeldir, args.numOfCluster)	

#	((trainingDataSet, trainingLabelSet), (testDataSet, testLabelSet)) = getTrainingAndTestingSet(datas, labels)
	print trainingDataSet.shape, trainingLabelSet.shape
	#sys.exit()
#	applyAdaboost(trainingDataSet, trainingLabelSet, testDataSet, testLabelSet, args.numOfCluster)

	print "====================================="


