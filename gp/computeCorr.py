import numpy as np
import sys
from scipy.stats import pearsonr
from scipy.stats import spearmanr
from scipy.stats import rankdata

def computeSpearmanCorr(afeature, bfeature):
	rankAstd = np.std(rankdata(afeature))
	rankBstd = np.std(rankdata(bfeature))
	
	if (rankAstd == 0 or rankBstd == 0):
		if (rankAstd == 0 and rankBstd == 0):
			rho = 1.0; pval = 0.0
		else:#std of only one of them is 0.0
			rho = 0.0; pval = 1.0

	else:
		rho, pval = spearmanr(afeature, bfeature)
	
	if (np.isnan(pval)):
		print "std: " + str(rankAstd)
		print "std: " + str(rankBstd)
		sys.exit()

	return (rho, pval)


def computePearsonCorr(afeature, bfeature, top10 = False):
	astd = np.std(afeature)
	bstd = np.std(bfeature)
	
	if (astd == 0 or bstd == 0):
		if (astd == 0 and bstd == 0):
			return (0.0, 1.0)
		else:
			return (1.0, 0.0)
	else:
		(corr, pval) = pearsonr(afeature, bfeature)

	return (corr, pval)
