#!/usr/bin/python
"""
Evaluate given formulas with data in specified data directory
"""
import pickle, os, sys
import time
import csv
import re
import argparse
from scipy.stats import rankdata
import numpy
from gpOperators import *
parent_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir)
sys.path.insert(0, os.path.join(parent_dir, 'feature'))
import features


#read formulafile and return the target formula with appending of [tid] for each used feature name
def readFormula(formulaFile, featureInst):
	csvReader = csv.reader(open(formulaFile))
	row = csvReader.next()
	formula = "".join(row[:-1])
	if (formula.startswith('"')):
		formula = formula[1:]
	if (formula.endswith('"')):
		formula = formula[:-1]

	score = numpy.float32(row[-1])
	usedFeatures = featureInst.getAllFeatures()
	return (formula, score)


def convFormulaForGP(formula, featureInst):
    for feature in featureInst.getAllFeatures():
            formula = formula.replace(feature, feature + "[tid]")
    return formula


def record(fileName, datas):
	csvWriter = csv.writer(open(fileName, "a"))

	for data in datas:
		csvWriter.writerow([data])


#return evaluation file list paired with its fold id
def getFileList(datadir, pairFile):
	files = dict()
	csvReader = csv.reader(open(pairFile))
	foldId = 0
	for (idx, row) in enumerate(csvReader):
		if (idx % 3 == 0):
			foldId = row[0]
		elif (idx % 3 == 2):
			files[int(foldId)] = [os.path.join(datadir, fileName) for fileName in row]
		else:
			continue
	return files


def extract_faultId(dataFile):
	pattern = "([^\/]+)\.dat"
	faultId = re.search(pattern, dataFile).group(1)
	return faultId


#datadir: directory which contains evaluation data filess
#evalDataFiles: a list of evaluation data filename i.e. Lang_5.dat
#postfix: (i.e. min.GP, med.GP)
#formula: a target formula
#dest: directory where evaluation results will be stored
def evaluate(datadir, evalDataFiles, formula, dest, postfix, tiebreak, featureInst, cudaFileDir, kernelFileName = None, use_gpu = False, with_record = True):
	#initialze evaluation data
	evaluationDatas = list()
	faultIds = list()
	#faultIndiceLst = list()

	for dataFile in evalDataFiles:
                if (datadir not in dataFile):
                    dataFile = os.path.join(datadir, dataFile)
		evalData = pickle.load(open(dataFile))
		#extract fault id part from data file name
		#pattern = "([^\/]+)\.dat"
		#faultId = re.search(pattern, dataFile).group(1)
		faultId = extract_faultId(dataFile)

		evaluationDatas.append((faultId, evalData))

        arg_dict = None
	if (use_gpu):
                #import cudaconv for using gpu and make it as global
                globals()['cudaconv'] = __import__('cudaconv')
                formula = convFormulaForGP(formula, featureInst)
                print formula
                #find out the name of kernel file to use by looking at the Features instance
                if (kernelFileName is None):
                    if (not featureInst.isClustered):
                        kernelFileName = os.path.join(cudaFileDir, "kernel_template_basic.cuda")
                    else:
                        kernelFileName = os.path.join(cudaFileDir, "kernel_template_F_" + str(featureInst.getSize()) + ".cuda")

		eval_cudatool = cudaconv.CUDATool(featureIndice = range(1, featureInst.getSize() + 1), kernelFileName = os.path.join(cudaFileDir,  kernelFileName))
                onlyDatas = [data for (_, data) in evaluationDatas]
		eval_cudatool.mem_cp(onlyDatas)

	else:
		for (featureIdx, featureName) in enumerate(featureInst.getAllFeatures()):
			#will access evaluation data by its feature name and index(idx) to the method
			formula = formula.replace(featureName, "_features['" + featureName + "'][idx]")

	#dict_rank: key is faultId and value is a list of computed rank for each method
	dict_ranksAndIndice = {}
	for (eIdx, (faultId, evalData)) in enumerate(evaluationDatas):
		faultIndice = None; ranks = []; faultyRanks = []
		if (use_gpu):
			faultIndice = evalData[0]
			eval_cudatool.Compile([formula])
			suspVals = eval_cudatool.core(eIdx, 0)
			ranks = rankdata(suspVals, method = tiebreak)
			for faultIndex in faultIndice:
				faultyRanks.append(ranks[faultIndex])
		else:
			_features = dict()
			(faultIndice, vectors) = evalData
			print formula
			##
			#headers = [v[0] for v in vectors]
			##
			for (featureIdx, featureName) in enumerate(featureInst.getAllFeatures()):
				_features[featureName] = [numpy.float32(v[featureIdx + 1]) for v in vectors]

			suspVals = numpy.array([0.0] * len(vectors), dtype=numpy.float32)
			for idx in range(len(vectors)):
				suspVals[idx] = - eval(formula)
			##
			#csvWriter = csv.writer(open(faultId + ".susp.scores.csv", "w"))
			#for (header, suspVal) in zip(headers, suspVals):
			#	csvWriter.writerow([header, -suspVal])
			#sys.exit()
			##
			ranks = rankdata(suspVals, method = tiebreak)

			#retrieve ranks for faulty elements
			for faultIndex in faultIndice:
				faultyRanks.append(ranks[faultIndex])

		#write the result if with_record flag is True
		if (with_record):
			rankFile = os.path.join(dest, faultId + "." + postfix + ".ranks.csv")
			faultRankFile = os.path.join(dest, faultId + "." + postfix + ".faults.csv")
			record(rankFile, ranks)
			record(faultRankFile, faultyRanks)
		dict_ranksAndIndice[faultId] = (ranks, faultIndice)

	return dict_ranksAndIndice
	

def getMajorMinorFileName(file_header, foldId):
	majorFileName = file_header + "." + str(foldId) + ".major.result.csv"
	minorFileName = file_header + "." + str(foldId) + ".minor.result.csv"
	return (majorFileName, minorFileName)


if __name__ == "__main__":
	#Parse arguments from command line
	parser = argparse.ArgumentParser()

	parser.add_argument("-dest", action = "store", default = ".")
	parser.add_argument("-postfix", action = "store", default = None)
	parser.add_argument("-usegpu", action = "store", default = 0, type = int)
	parser.add_argument("-tiebreak", action = "store", default = "max")
	parser.add_argument("-formuladir", action = "store", default = None, help = "a directory where formula files(*.result.csv) are stored")
	parser.add_argument("-datadir", action = "store", default = None)
	parser.add_argument("-prefix", action = "store", default = None, help = "i.e. iterId.Perfault.pval.top10.Spear.foldId")
	parser.add_argument("-cInfoF", action = "store", default = None, help = "a file contain feature clustering info")
	parser.add_argument("-pairFile", action = "store", default = None, help = "a location of pair.txt file")
	parser.add_argument("-iterNum", action = "store", default = 30, type = int, help = "a number of iterations")
	parser.add_argument("-recordAll", action = "store", default = 0, type = int, help = "1 if results for all 30 formulas should be recorded else 0(only min and med)")
        parser.add_argument("-cudaFileDir", action = "store", default = os.path.dirname(os.path.abspath(__file__)), help = "a directory where cuda related files are stored")
	parser.add_argument("-num_of_fold", action = "store", default = 10, type = int, help = "the number of folds to use for k(arg)-fold cross validation");

	args = parser.parse_args()

	t = time.time()
	print "start"
	print t

	tiebreak_lst = ["max", "ordinal", "average", "dense", "min"]
	tiebreak = "max"
	evaluationFiles = getFileList(args.datadir, args.pairFile)
	featureInst = features.Features(args.cInfoF, k = 1)

	for foldId in range(args.num_of_fold):
		minFormula = None; medFormula = None
		formulas = []
		for iterIdx in range(args.iterNum):
			if (args.prefix is None):
				formulaFile = str(iterIdx) + "." + str(foldId) + ".result.csv"
			else:
				formulaFile = str(iterIdx) + "." + args.prefix + "." + str(foldId) + ".result.csv"
			(formula, score) = readFormula(os.path.join(args.formuladir, formulaFile), featureInst)
			formulas.append((formula, score))
			print formulaFile
			print formula
			if (args.recordAll == 1):
				evaluate(evaluationFiles[foldId], formula, args.dest, args.postfix + "." + str(iterIdx), tiebreak, featureInst, args.cudaFileDir, use_gpu = args.usegpu)

		#if args.recordAll is 0, evaluate for median and min performance formula
		if (args.recordAll == 0):
			sortedFormulas = sorted(formulas, key=lambda x: x[1])
			(minFormula, minScore) = sortedFormulas[0]
			(medFormula, medScore) = sortedFormulas[int(len(sortedFormulas) / 2)]

			#evaluate with min and median scored formulae

			evaluate(evaluationFiles[foldId], minFormula, args.dest, args.postfix + ".min", tiebreak, featureInst, args.cudaFileDir, use_gpu = args.usegpu)
			evaluate(evaluationFiles[foldId], medFormula, args.dest, args.postfix + ".med", tiebreak, featureInst, args.cudaFileDir, use_gpu = args.usegpu)



	t2 = time.time()
	print "end"
	print t2
	print "total: "
	print "{0} seconds".format(t2 -t)
