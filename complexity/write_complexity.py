#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------
"""
gather complexity metrics per method.
"""
import sys
import os
import argparse

parent_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir)
sys.path.insert(0, os.path.join(parent_dir, 'python'))
import classify_stmt
import code_change_metrics
import parser_check
#import targetMethod

script_dir = os.path.dirname(os.path.abspath(__file__))

#write new method complexity data: method_complexity.csv
def _write_complexity(pid, bid, complexities, resultdir):
#	targetMethodInfoHandler = targetMethod.TargetMethodInfo(pid, bid)

	handler = open(os.path.join(resultdir, "method_complexity.csv"), "w")
	for method in complexities.methods.values():
		if (method.in_stmt):
			candKey = method.class_in + "$" + method.name 
			#if (targetMethodInfoHandler.isTarget(candKey)):
			handler.write(candKey + "," + str(method.num_of_args) + "," + str(method.num_of_local_vars) + "," + str(method.instr_length) + "," + str(len(method.statements)) + "\n")
	handler.close()


if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("-pid", action = "store", default = None)
	parser.add_argument("-bid", action = "store", default = None)
	parser.add_argument("-resultdir", action = "store", default = None)

	args = parser.parse_args()

	parser_check.argument_check(parser, args_name_list = ["pid", "bid", "resultdir"], args_val_list = [args.pid, args.bid, args.resultdir])

	bceldatadir = os.path.join(os.path.abspath(os.path.join(script_dir, os.pardir)), "bcel/output")

	complexities = code_change_metrics.CG_Metrics(bceldatadir, args.pid, args.bid, "complexity")
	classify_stmt._classify_statements_(args.pid, args.bid, complexities)
	_write_complexity(args.pid, args.bid, complexities, args.resultdir)


