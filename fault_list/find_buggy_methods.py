#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------
"""
find the location of the fault using diff operator; the location of fault is determined from patches provided by Defects4J.
"""
import sys
import os
import subprocess
import re
import argparse

parent_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir)
sys.path.insert(0, os.path.join(parent_dir, 'python'))
import find_out_origin
import parser_check
import Migrate_SVN2GIT
import Get_Revision

loc = None

#find out defects4j's home
def find_D4J_HOME():
	loc = os.environ.get('D4J_HOME')
	if (loc == None):
		print("There is no D4J_HOME environment variable. Please check it\n")
		sys.exit()
	return loc 


#find out buggy files
#return list of bug files with relative location of package
def find_buggy_file(pid, bid):
	global loc
	target_patch = os.path.join(loc, pid + "/patches/" + bid + ".src.patch")
	handler = open(target_patch, "r")
	bug_files = []
	while True:
		bug_file = None
		line = handler.readline()
		if not line: break
		matched = re.search("^diff --git a\/([^\s]*)\s*b\/.*", line)
		if (bool(matched)):
			bug_file = matched.group(1)
		else:
			matched = re.search("Index:\s+([^\s]*\.java).*", line)
			if (bool(matched)):
				bug_file = matched.group(1)
			else:
				continue

		if (pid == "Lang"):
			bug_file = re.search(".+(org\/apache\/commons\/lang.+)$", bug_file).group(1)
		elif (pid == "Math"):
			bug_file = re.search(".+(org\/apache\/commons\/math.+)$", bug_file).group(1)
		elif (pid == "Closure"):
			bug_file = re.search(".+(com\/google.+)$", bug_file).group(1)
		elif (pid == "Time"):
			bug_file = re.search(".+(org\/joda\/time.+)$", bug_file).group(1)
		elif (pid == "Chart"):
			bug_file = re.search(".+(org\/jfree\/chart.+)$", bug_file).group(1)		
		elif (pid == "Mockito"):
			bug_file = re.search(".+(org\/mockito.+)$", bug_file).group(1)
		else:
			print "Wrong pid " + pid + "\n"
			sys.exit()

		bug_file.rstrip()
		bug_files.append(bug_file)
	
	handler.close()
	return bug_files



#find out revision id of buggy and fixed version
def find_fixed_buggy(pid, bid):
	global loc
	target_commit_db = os.path.join(loc, pid + "/commit-db")
	handler = open(target_commit_db, "r")
	fixed_buggy = dict()
	while True:
		fix_ver = None; bug_ver = None
		line = handler.readline()
		if not line: break
		results = None	
		try:
			results = re.search(bid + ",([^,]*),(.*)", line).groups()
		except:
			continue
		fixed_buggy["buggy"] = results[0]
		fixed_buggy["fixed"] = results[1]
		break

	handler.close()
	return fixed_buggy


#find file with full path in the revision
def find_path(revision, file_name):
	cmd = "git ls-tree -r --name-only " + revision + " | grep '" + file_name + "'"
	proc = subprocess.Popen(cmd.rstrip(), stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell = True)
	(std_out, std_err) = proc.communicate()
	matched_files = std_out.split("\n")
	find_file = matched_files[0]
	return find_file


#find out faulty methods
#base_dir is the full path of working directory
def find_buggy_methods(base_dir, pid, bid):
	bug_files = find_buggy_file(pid, bid)
	(b_revid, f_revid) = Get_Revision.get_buggy_fix_revid(pid, bid)

	if (pid == "Chart"):
		#do not need an absoulte path for svn_dir --> _
		(b_revid, _) = Migrate_SVN2GIT.convert_svn_rev2git_rev(pid, b_revid, None)
		(f_revid, _) = Migrate_SVN2GIT.convert_svn_rev2git_rev(pid, f_revid, None)	

	bug_methods = []
	if (bool(bug_files)):
		os.chdir(base_dir)
		#for each bug file
		for bug_file in bug_files:
			check_in = 0
			check_need = 1

			file_fixed = find_path(f_revid, bug_file)
			file_bugged = find_path(b_revid, bug_file)
			cmd = "git diff -u " + b_revid + ":" + file_bugged + " " + f_revid + ":" + file_fixed
			proc = subprocess.Popen(cmd.rstrip(), stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell = True)
			(std_out, std_err) = proc.communicate()
			line_numbers = []
			if (bool(std_out)):#Has difference, which should be
				lines = std_out.split("\n")
				start_line_number = None; line_number = None; new_start = False
				#find out the start of buggy lines
				for line in lines:
					#if meet @@ start_line_number is newly set and also line_number
					try:
						match_result = re.search("^@@ -([^,]*),([^\+\s]*)\s+\+.*", line)
						start_line_number = int(match_result.group(1))
						number_of_lines = int(match_result.group(2))
						new_start = True
						check_in = 1; line_number = start_line_number
						
						#add line numbers in range of diff results
						for x in range(start_line_number, start_line_number + number_of_lines):
							line_numbers.append(x)

						continue#after start number is set, no need to process the line more
					except:
						new_start = None
		
	
				for line_number in line_numbers:
					class_name = re.search("(.*)\.java", bug_file).group(1)
					class_name = re.sub("/", ".", class_name)
					origin = find_out_origin.from_which(pid, bid, class_name, line_number)
					if (bool(origin)):
						buggy_method = class_name
						for e in origin:
							buggy_method += "$" + e
						bug_methods.append(buggy_method)
			else:
				print "no difference between buggy and fixed version for " + bug_file +".\n"
				sys.exit()
		
		bug_methods = list(set(bug_methods))
	else:
		print "There are no buggy files for %s's %s version" % (pid, bid)
		print "Look at " + os.path.join(loc, pid + "/patches/" + bid + ".src.patch")
		sys.exit()
	return bug_methods


if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("-workdir", action = "store", default = None)
	parser.add_argument("-pid", action = "store", default = None)
	parser.add_argument("-bid", action = "store", default = None)

	args = parser.parse_args()

	parser_check.argument_check(parser, args_name_list = ["pid", "bid", "workdir"], args_val_list = [args.pid, args.bid, args.workdir])
	loc = os.path.join(find_D4J_HOME(), "framework/projects")
	resultdir = os.path.join(find_D4J_HOME(), "framework/bin/fluccs/fault_list/" + args.pid)

	workdir = args.workdir if (args.pid != "Chart") else os.environ['D4J_HOME'] + "/framework/bin/fluccs/checkout/Chart/svnChart"
	bug_methods = find_buggy_methods(workdir, args.pid, args.bid)

	handler = open(os.path.join(resultdir, "buggy_methods_" + args.pid + "_" +  args.bid + "_list.csv"), "w")
	for bug_method in bug_methods:
		handler.write(bug_method + "\n")
	handler.close()

