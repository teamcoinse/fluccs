import subprocess
import os, sys
import shutil
import re

D4J_HOME = os.environ['D4J_HOME']

#migrate work_dir(svn) to git 
def migrate_svn2git(pid, svn_dir, work_dir):
	global D4J_HOME
	if (pid != 'Chart'):
		print "%s does not need the migration from svn to git" % (pid)
		sys.exit()

	svn_parent_dir = os.path.join(D4J_HOME, "framework/bin/fluccs/checkout/Chart")
	if (svn_parent_dir == os.path.abspath(os.path.join(svn_dir, os.pardir))):
		shutil.rmtree(svn_dir)
		os.mkdir(svn_dir)
	else:
		print "There is something wrong in a given svn_dir: %s\nStop the process" % (svn_dir)
		sys.exit()
	current_dir = os.getcwd()

	cmd = "cd %s; git init" % (svn_dir)
	proc = subprocess.Popen(cmd.rstrip(), stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell = True)
	(_, std_err) = proc.communicate()
	if (not (not std_err)): print "problem in executing " + cmd; sys.exit()
	print cmd
	svn_url = get_svn_url(work_dir)
	print svn_url	
	cmd = "cd %s; git svn init %s --stdlayout --prefix=svn/; git svn fetch" % (svn_dir, svn_url)
	print cmd
	proc = subprocess.Popen(cmd.rstrip(), stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell = True)
	(std_out, std_err) = proc.communicate()
	print std_out
	print "Migration between svn and git repositories succeed"
	os.chdir(current_dir)	


#get svn url fo a given directory
def get_svn_url(work_dir):
	cmd = "svn info %s --show-item repos-root-url" % (work_dir)
	proc = subprocess.Popen(cmd.rstrip(), stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell = True)
	(std_out, std_err) = proc.communicate()
	if (not (not std_err) or len(std_out) == 0): 
		print "problem in executing " + cmd
		print "Error message:\n\t%s" % (std_err)
		print "Exit code is %d" % (proc.returncode)
		sys.exit()

	svn_url = std_out.rstrip()
	return svn_url	

#check git svn info
def check_git_svn_dir(svn_dir):
	if (not os.path.exists(svn_dir)):
		os.mkdir(svn_dir)
		return False
	else:
		current_dir = os.getcwd()
		os.chdir(svn_dir)
		cmd = "git svn info"
		print cmd
		proc = subprocess.Popen(cmd.rstrip(), stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell = True)
		(_, std_err) = proc.communicate()
		os.chdir(current_dir)
		if (not (not std_err)):
			if (bool(re.match("Wide character in print.*git-svn",std_err))):#can be ignored: perl --> 'use encoding utf8' can fix this
				return True
			else:
				print "WHILE CHECKING SVN WORKING DIRECTORY:\n \t%s" % (std_err)
				return False
		else:
			return True


#return matching git revision id with a given svn revision id
def convert_svn_rev2git_rev(pid, svn_rev, work_dir):
	current_dir = os.getcwd()
	svn_dir = os.environ['D4J_HOME'] + "/framework/bin/fluccs/checkout/Chart/svnChart"

	if (not check_git_svn_dir(svn_dir) and work_dir is not None):
		migrate_svn2git(pid, svn_dir, work_dir)
	os.chdir(svn_dir)
	cmd = "git svn find-rev r" + str(svn_rev)
	print cmd
	proc = subprocess.Popen(cmd.rstrip(), stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell = True)
	(std_out, std_err) = proc.communicate()
	print std_out
	if (not (not std_err)):
		print "problem in executing " + cmd; os.chdir(current_dir); sys.exit()
	elif (len(std_out) == 0):
		print "Not a valid svn rev " + str(svn_rev)
		os.chdir(current_dir); sys.exit()
	else:
		git_rev = std_out.rstrip()
		os.chdir(current_dir)
		return (git_rev, svn_dir)	




