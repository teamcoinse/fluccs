#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------
"""
find out which method does a given statement, which is specified by class_name and line_number, belong.
"""
import re

def from_which(class_name, line_number, from_infos):
	from_method = "none"
	from_sub_class = "none"
	class_loc = class_name.replace(".","/")
	find = 0

	if (class_loc in from_infos):
		if (from_infos.index(class_loc) == 0):#the start --> subclass exist 
			from_sub_class = re.search(re.escape(class_loc) + "\$(.+)\$[^\$]*\<", from_infos).group(1)
			from_method = re.search("\$([^\$]+\<.*\>)", from_infos).group(1)
			return (from_sub_class, from_method)
		

	from_method = from_infos
	return (from_method,)
	
