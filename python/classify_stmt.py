#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------
"""
classify statements using pairing files under method_stmt.
"""
import csv
import sys
import os
import Which_from

script_dir = os.path.dirname(os.path.abspath(__file__))


#classify statements to its belonging method - Method instance's statements
#use method_stmt/Lang|Closure|Math|Time/stmt_mth_Lang|Closure|Time|Math_bid.csv
#method_graph deals with only methods that are in the target list 
def _classify_statements_(pid, bid, method_graph):
	global script_dir
	loc = os.path.join(os.path.abspath(os.path.join(script_dir, os.pardir)), "method_stmt/" + pid)
	handler = open(os.path.join(loc, "stmt_mth_" + pid + "_" + bid + ".csv"), "r")
	csvReader = csv.reader(handler, delimiter = ",")
	datas = list(tuple(row) for row in csvReader)
	handler.close()
	
	#retrieve a list of target methods
	targetMethodIds = method_graph.methods.keys()
	for data in datas:
		class_name = data[0]
		line_number = data[1]
		from_infos = data[2]
		origin = Which_from.from_which(class_name, line_number, from_infos)
		
		if (origin == None):
			print "invalid statement... error in stmt_method file"
			sys.exit()
		else:
			if (len(origin) == 2):#subclass, methodname
				methodId = class_name + "$" + origin[0] + "$" + origin[1]
				if (methodId in targetMethodIds):
					method_graph.methods[methodId]._init_statement_(line_number)
					method_graph.methods[methodId].in_stmt = 1
			elif (len(origin) == 1):#methodname
				methodId = class_name + "$" + origin[0]
				if (methodId in targetMethodIds):
					method_graph.methods[methodId]._init_statement_(line_number)	
					method_graph.methods[methodId].in_stmt = 1
			else:
				print "invalid number of origin inofs... error in stmt_method file"
				print str(len(origin)) 
				print origin
				sys.exit()

