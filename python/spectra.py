#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------
"""
class for spectra.
"""
import os
import csv

class Spectra:
	def __init__(self, loc):
		self.loc = loc
		self.stmt_spectra = {}
		self._set_spectra()

	def _set_spectra(self):
		handler = open(os.path.join(self.loc, "spectra.all.csv"), "r")
		csvReader = csv.reader(handler, delimiter = ",")
		sp_datas = list(tuple(row) for row in csvReader)
		handler.close()

		for sp_data in sp_datas:#data = [class, linenumber, ep, np, ef, nf]
			self.stmt_spectra[sp_data[0] + "$" + sp_data[1]] = [sp_data[2], sp_data[3], sp_data[4], sp_data[5]]
		
