#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------
"""
class for method per churn, including call graph propagation operations.
"""
import numpy

class Method:
	def __init__(self, name, churn):
		#class$method$method_args
		self.name = name

		self.calls = []
		self.churn = churn
		self.CG_min_churn = 0.0
		self.CG_max_churn = 0.0
		self.CG_mean_churn = 0.0

	#called is a Method instance
	def _add_(self, called):
		self.calls.append(called)

	def _gather_calls_(self, caller = None):
		if (caller == None):
			caller = [self.name,]
		else:
			caller.append(self.name)
		
		churn_list = [float(self.churn),]

		if (not self.calls):
			return churn_list
		else:
			for call in self.calls:
				call_name = call.name
				if (call_name in caller):
					continue
				else:	
					churn_list.extend(call._gather_calls_(caller))
			return churn_list
				
	def _min_(self, method_churns):
		min_churn = method_churns[0]
		for churn in method_churns:
			if (min_churn > churn and churn >= 0):
				min_churn = churn
		return min_churn

	
	def _mean_(self, method_churns):
		m_list = [method_churns[0]]
		for churn in method_churns:
			if (churn >= 0):
				m_list.append(churn)
		return numpy.mean(m_list)
		

	#calculate CG considered method churn	
	def _cal_cg_method_churn_(self):
		gathered_churns = self._gather_calls_()
		self.CG_min_churn = self.churn
		if (len(gathered_churns) > 1):
			self.CG_min_churn = self._min_(gathered_churns)
		
		self.CG_max_churn = self.churn
		if (len(gathered_churns) > 1):
			self.CG_max_churn = numpy.max(gathered_churns)
		
		self.CG_mean_churn = self.churn
		if (len(gathered_churns) > 1):
			self.CG_mean_churn = self._mean_(gathered_churns)
		











	
