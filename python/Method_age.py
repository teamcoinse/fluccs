#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------
"""
class for method per age, including call graph propagation operations. 
"""
import sys
import numpy

class Method:
	def __init__(self, class_in, name):
		self.class_in = class_in
		self.name = name

		self.calls = []
		self.CG_min_age = 0
		self.CG_max_age = 0
		self.CG_mean_age = 0

		self.min_age = 0
		self.max_age = 0
		self.mean_age = 0

		self.statements = dict()
		self.spectras = dict()
		self.method_spectra = []	
		self.stmts_cal_min_age = dict()
		self.stmts_cal_max_age = dict()
		self.stmts_cal_mean_age = dict()
		self.stmts_cal_mean_stmt_age = dict()

		self.in_stmt = 0

	#called is a Method instance
	def _add_(self, called):
		self.calls.append(called)

	def _init_statement_(self, linenumber):
		self.statements[linenumber] = -1
		self.stmts_cal_min_age[linenumber] = -1
		self.stmts_cal_max_age[linenumber] = -1
		self.stmts_cal_mean_age[linenumber] = -1
		self.stmts_cal_mean_stmt_age[linenumber] = -1

	def _set_statement_(self, linenumber, age):
		self.statements[linenumber] = long(age) 	

	def _init_spectra_(self, linenumber):
		self.spectras[linenumber] = []

	def _set_spectra_(self, linenumber, spectra):
		self.spectras[linenumber] = spectra	
	
	#Calculate the age(without call graph) for the method
	def _cal_age_(self):
		ages_list = []
		for age in self.statements.values():
			if (age >= 0):
				ages_list.append(age)
		if (not ages_list):
			self.min_age = -1; self.max_age = -1; self.mean_age = -1
		else:
			self.min_age = numpy.min(ages_list)
			self.max_age = numpy.max(ages_list)
			self.mean_age = numpy.mean(ages_list)

	
	def _gather_calls_(self, mode, caller = None):
		if (caller == None):
			caller = [self.class_in + "$" + self.name,]
		else:
			caller.append(self.class_in + "$" + self.name)
		
		if (mode == "min"):
			ages_list = [self.min_age,]	
		elif (mode == "max"):
			ages_list = [self.max_age,]
		elif (mode == "mean"):
			ages_list = [self.mean_age,]
		else:
			print "Wrong mode " + mode
			sys.exit()

		if (not self.calls):
			return ages_list
		else:
			for call in self.calls:
				call_name = call.class_in + "$" + call.name
				if (call_name in caller):
					continue
				else:	
					ages_list.extend(call._gather_calls_(mode, caller))
			return ages_list
				
	def _min_(self, method_ages):
		min_age = method_ages[0]
		for age in method_ages:
			if (min_age > age and age >= 0):
				min_age = age	
		return min_age

	
	def _mean_(self, method_ages):
		m_list = [method_ages[0]]
		for age in method_ages:
			if (age >= 0):
				m_list.append(age)
		return numpy.mean(m_list)
		

	#calculate CG considered method age	
	def _cal_cg_method_age_(self):
		self.CG_min_age = self.min_age
		method_min_ages = [self.min_age,]
		method_min_ages = self._gather_calls_("min")
		if (len(method_min_ages) > 1):
			self.CG_min_age = self._min_(method_min_ages)
		
		self.CG_max_age = self.max_age
		method_max_ages = [self.max_age,]
		method_max_ages = self._gather_calls_("max")
		if (len(method_max_ages) > 1):
			self.CG_max_age = numpy.max(method_max_ages)
		
		self.CG_mean_age = self.mean_age
		method_mean_ages = [self.mean_age,]
		method_mean_ages = self._gather_calls_("mean")
		if (len(method_mean_ages) > 1):
			self.CG_mean_age = self._mean_(method_mean_ages)
		
	#calculate CG considered statements age	
	def _cal_stmts_age_(self):
		min_called_age = -1
		max_called_age = -1
		mean_called_age = -1
		method_min_ages = [self.min_age,]
		method_max_ages = [self.max_age,]
		method_mean_ages = [self.mean_age,]
		
		method_min_ages = self._gather_calls_("min")
		method_max_ages = self._gather_calls_("max")
		method_mean_ages = self._gather_calls_("mean")
		if (len(method_min_ages) > 1):
			min_called_age = self._min_(method_min_ages)
		if (len(method_max_ages) > 1):
			max_called_age = numpy.max(method_max_ages)
		if (len(method_mean_ages) > 1):
			mean_called_age = self._mean_(method_mean_ages)
		
		for linenumber in self.statements.keys():
			if (self.statements[linenumber] >= 0 and min_called_age >= 0): 
				self.stmts_cal_min_age[linenumber] = min([long(self.statements[linenumber]),long(min_called_age)])
			elif (self.statements[linenumber] >= 0):
				self.stmts_cal_min_age[linenumber] = self.statements[linenumber]
			elif (min_called_age >= 0):
				self.stmts_cal_min_age[linenumber] = min_called_age
			else:
				self.stmts_cal_min_age[linenumber] = -1


			if (self.statements[linenumber] >= 0 and max_called_age >= 0):
				self.stmts_cal_max_age[linenumber] = max([long(self.statements[linenumber]),long(max_called_age)])
			elif (self.statements[linenumber] >= 0):
				self.stmts_cal_max_age[linenumber] = self.statements[linenumber]
			elif (max_called_age >= 0):
				self.stmts_cal_max_age[linenumber] = max_called_age 
			else:
				self.stmts_cal_max_age[linenumber] = -1


			if (self.statements[linenumber] >= 0 and mean_called_age >= 0):
				self.stmts_cal_mean_stmt_age[linenumber] = numpy.mean([long(self.statements[linenumber]),long(mean_called_age)])
				self.stmts_cal_mean_age[linenumber] = long(mean_called_age)
			elif (self.statements[linenumber] >= 0):
				self.stmts_cal_mean_age[linenumber] = self.statements[linenumber]
				self.stmts_cal_mean_stmt_age[linenumber] = self.statements[linenumber]
			elif (mean_called_age >= 0):
				self.stmts_cal_mean_age[linenumber] = mean_called_age
				self.stmts_cal_mean_stmt_age[linenumber] = mean_called_age
			else:
				self.stmts_cal_mean_age[linenumber] = -1
				self.stmts_cal_mean_stmt_age[linenumber] = -1











	
