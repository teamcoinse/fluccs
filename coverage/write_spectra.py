#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------
"""
gather statement's spectra per method; as a result, spectra for a method will consist of multiple 
spectra, each comes from a statement which belongs to the method.
"""
import sys
import os, csv
import argparse

parent_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir)
sys.path.insert(0, os.path.join(parent_dir, 'python'))
import Method_spectra
import spectra
import Which_from
import parser_check
import targetMethod

script_dir = os.path.dirname(os.path.abspath(__file__))

def _init_method_spectra(pid, bid):
	global script_dir
	loc = os.path.join(os.path.abspath(os.path.join(script_dir, os.pardir)), "method_stmt/" + pid)
	handler = open(os.path.join(loc, "stmt_mth_" + pid + "_" + bid + ".csv"), "r")
	print "pairing file: " + os.path.join(loc, "stmt_mth_" + pid + "_" + bid + ".csv")
	csvReader = csv.reader(handler, delimiter = ",")
	datas = list(tuple(row) for row in csvReader)
	handler.close()
	
	method_spectra = dict()
	#get target methods info
	targetMethodInfoHandler = targetMethod.TargetMethodInfo(pid, bid)

	#initialize method spectra
	for data in datas:
		class_name = data[0]; line_number = data[1]; from_infos = data[2]
		#find the origin of the line && if the origin is the pasrt of target, continue
		#else skip it
		origin = Which_from.from_which(class_name, line_number, from_infos)
		if (origin == None):
			print "invalid statement... error in stmt_method file"
			sys.exit()
		else:
			if (len(origin) == 2):#subclass, methodname<args>
				cand = class_name + "$" + origin[0] + "$" + origin[1]
			elif (len(origin) == 1):#methodname<args>
				cand = class_name + "$" + origin[0]
			else:
				print "invalid number of origin infos... error in stmt_method file"
				print str(len(origin))
				print origin
				sys.exit()

		if (targetMethodInfoHandler.isTarget(cand)):
			if (cand in list(method_spectra.keys())):#already exist
				method_spectra[cand]._init_spectra_(line_number)
			else:#new element
				method_spectra[cand] = Method_spectra.Method(class_name, from_infos)
				method_spectra[cand]._init_spectra_(line_number)

	#print "in initializing method spectra\n"
	return method_spectra

#gather spectra per method
def gather(spectra_list, method_spectra):
	#initialize method spectra
	for key in list(method_spectra.keys()):
		#setting spectra and age (spectra_age_list value) for statement spectra for method
		for linenumber in list(method_spectra[key].spectras.keys()):
			method_spectra[key]._set_spectra_(linenumber, spectra_list[method_spectra[key].class_in + "$" + linenumber])
	#print "in gathering spectra\n"
	return method_spectra


#write method spectra : method_id , stmt_1_spectra, stmt_2_spectra, ...
def write_new_method_spectra(method_spectra, resultdir):
	handler = open(os.path.join(resultdir, "method_spectra.csv"), "w")
	for method in list(method_spectra.keys()):
		line = method
		for spectra in method_spectra[method].spectras.values():
			for i in range(len(spectra)):
				line += "," + str(spectra[i])
		handler.write(line + "\n")
	handler.close()

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("-pid", action = "store", default = None)
	parser.add_argument("-bid", action = "store", default = None)
	parser.add_argument("-datadir", action = "store", default = None)
	parser.add_argument("-resultdir", action = "store", default = None)
	args = parser.parse_args()

	parser_check.argument_check(parser, args_name_list = ["datadir", "pid", "bid", "resultdir"], args_val_list = [args.datadir, args.pid, args.bid, args.resultdir])

	#dictionary of method instance: key is class$method<args>, value is Method instance
	method_spectra = _init_method_spectra(args.pid, args.bid)
	#dictionary of spectra for each statement: key is class$linenumber, value is spectra for the statement
	spectra_list = spectra.Spectra(args.datadir).stmt_spectra
	#generate method spectra using spectra list
	method_spectra = gather(spectra_list, method_spectra)
	#print ("start writing \n")
	write_new_method_spectra(method_spectra, args.resultdir)	
