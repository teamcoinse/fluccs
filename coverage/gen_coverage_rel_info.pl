#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------

=pod

=head1 NAME

gen_coverage_rel_info.pl -- performs junit tests and generates spectra for each line of the entire project

=head1 SYNOPSIS

	gen_coverage_rel_info.pl -p project_id -b bug_id -w working_directory -o output_file_id -c use_cobertura [-a] [-t test_class] [-m test_methods_list] 

=head1 OPTIONS

=over 7

=item -p C<project_id>

The id of the project for which is the target of testing and generating spectra.

=item -b F<bug_id>

Project bug id

=item -n C<id_of_testclasses_set>

The id of testclasses set. if only one testclass file exist, does not required.

=item -a C<all>

When option 'a' is given, will be executed for all testcases for test classes listed in testclass_list_<id_of_testclasses_set>.txt or testclasses_list.txt.

=item -t C<test_class>

The target test class.
Testcases for test class will be executed unless test methods list is specified.

=item -m <test_method_list>

The list of test methods to be executed.

=item -w F<working_directory>

The working directory

=item -o F<output_file_id>

The identifier of output file

=item -c F<use_cobertura>

1 if using cobertura as coverage tool else (use jacoco) 0

=back

=head1 Description

Tests testcases specified by the combination of options. Test will be performed per testcase.
After finishing the testing, generates spectra for each line of the codes for the target project.
JaCoCo is used for determining whether the line was executed by the testcase.
The final result is spectra file and located under output directory.

=cut

use strict;
use warnings;
use Getopt::Long;
use File::Spec;
use Cwd;
use File::Find;
use FindBin;
use lib "$FindBin::Bin/..";
use perl::extracting_lines;
use perl::project;
use perl::write_target_prop;
use perl::extract_class;
#below two are added for measuring
use Time::HiRes qw(time);
use POSIX qw(strftime);

my $ARG_ERROR="wrong arguments";
my $CMD_FAIL = "command is failed...";

my $base_dir; #contain working directory
my ($all, $test);
my @methods; #mode related: all vs test vs test & methods
my ($id, $pid, $bid);
my $use_cobertura = undef;

my @classes_list=();
my $num_total= undef;

GetOptions (    
            "a"   => \$all,      
            "t=s"  => \$test,
            "m=s" => \@methods,
	    "b=s" => \$bid,
	    "o=s" =>\$id,
	    "p=s" => \$pid,
	    "w=s" => \$base_dir,
	    "c=s" => \$use_cobertura)   
or die("Error in command line arguments\n");

print "Given base directory: $base_dir AND current base directory: ".cwd()."\n";

my $current_dir = getcwd();#current working directory
chdir($base_dir);

my $src_home = srcdir_from_ant($pid);
if (index($src_home, $base_dir) == -1){
	$src_home = File::Spec->catfile($base_dir, srcdir_from_ant($pid));
}
my $tc_coverage_home = File::Spec->catfile($base_dir, 'test_classes');
my @test_cases=();
my $cmd;
my @all_tests=();
my %spectra; #spectra result will be saved here
my $gen=0;

#depending on the combination of options execute all mode or specified testcase mode
if ($all){	#all mode
	my @now=localtime();
        my $timestamp = sprintf("%04d%02d%02d%02d%02d%02d",$now[5]+1900,$now[4]+1,$now[3],$now[2],$now[1],$now[0]);
	print "$timestamp\n";
	my $coverage_file=undef;
	my $t_handler=undef;
	my $c_handler=undef;
	my $number_of_fails=0;
	my $number_of_success=0;
	
	@all_tests=();
	open($c_handler,"<","$base_dir/testclasses_list.txt") or die "CANNOT OPEN FILE";
	#extract test class list of the project
        while(my $c=<$c_handler>){
                chomp $c;
		push @all_tests, $c;
                #@all_tests=(@all_tests,$c);
        }
	close($c_handler);
	#get class list from source directory
	@classes_list=class_list($src_home, $base_dir);
	#test all test case and record it to spectra	
	my $t1 = time;
	foreach my $test_class (@all_tests){
		@test_cases=();
		print "current testclass is $test_class\n";
		open ($t_handler,"<",File::Spec->catfile($base_dir,"testcase_list_$test_class.txt"));
	        while(my $testcase=<$t_handler>){
 	      	        chomp $testcase;
			#$cov_class = $test_class; $cov_testcase = $testcase;
	              	#@test_cases=(@test_cases,$testcase);
			push @test_cases, $testcase;
        	}
        	close($t_handler);
		($number_of_fails, $number_of_success) = testing_w_write($test_class,$number_of_fails, $number_of_success, $use_cobertura, $gen, @test_cases);
		$gen=1;	
	}
	my $t2 = time;
	printf "Generate coverage: ".($t2-$t1)."\n";
#	print "Number of failures: $number_of_fails and Number of success: $number_of_success\n";
	end_spectra_tc($number_of_fails, $number_of_success);	

	print "writing start\n";	
	$t1 = time;
	csv_converter($id);
	$t2 = time;
	printf "writing time: ".($t2-$t1)."\n";
	@now=localtime();
        $timestamp = sprintf("%04d%02d%02d%02d%02d%02d",$now[5]+1900,$now[4]+1,$now[3],$now[2],$now[1],$now[0]);
	print "$timestamp\n";
}
else{	#specified test cases mode
	my $target_test = $test;
	$target_test=~ s/\./\//g;
	print "\ntarget_test: $target_test\n";	

	my $c_handler = undef;
	@test_cases = ();
	if ($#methods == -1){
		open($c_handler,"<",File::Spec->catfile($base_dir,"testcase_list_$test.txt")) or die "CANNOT OPEN FILE";
		while(my $c=<$c_handler>){
			chomp $c; 
			push @all_tests, $c;
			#@all_tests = (@all_tests, $c);
		
		}
	}
	else{ @test_cases = @methods;}

	#get class list from source directory	
	@classes_list = class_list($src_home, $base_dir);
	my @failed_list = testing($test, $use_cobertura, @test_cases);
	reflect_spectra_all($test, \@failed_list, @test_cases);
	csv_converter($id);
}

#go back to original directory
chdir($current_dir);


#reflect total num_pass and total num_fail to spectrum
#generate final spectrum results
sub end_spectra_tc{
	my ($num_fail, $num_pass) = @_;
	foreach my $x (keys %spectra){ #x is class
		my $lines = ref($spectra{$x}); 
		my @temp = sort {$a <=> $b} keys %{$spectra{$x}};
		foreach my $y (@temp){ #y is line number
			$spectra{$x}{$y}[1] = $spectra{$x}{$y}[1] + $num_pass;
			$spectra{$x}{$y}[3] = $spectra{$x}{$y}[3] + $num_fail;
		}
	}
}

#write the output file
#each line of output file will be consisted of class,line number,content of line,branch,ep,np,ef,nf,executed passed testcases,executed failed testcases
#each column will be separated by ':::'
#if passed parameter $id is -1 then it is for local version, if it is not -1 then condor version
sub csv_converter{
	my $id=shift @_;
	my @keys=keys %spectra;
	my $handler=undef;
	#dest directory where output csv file will be stored	
	my $dest = File::Spec->catfile($base_dir, "output");
	if (not (-e $dest and -d $dest)){
		mkdir $dest;
	}

	if ($id != 0){
		open($handler,">", File::Spec->catfile($dest, "spectra.all.$id.need_conv.csv"));
	}
	else{
		open($handler,">", File::Spec->catfile($dest, "spectra.all.need_conv.csv"));
	}

	foreach my $x (@keys){
		my $lines=ref($spectra{$x});
		my @temp=sort {$a <=> $b} keys %{$spectra{$x}}; #sort line numbers
		foreach my $y (@temp){
			print $handler "\'$x\':::\'$y\':::\'$spectra{$x}{$y}->[7]\':::";
			if($spectra{$x}{$y}->[8]==1){
                                print $handler "\'1\':::";
                        }
                        else{
                                print $handler "\'0\':::";
                        }
                        foreach my $i (0 .. 6){
                                if ($i == 4 or $i == 5){
					if ($i == 4){
                                        	print $handler "\'@{$spectra{$x}{$y}->[$i]}\':::";
					}
					else{
						print $handler "\'@{$spectra{$x}{$y}->[$i]}\'";
					}
                                }
                                elsif ($i!=6){#0,1,2,3
                                	print $handler "\'$spectra{$x}{$y}->[$i]\':::";
                                }
                        }
                        print $handler "\n";
                }
        }
        close($handler) or warn "close file failed : $!";

}

#print out detailed result of coverage
sub print_out{
	my $test_class=shift @_;
	my @keys=keys %spectra;
        my $handler=undef;
	my $dest = File::Spec->catfile($base_dir, "output");
	if (not (-e $dest and -d $dest)){
		mkdir $dest;
	}
	
        open($handler,">", File::Spec->catfile($dest, "spectra.$pid.$bid.$test_class.output"));
        print "\n--------------------------------------------------------------------\n";
        print "\nwriting spectra\n";
        foreach my $x (@keys){
                print $handler "\nclass: $x\n";
                my $lines= ref($spectra{$x});
                my @temp=sort {$a <=> $b} keys %{$spectra{$x}}; 

                foreach my $y (@temp){
                        print $handler "$y:\"$spectra{$x}{$y}->[7] ";
                        if($spectra{$x}{$y}->[8]==1){
                                print $handler "branch";
                        }
                        else{
                                print $handler "non-branch";
                        }
                        print $handler "\nspectra: (";

                        foreach my $i (0 .. 6){
                                if ($i == 4 or $i == 5){
                                        if($i == 4){
                                                print $handler "\npass: ";
                                        }
                                        else{
                                                print $handler "\nfail:";
                                        }
                                        print $handler "@{$spectra{$x}{$y}->[$i]} ";
                                }
                                elsif ($i!=6){#0,1,2,3
                                        if($i != 3){
                                                print $handler $spectra{$x}{$y}->[$i].",";
                                        }
                                        else{
                                                print $handler $spectra{$x}{$y}->[$i].")";
                                        }
                                }
                                else{#6
                                }
                        }
                        print $handler "\n";
                }
		print $handler "\n";
        }
        close($handler) or warn "close file failed : $!";
}


#initialize spectra
#when the init_spectra_tc is in first mode, generates the row. 
#when the init_spectra_tc is not in first mode, updates the row.
#inputs are coverage file, ref_tc_results(reference to extracted line infos after the testing, number of pass and fail, and first mode
sub init_spectra_tc{
	@_ == 4 or die $ARG_ERROR;
	my ($ref_tc_results,$num_pass,$num_fail,$first)=@_;
	my $statement=undef;
	my $branch=0;
	my %tc_results=%{$ref_tc_results};

	foreach my $class (keys %tc_results){
		foreach my $line_info (@{$tc_results{$class}}){
			#attris is consisted of line number, hit(1>= for executed), branch 
			my @attris=split ',',$line_info;
			$statement="";
			#if content of the line is needed, comment off the lines
			#$statement=find_statement(parsing_class($src_home,$class),$attris[0]);
			#if (defined $statement){
				if ($first==0){
                                	$spectra{$class}{$attris[0]}[1]=$spectra{$class}{$attris[0]}[1]+$num_pass;
                                	$spectra{$class}{$attris[0]}[3]=$spectra{$class}{$attris[0]}[3]+$num_fail;
				}
				else{
					$spectra{$class}{$attris[0]}=[0,$num_pass,0,$num_fail,[],[],[],$statement,$branch];
				}
			#}
		}
	}
	
}

#find failed test cases and return the list of them
sub failed_list{
	my $cmd="ack-grep \"".'$Tests run: 1, Failures: 1'."\""." $base_dir/reports/junit-xml/";
	my $result=`$cmd`;
	my @fail_list=split /\n/, $result;
	my @final_failed_list=();

	$cmd="ack-grep \"".'$Tests run: 1, Failures: 0, Errors: 1'."\""." $base_dir/reports/junit-xml/";
	$result=`$cmd`;
	push @fail_list, split /\n/,$result;
	#@fail_list=(@fail_list,split /\n/,$result);

	foreach my $index (0 .. $#fail_list){
		if ($fail_list[$index] =~ m/TEST-.*-.*\.txt/){
			$fail_list[$index] =~ m/TEST-/;
			$fail_list[$index] = $';
			$fail_list[$index] =~ m/\.txt/;
			$fail_list[$index] = $`;
			#@final_failed_list=(@final_failed_list,$fail_list[$index]);
			push @final_failed_list, $fail_list[$index];
		}
	}
	return @final_failed_list;
	
}

#update the spectra of the project's each line of source
#inputs are coverage file, test case which was executed, ref_tc_results(reference to extracted line infos after the testing, and failure status 
sub update_spectra_tc{
        my ($test_case,$ref_tc_results,$fail) = @_;
        my $hit;
        my $line_num;
        my $n_line=undef;
        my $branch=0;
        my $line_info=undef;
	my %tc_results=%{$ref_tc_results};
		
	print "in update spectrac tc: $test_case\n";
	for my $class (keys %tc_results){
       		foreach my $line_info (@{$tc_results{$class}}){
       			my @attris=split ',',$line_info;

			if ($attris[1]!=0){
				#if you need to know which test cases executed the line, comment off 
       				if ($fail){
					if (exists $spectra{$class}{$attris[0]}){
       						$spectra{$class}{$attris[0]}->[2]=$spectra{$class}{$attris[0]}->[2]+1;
       						$spectra{$class}{$attris[0]}->[3]=$spectra{$class}{$attris[0]}->[3]-1;
       					#	my $size=@{$spectra{$class}{$attris[0]}->[5]};
       					#	$spectra{$class}{$attris[0]}->[5][$size]=$tc;
					}
					else{
						$spectra{$class}{$attris[0]}=[0,0,1,0,[],[],[],"",$branch];
					}
       				}
       				else{
					if (exists $spectra{$class}{$attris[0]}){
       						$spectra{$class}{$attris[0]}->[0]=$spectra{$class}{$attris[0]}->[0]+1;
       						$spectra{$class}{$attris[0]}->[1]=$spectra{$class}{$attris[0]}->[1]-1;
       					#	my $size=@{$c_spectra{$attris[0]}->[4]};
       					#	$spectra{$class}{$attris[0]}->[4][$size]=$tc;
					}
					else{
						$spectra{$class}{$attris[0]}=[1,0,0,0,[],[],[],"",$branch];
					}
       				}
       			}
       		}
	}
}


#return the number of passed testcases and failed testcases
sub total_pass_fail{
	@_ == 2 or die $ARG_ERROR;
	my ($num_total, $num_fail)=@_;
	my $num_pass=$num_total-$num_fail;
	return ($num_fail, $num_pass);
}

#tests the testcases of the test class in test case array
#testcases are tested individually
#inputs are testcases list and the test class which the testcases are from
sub testing{
	my ($test_class, $use_cobertura, @test_cases)=@_;
	my $cmd;
	my $handler;
	my $log;
	#my $num_fail = 0;
	my @failed_list = ();

	foreach my $index (0 .. $#test_cases){
		if ($use_cobertura){
			$cmd = "defects4j-fluccs 0 coverage -t ".$test_class."::".$test_cases[$index]." -i classes.list -R 1";
			if ($index == 0){#first running --> need to compile and instrument
				$cmd = $cmd." -P"
			}
			system($cmd);
			if (-s "$base_dir/failing_tests"){
				push @failed_list, $test_class-$test_cases[$index];
			}
			system("rm $base_dir/failing_tests");
		}
		else{
			write_target($base_dir, $test_class, $test_cases[$index]);
                	$cmd="ant jacoco.coverage.filter";
	                $log = `$cmd`;
        	        print "\n-------------ANT COVERAGE------------\n$log\n------------ANT COVERAGE--------------\n";
                	$cmd="ant jacoco.report";
	                print "\n-------------ANT REPORT------------\n$log\n------------ANT REPORT--------------\n";
        	        $log = `$cmd`;
		}
	}
	#return $num_fail;
	return @failed_list;
}


#look through log and check whether the test is failed or not, return check fail value
sub check_failure{
	my ($log, $test_class, $test_case, $use_cobertura) = @_;
	my $fail = undef;
	my $t1 = undef;
	my $t2 = undef;
	my $failure_pattern = undef;

	print $log;
	$t1 = time;
	if ($use_cobertura){
		$failure_pattern = "--- ".$test_class."::".$test_case;
		print $failure_pattern;
	}
	else{
		$failure_pattern = "\[java\] \.*E\.*"
	}
	
	if ($log =~ /$failure_pattern/){
		print "Failure in $test_class\:\:$test_case\n";
		$fail = 1;
	}
	else{
		$fail = 0;
	}
	$t2 = time;
	print "in check failure: ".($t2 - $t1)."\n";
	return $fail;
}	

#tests the all testcases for the test class
#the result will be reflected right after the test
#inputs are test class, number of failed testcase, and testcases list which contained all testcases of the test class
sub testing_w_write{
        my ($test_class, $number_of_fails, $number_of_success, $use_cobertura, $not_first, @test_cases)=@_;
        my $cmd;
        my $handler;
        my $log;
	my %tc_results=();
	my $fail=0;
	my $coverage_file=undef;
	my $t1 = undef; my $t2 = undef;
	my $stdout = undef;

	if ($test_class =~ m/\$/){
		$test_class =~ s/\$/\\\$/g;
	}
        foreach my $test_case (@test_cases){
		$t1 = time;
		#coverage file on which current coverage result will be stored
		$coverage_file=$tc_coverage_home.'/'.$test_class.'-'.$test_case.'.xml';
		if ($use_cobertura){
			$coverage_file = "$base_dir/coverage.xml";
			$cmd = "defects4j-fluccs 0 coverage -t ".$test_class."::".$test_case." -i classes.list -R 1";
			if (not $not_first){#first test case run --> need to compile and instrument
				$cmd = $cmd." -P";
				$not_first = 1;
			}
			print $cmd."\n";
			system($cmd);
			if (-s "$base_dir/failing_tests"){
				$fail = 1;
			}
			else{
				$fail = 0;	
			}

		}
		else{
	                write_target($base_dir, $test_class,$test_case);
        	        $cmd="ant jacoco.coverage.filter";
			$stdout = `$cmd`;	
                	print "\n-------------ANT COVERAGE------------\n$stdout\n------------ANT COVERAGE--------------\n";
			$cmd="ant jacoco.report";
			$log = `$cmd`;
			print "\n-------------ANT REPORT------------\n$log\n------------ANT REPORT--------------\n";
			$fail = check_failure($stdout, $test_class, $test_case, $use_cobertura);    
		}

		if ($fail == 1){
			$number_of_fails += 1;
		}
		else{
			$number_of_success += 1;
		}
			
		$t2 = time;
		printf "Time for a single test coverage: ".($t2-$t1)."\n";

		#extract the information(line number, coverage of line) of the each line of the class in classes list
		$t1 = time;
		my $w_init = 1 - $gen;
		if ($gen == 0){
			%tc_results=extract_line($coverage_file, $w_init, $use_cobertura, @classes_list);
			$gen = 1;
		}
		else{
			%tc_results=extract_line($coverage_file, $w_init, $use_cobertura, @classes_list);
		}
		$t2 = time;
		print "extract line: ".($t2-$t1)."\n";
	
		$t1 = time;
		my $ref_tc_results = \%tc_results;
		reflect_spectra($test_case,$ref_tc_results,$fail,$w_init);
		$t2 = time;
		printf "reflect_spectra: ".($t2-$t1)."\n";

		$cmd = "rm $coverage_file"; `$cmd`;
		if (not $use_cobertura){
			$cmd = "rm $tc_coverage_home/$test_class"."-$test_case".".csv"; `$cmd`;
		}
        }
	return ($number_of_fails, $number_of_success);
}


#reflects testcase result to the spectra
#inputs are coverage file, test case which was executed, reference to the testing result of the testcase,failure status, value of first mode
sub reflect_spectra{
	my ($test_case,$ref_tc_results,$fail,$first)=@_;	
	my ($num_fail,$num_pass) = total_pass_fail(1,$fail);

	#if first mode, initialize the spectra
	if ($first == 1){
		init_spectra_tc($ref_tc_results,0,0,$first);
	}
	update_spectra_tc($test_case,$ref_tc_results,$fail);

	return 1;		
}


#reflects testcases result to the spectra
#inputs are test class where testcases come from and a list of testcases which were executed.
sub reflect_spectra_all{
	my ($test_class, $ref_fail_list, @test_cases)=@_;
	my @fail_list = @{$ref_fail_list};
	my $coverage_file=undef;
	my $cmd=undef;
	my %tc_results=();
	my $ref_tc_results=undef;
	my $fail=undef;
	my ($num_fail,$num_pass) = total_pass_fail(scalar @test_cases,scalar @fail_list);
	my $first=1;
	my $gen = 1;
	
	foreach my $test_case (@test_cases){
		my $full_tc=$test_class."-".$test_case;
		if ( grep { $full_tc eq $_ } @fail_list){
			$fail=1;
		}
		else{
			$fail=0;
		}
		#set coverage file to read
		$coverage_file=File::Spec->catfile($tc_coverage_home,"$full_tc".".xml");
		
		#extract the information(line number, hitting statue, type of the statement) of the each line of the class in classes list
		if ($first == 1){
			%tc_results=extract_line($coverage_file, $first, $use_cobertura, @classes_list);
			$ref_tc_results=\%tc_results;
			init_spectra_tc($ref_tc_results,$num_pass,$num_fail,$first);
			$first=0;
		}
		update_spectra_tc($test_case,$ref_tc_results,$fail);
		$cmd = "rm $coverage_file"; `$cmd`;
		$cmd = "rm ".File::Spec->catfile($tc_coverage_home, "$test_class"."-$test_case".".csv");`$cmd`;
	}
}

