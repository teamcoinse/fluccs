#!/bin/bash
#$1 = source directory, $2 = targetFile.csv, $3 = working directory, $4 = prefix of the package of the project, 
#$5 = output directory, $6 = project id, $7 = bug number

mvn exec:java -Dexec.mainClass="FLUCCS.codeAndchange.GenMethodCodeAndChange" -Dexec.args="$1 $2 $3 $4 $5 $6 $7 $D4J_HOME"
