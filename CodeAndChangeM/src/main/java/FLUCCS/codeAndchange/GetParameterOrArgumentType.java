package FLUCCS.codeAndchange;

import com.github.javaparser.ast.body.Parameter;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.type.Type;
import com.github.javaparser.ast.expr.TypeExpr;


public class GetParameterOrArgumentType{
	//Use parsing
	public static String getType(Node node, String ExtendType){
		String param = null;
		Type nodeType = null;
		boolean isGeneric = false;
		if (node instanceof Parameter){
			nodeType = ((Parameter)node).getType();
			if (nodeType.toString().equals("T")){
				isGeneric = true;
			}
			
			if (((Parameter)node).isVarArgs()){
				param = "...";
			}	
		}
		else if (node instanceof Expression){
			return node.toString().replace("\n","");
		}

		if (param != null){//the case where parameter is variable type
			param = nodeType.toString() + param;
		}
		else{
			param = nodeType.toString();
		}
		
		if (ExtendType != null && isGeneric){
			param = param.replace("T", ExtendType);
		}	

		return param;	
	}
}
