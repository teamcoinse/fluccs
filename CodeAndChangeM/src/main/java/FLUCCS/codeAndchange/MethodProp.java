package FLUCCS.codeAndchange;

import java.util.HashMap;
import java.util.ArrayList;

public class MethodProp{
	private String declaredClass;
	private String methodId; 
	private ArrayList<Integer> lines;

	public MethodProp(String declaredClass, String methodId){
		this.declaredClass = declaredClass;
		this.methodId = methodId;
		this.lines = new ArrayList<Integer>();
	}

	public MethodProp(String declaredClass, String methodId, ArrayList<Integer> lines){
		this.declaredClass = declaredClass;
		this.methodId = methodId;
		this.lines = lines;
	}

	public String getMethodId(){
		return this.methodId;
	}

	public String getDeclaredClass(){
		return this.declaredClass;
	}
	
	public void renameDeclaredClass(String new_declaredClass){
		this.declaredClass = new_declaredClass;
	}

	public ArrayList<Integer> getLines(){
		ArrayList<Integer> tempLines = new ArrayList<Integer>();
		tempLines.addAll(this.lines);
		return tempLines;
	}

	public void deleteLines(ArrayList<Integer> delLines){
		this.lines.removeAll(delLines);
	}
	
	public void deleteLines(int delLine){
		this.lines.remove(delLine);
	}

	public void addLines(ArrayList<Integer> addLines){
		this.lines.addAll((ArrayList<Integer>)addLines);
	}

	public void addLines(int addLine){
		this.lines.add(addLine);
	}
}
