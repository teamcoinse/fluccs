package FLUCCS.codeAndchange;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Iterator;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collections;

import java.nio.file.Paths;
import java.nio.file.Path;
import java.util.regex.*;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;


public class GenMethodCodeAndChange{
	private static String workDir = null;
	//a directory where the checked out source files are temporarily saved
	private static String output_dir = null;
	private String rootPackageName = null;
	private String D4J_HOME = null;
	private String projectName = null;
	private int bugNum = 0; 
	//String is a className
	private HashMap<String, HashMap<String, MethodCodeAndChageM>> methodCodeAndChanges;

	public GenMethodCodeAndChange(String rootPackageName, String workDir, String output_dir, String D4J_HOME, String projectName, int bugNum){
		this.rootPackageName = rootPackageName;
		this.workDir = workDir;
		this.output_dir = output_dir;
		this.D4J_HOME = D4J_HOME;
		this.projectName = projectName;
		this.bugNum = bugNum;
	}

	
	//get hashmap of the method code and change metric values for the file specified by the given fileId 	
	public HashMap<String, MethodCodeAndChageM> getMethodCodeAndChanges(String fileId){
		HashMap<String, MethodCodeAndChageM> returnedM = this.methodCodeAndChanges.get(fileId);
		if (returnedM == null){
			String postfix = isPackageNameHasPostfix(fileId);
			if (postfix != null){
				String header = this.rootPackageName.replace(".", "/");
				String alteredFileId = fileId.replace(header + postfix, header);
				//System.out.println("ALTERED FILEID: " + fileId + " becomes " + alteredFileId); // logging
				returnedM = this.methodCodeAndChanges.get(alteredFileId);
			}else{
				System.out.println("FAIL TO RETRIEVE: CANNOT FIND THE KEY " + fileId);
				System.out.println("Suspected cause: git rename --> git failed to detect the previous renaming of the file");
				//System.exit(0);
				return null;
				//throw new RuntimeException();
			}
		}
		return returnedM;
	}
	
	//Get revisions of the last revision among 'num' number of revisions
	public String getRevs(int num){
		String rev = null;
		String cmd = "git log -" + Integer.toString(num);
		Pattern p = Pattern.compile("^commit\\s++(.*)");
		ArrayList<String> logs = new ArrayList<String>();
		int found_num = 0;
		BufferedReader br_log = null;
		try{
			Process process = execCmd(cmd);
			br_log = new BufferedReader(new InputStreamReader(process.getInputStream()));
			String line = br_log.readLine();
			while(line != null){
				Matcher m = p.matcher(line);
				if (m.find()){
					found_num ++;
					if (found_num == num){
						rev = m.group(1);
						break;
					}
				} 
				line = br_log.readLine();
			}
			if (br_log != null){
				br_log.close();
			}
		} catch(IOException e){
			System.out.println(e);
			System.out.println("IOException occurrend while executing " + cmd);
		}
		//check if we meet the end of the history, meaning we reached the start of the project
		if (found_num < num){
			//System.out.println("It is impossible to go back " + Integer.toString(num) + " times"); // logging
			//System.out.println("WE REACH THE START OF THE PROJECT"); // logging
			return null;
		}
		//System.out.println("Current Revision: " + rev); // logging
		return rev;	
	}	

	public boolean compareTwoFile(String Target, String Comp){
		String header = this.rootPackageName.replace(".", "/");
		Pattern p = Pattern.compile(header + "[^\\/]*\\/(.*\\.java)");
		Matcher m_comp = p.matcher(Comp);
		Matcher m_target = p.matcher(Target);
		if (m_comp.find() && m_target.find()){
			if (m_comp.group(1).equals(m_target.group(1))){
				return true;
			}
		}
		return false;
	}


	//Checkout whether target is in class_lst	
	public boolean in_class(ArrayList<String> class_lst, String target){
		boolean compResult = false;
		for (String aclass: class_lst){
			if (target.equals(aclass)){
				compResult = true;
				break;
			}
			else{
				compResult = compareTwoFile(aclass, target);
				if (compResult == true){
					break;
				}	
			}	
		}
		return compResult;
	}

	

	public HashMap<String, HashMap<String, String>> readMthIdInfo(){
		HashMap<String, HashMap<String, String>> mthIdInfo = new HashMap<String, HashMap<String, String>>();
		FileReader fr = null;
		BufferedReader br = null;
		String targetMthDir = Paths.get(this.D4J_HOME, "framework/bin/fluccs/method_stmt/", this.projectName + "/Methods").toString();
		String fileName = targetMthDir + "/" + this.projectName + "." + this.bugNum + ".csv";
		try{
			fr = new FileReader(fileName);
			br = new BufferedReader(fr);
			String line = br.readLine();
			while (line != null){
				//temp[0] = ID, temp[1] = declaredFile, temp[2] = declaredClass, temp[3] = methodName, temp [4:length] = arguments
				String[] temp = line.split(",");
				//if the hashmap for the declaredFile is not made yet, made it
				if (!mthIdInfo.keySet().contains(temp[1])){
					mthIdInfo.put(temp[1], new HashMap<String, String>());
				}
				String _params = temp[4];
				if (temp.length > 5){
					for (int i = 5; i < temp.length; i++){
						_params += "," + temp[i];
					}
				}
				mthIdInfo.get(temp[1]).put(temp[2] + "," + temp[3] + "," + _params, temp[0]);
				line = br.readLine();
			}
			if (br != null){
				br.close();
			}
		} catch (IOException e){
			System.out.println(e);
			System.out.println("Error occurred while reading targetMethods.csv");
		}
		return mthIdInfo;	
	}


	public String findID(MethodProp methodProp, String fileName, HashMap<String, HashMap<String, String>> mthIdInfo){
		String methodDesc = methodProp.getDeclaredClass() + "," + methodProp.getMethodId();
		String ID = null;
		try{
			ID = mthIdInfo.get(fileName).get(methodDesc);
		} catch(NullPointerException e){
			return null;
		} finally{
			return ID;
		}
	} 


	public void initialize(Parsing parser, String srcDirName, ArrayList<String> file_lst){
		int count = 0;
		HashMap<String, HashMap<String, String>> mthIdInfo = readMthIdInfo();
		//TO FIND UNMATCHED ONES
		Set<String> mthInfoIDs = new HashSet<String>();
		for (String key: mthIdInfo.keySet()){
			if (mthInfoIDs.contains(key)){
				System.out.println("SAME KEY: " + key);
			}
			Set<String> temp = new HashSet<String>();
			for (String _key: mthIdInfo.get(key).keySet()){
				temp.add(mthIdInfo.get(key).get(_key));
			}
			mthInfoIDs.addAll(temp);
		}
		Set<String> foundMethods = new HashSet<String>();

		String postfix = isPackageNameHasPostfix(file_lst.get(0));
		for (String fileName: file_lst){
			if (srcDirName.endsWith("/")){
				srcDirName = srcDirName.substring(0, srcDirName.length() - 1);
			}
			String filePath = srcDirName + "/" + fileName;
			HashMap<String, MethodCodeAndChageM> hashMethodCodeAndChageMs = new HashMap<String, MethodCodeAndChageM>();
			ArrayList<MethodProp> methodProps = parser.getDeclaredMethods(filePath);
			for (MethodProp methodProp: methodProps){
				String ID = findID(methodProp, fileName, mthIdInfo);

				//add methodProp to hashMethodCodeAndChageM only if the ID is not null
				if (ID != null){
					foundMethods.add(ID);	
				}
				else{
					if (foundMethods.contains(ID)){
						System.out.println("SAME OCCURRED: " + ID);
					}
				}
				String methodDesc = methodProp.getDeclaredClass() + "," + methodProp.getMethodId();
				hashMethodCodeAndChageMs.put(methodDesc, new MethodCodeAndChageM(methodProp, ID));
				count++;
			}

			String outerKey = filePath.substring(srcDirName.length() + 1, filePath.length());
			if (postfix != null){
				String header = this.rootPackageName.replace(".", "/");
				outerKey = outerKey.replace(header + postfix, header);
			}
			//System.out.println("OUTERKEY: " + outerKey + " and original " + fileName + " and filepath is " + filePath);  // logging
			this.methodCodeAndChanges.put(outerKey, hashMethodCodeAndChageMs);
		}
	
		mthInfoIDs.removeAll(foundMethods);
		if (mthInfoIDs.size() != 0){
			try{
				System.out.println("RECORDING NOT FOUND ONES");
				FileWriter fw = new FileWriter(this.output_dir + "/Not_Found.csv");
				BufferedWriter bw = new BufferedWriter(fw);
				for (String mthID: mthInfoIDs){
					bw.write(mthID);
					bw.newLine();
					bw.flush();
				}
				if (bw != null){
					bw.close();
				}
			} catch(IOException e){
				System.out.println(e);
			}
		}
	}

	//update MethodProp of the MethodCodeAndChageM in this.methodCodeAndChanges 
	public void updateMethodProp(String fileId, ArrayList<MethodProp> p_declaredMethods, int examinedVersions){
		//elements in the keySet are methodDesc
		Set<String> c_keySet = new HashSet<String>();
		HashMap<String, MethodCodeAndChageM> returnedM = getMethodCodeAndChanges(fileId);
		if (returnedM == null){ // MAINLY CAUSED BY THE FAILED DECTECTION OF FILE RENAMING IN GIT 
			return;
		}
		c_keySet.addAll(returnedM.keySet());
		Set<String> p_keySet = new HashSet<String>();

		if (p_declaredMethods != null){
			for (MethodProp methodProp: p_declaredMethods){
				String methodDesc = methodProp.getDeclaredClass() + "," + methodProp.getMethodId();
				p_keySet.add(methodDesc);
				if (c_keySet.contains(methodDesc)){
					returnedM.get(methodDesc).replaceMethodProp(methodProp);
				}
			}
		} 

		//Set NumExist of deleted Methods
		c_keySet.removeAll(p_keySet);//only keys for deleted methods are remained in c_keySet
		for (String c_key: c_keySet){
			returnedM.get(c_key).setNumExist(examinedVersions);
		}

	}

	public void updateMethodCodeAndChangeMs(String fileId, ArrayList<MethodProp> p_declaredMethods, ArrayList<Integer> p_modeLines, ArrayList<Integer> c_modeLines, int examinedVersions){
		Set<String> methodDesc_lst = new HashSet<String>();
		Set<String> alreadyExamined = new HashSet<String>();
		HashMap<String, MethodCodeAndChageM> returnedM = getMethodCodeAndChanges(fileId);
		if (returnedM == null){
			return;
		}
		methodDesc_lst.addAll(returnedM.keySet());
		Iterator iterator = returnedM.keySet().iterator();

		while(iterator.hasNext()){
			String key = (String)iterator.next();
			ArrayList<Integer> lines = returnedM.get(key).getMethodProp().getLines();
			lines.retainAll(c_modeLines);

			if (!lines.isEmpty()){//common elements?//
				alreadyExamined.add(key);
				returnedM.get(key).increaseNumChanges(examinedVersions); 
			}
		}

		if (p_declaredMethods != null){	
			for (MethodProp methodProp: p_declaredMethods){
				ArrayList<Integer> lines = methodProp.getLines();
				lines.retainAll(p_modeLines);
				if (lines.isEmpty()){//common elements?
					String methodDesc = methodProp.getDeclaredClass() + "," + methodProp.getMethodId();
					//should be in methodDesc_lst and not examined yet
					if (methodDesc_lst.contains(methodDesc) && !alreadyExamined.contains(methodDesc)){
						returnedM.get(methodDesc).increaseNumChanges(examinedVersions);
					}
				}
			}
		}
		updateMethodProp(fileId, p_declaredMethods, examinedVersions);
	}

	//set numExist of MethodCodeAndChageM to examinedVersions
	public void setNumExistForRemained(int examinedVersions){
		Iterator fileIterator = this.methodCodeAndChanges.keySet().iterator();
		while (fileIterator.hasNext()){
			String fKey = (String)fileIterator.next();
			HashMap<String, MethodCodeAndChageM> returnedM = getMethodCodeAndChanges(fKey);
			if (returnedM == null){
				continue;
			}
			Iterator methodIterator = returnedM.keySet().iterator();
			while (methodIterator.hasNext()){
				String mKey = (String)methodIterator.next();
				returnedM.get(mKey).setNumExist(examinedVersions);
			}
		}
	}

	//Print out methodCodeAndChanges
	public void printOut(int examinedVersions){
		Iterator outerIterator = null;
		FileWriter fw = null;
		//FileWriter fw_sub = null;
		BufferedWriter bw = null;
		//BufferedWriter bw_sub = null;
		try{
			outerIterator = this.methodCodeAndChanges.keySet().iterator();
			/*if (mode.equals("age")){
				fileName = this.output_dir + "/methodAges.csv";
			} else if (mode.equals("churn")){
				fileName = this.output_dir + "/methodChurns.csv";
			} else if (mode.equals("both")){*/
			String fileName = this.output_dir + "/methodAgeAndChurns.csv";
			/*} else {
				System.out.println("Wrong mode");
				System.exit(0);
			}*/
			fw = new FileWriter(fileName);
			//fw_sub = new FileWriter(this.output_dir + "/matchingResults.csv");
			bw = new BufferedWriter(fw);
			//bw_sub = new BufferedWriter(fw_sub);
			while(outerIterator.hasNext()){
				String outerKey = (String)outerIterator.next();
				HashMap<String, MethodCodeAndChageM> returnedM = getMethodCodeAndChanges(outerKey);
				if (returnedM == null){
					continue;
				}
				Iterator innerIterator = returnedM.keySet().iterator();
				while(innerIterator.hasNext()){
					String innerKey = (String)innerIterator.next();
					MethodCodeAndChageM methodCodeAndChange = returnedM.get(innerKey);
					//System.out.print("++" + outerKey + "," + innerKey + ":\t");
					//System.out.print(Integer.toString(methodCodeAndChange.getNumChanges()) + ",");
					//System.out.println(Integer.toString(methodCodeAndChange.getNumExist()) + ",");
					double normChurn = methodCodeAndChange.getNumChanges() * 1.0 / methodCodeAndChange.getNumExist();
					double normAge = methodCodeAndChange.getFirstChange() * 1.0 / examinedVersions;
					double normMaxAge = methodCodeAndChange.getNumExist() * 1.0 / examinedVersions;
					if (methodCodeAndChange.getID() != null){
						bw.write(methodCodeAndChange.getID() + "," + Integer.toString(methodCodeAndChange.getNumChanges()) + "," 
							+ Integer.toString(methodCodeAndChange.getNumExist()) + "," + Integer.toString(methodCodeAndChange.getFirstChange()) + "," + Double.toString(normChurn) + "," + Double.toString(normMaxAge) + "," + Double.toString(normAge));
						bw.newLine();
						bw.flush();
					}
					/*else{
						System.out.println("NO ID IS FOUND: " + outerKey + "," + innerKey + ": " + methodCodeAndChange.getMethodProp().getDeclaredClass() + "::" +  methodCodeAndChange.getMethodProp().getMethodId());
					}
					bw_sub.write(methodCodeAndChange.getID() + "," + outerKey + "," + innerKey);
					bw_sub.newLine();
					bw_sub.flush();*/
				}
			}
			if (bw != null){
				bw.close();
				//bw_sub.close();
			}
		} catch(IOException e){
			System.out.println(e);
			System.out.println("Error occurred while writing the output method churn");
		}
	}	
	

	public Boolean isFileExist(String fileId, String line){
		Matcher m_null, m_found;
		Pattern nullFileP, foundP;
		
		nullFileP = Pattern.compile("^\\+\\+\\+ /dev/null");
		foundP = Pattern.compile("^\\+\\+\\+ b/" + fileId);
		m_null = nullFileP.matcher(line);
		if (m_null.find()){
			return false;
		}
		else{
			m_found = foundP.matcher(line);
			if (m_found.find()){	
				return true;
			}
			return null;//this line is not the target to check existence of the file
		}
	}

	//execute a given command
	public Process execCmd(String cmd) {
		Process process = null;
		try{
			process = Runtime.getRuntime().exec(cmd, null, new File(this.workDir));
		} catch(Exception e){
			System.out.println(e);
			System.out.println("Error occured while executing: " + cmd);
		}
		return process;
	}

	public String getBuggyRev(String projectName, int bugNum){
		String bugRev = null;
		FileReader fr = null;
		BufferedReader br = null;
		String commitDbPath = Paths.get(this.D4J_HOME, "framework/projects", projectName, "commit-db").toString();
		try{
			fr = new FileReader(commitDbPath);
			br = new BufferedReader(fr);
			String line = br.readLine();
			int count = 1;
			while(line != null){
				if (count == bugNum){
					String[] tokens = line.split(",");
					bugRev = tokens[1];
					if (!tokens[0].equals(Integer.toString(bugNum))){
						System.out.println("MISMATCHED bugNum and count: " + Integer.toString(bugNum) + "," + tokens[0]);
						throw new IOException();
					}
					break;
				}
				count++;
				line = br.readLine();
			}
		} catch(IOException e){
			System.out.println(e);
			System.out.println("Error while getting buggy rev for " + projectName + ":" + bugNum);
		} finally{
			return bugRev;
		}	
	}
	

	//Check whether a given line uses different header(root package name) than this.rootPackageName
	//i.e. lang3, 
	public String isPackageNameHasPostfix(String line){
		String header = this.rootPackageName.replace(".", "/");
		Pattern p = Pattern.compile("^" + header + "([^\\/]*)\\/");
		Matcher m = p.matcher(line);
		String postfix = null;
		if (m.find() && (postfix = m.group(1)).length() > 0){
			return postfix;
		}
		else{
			postfix = null;
		}
		return postfix;
	}	


	public int GenerateMethodCodeAndChageMs(String src_dir, String javaFile_lst){
		this.methodCodeAndChanges = new HashMap<String, HashMap<String, MethodCodeAndChageM>>();
		int examinedVersions = 0;
		
		ArrayList<String> targetFiles = new ArrayList<String>();
		FileReader fr = null;
		BufferedReader br = null;
		String line = null;
		File javaFile_lstPath = new File(javaFile_lst);

		//if javaFile_lstPath exists, than read form it. else write and read from it
		//read from class list file and set targetFiles which contains from path to
		//the source file, starting from root package	
		try{
			if (javaFile_lstPath.exists()){
				//System.out.println("Reading " + javaFile_lst);  // logging
				fr = new FileReader(javaFile_lst);
				br = new BufferedReader(fr);
				line = br.readLine();
				while(line != null){
					String filePath = line.replace("\n", "");
					targetFiles.add(filePath);
					line = br.readLine();
				}	
			}
			else{
				//System.out.println("Writing " + javaFile_lst);  // logging
				targetFiles = ExtractMethods.writeFileLst(src_dir, javaFile_lst, "", targetFiles);
			}	
			if (br != null){
				br.close();
			}	
		} catch (IOException e){
			System.out.println(e);
			System.out.println("Error occurred while reading class list file");
		}

		String cmd = null;
		String c_rev = getRevs(1);
		String p_rev = getRevs(2);
		boolean newStart = true;
		int num = 2;

		Parsing parser = new Parsing();
		initialize(parser, src_dir, targetFiles);
		//check whether ther are some previous revisions to look at
		while (p_rev != null){
			cmd = "git diff " + c_rev + " " + p_rev;
			Process process = execCmd(cmd);
			BufferedReader br_log = new BufferedReader(new InputStreamReader(process.getInputStream()));	
			//SHOULD CONSIDER ABOUT CASE WHERE: lang3, math3
			Pattern p = Pattern.compile("^diff --git a/(.*)(" + this.rootPackageName + ".*/.*.java) b/(.*)(" + this.rootPackageName + ".*/.*.java)");
			Pattern getRangeP = Pattern.compile("^@@ -([0-9]+),([0-9]+) \\+([0-9]+),([0-9]+)\\s++@@");

			/* For all java src files */
			boolean newFileDiff = false;
			Boolean foundP = null;
			boolean in = false;
			String c_file = null, c_file_id = null;
			String p_file = null, p_file_id = null;
			ArrayList<MethodProp> p_file_declaredMethods = null;
			int c_startNum = 0, c_range = 0, c_lineNum = 0;
			int p_startNum = 0, p_range = 0, p_lineNum = 0;
			ArrayList<Integer> c_modeLines = new ArrayList<Integer>();
			ArrayList<Integer> p_modeLines = new ArrayList<Integer>();

			String aline = null;
			try{
			while((aline = br_log.readLine()) != null){
				Matcher m = p.matcher(aline);
				if (m.find()){//new File
					if (c_file_id != null){
						//should cover cases like lang3
						//System.out.println("UPDATE BEFORE READING ANOTHER FILE"); // logging
						updateMethodCodeAndChangeMs(c_file_id, p_file_declaredMethods, p_modeLines, c_modeLines, examinedVersions);
					}
					//System.out.println(m.group());
					c_file_id = m.group(2);
					c_file = m.group(1) + c_file_id;
					p_file_id = m.group(4);
					p_file = m.group(3) + p_file_id;
					//if current c_file is not the target pass it
					//this part will make further process to exclude cases where current revision
					//has different files than the starting point & the cases where previous(next) version has files
					//that are not in the current version(=meaning by going to current version, this file has beed deleted)
					//using c_file_id instead of c_file is more clear
					if (!in_class(targetFiles, c_file_id)){
						c_file_id = null; c_file = null;
						p_file_id = null; p_file = null;
						newFileDiff = false;
					}
					else{
						try{
							if (!c_file_id.equals(p_file_id)){
								//CASE WHERE FILE RENAME OCCURRED
								//list targetFiles should be changed: switch the old one with the new one
								cmd = "git --version";
								Process Aprocess = execCmd(cmd);
								BufferedReader br_out = new BufferedReader(new InputStreamReader(Aprocess.getInputStream()));
								String gitVerLine = null;
								if (br_out != null){
									gitVerLine = br_out.readLine();
									br_out.close();
								}
								else{
									System.out.println("Faile to read stdandard output of `git version1");
									throw new NullPointerException();
								}
								Pattern gitVerP = Pattern.compile("git version (.*)$");
								Matcher gitVerM = gitVerP.matcher(gitVerLine);
								if (gitVerM.find()){
									String gitVer = gitVerM.group(1);
									String[] gitVerNums = gitVer.split("\\.");
									if (Integer.parseInt(gitVerNums[0]) >= 2 && Integer.parseInt(gitVerNums[1]) >= 15){							
										//Replace targetFiles with the new file identifier
										//**
										System.out.println("File is renamed: from " + c_file_id + " to " + p_file_id);  // logging
										int c_fileIndex = targetFiles.indexOf(c_file_id);
										if (c_fileIndex < 0){// -1 --> handle the case of file renaming where only the root source directory(package) name has been
															 //		changed before without any file renaming 
											String header = this.rootPackageName.replace(".", "/");
											//Pattern old_file_name_p = Pattern.compile(
											String old_c_file_id = c_file_id.replace(header, header + "3");
											c_fileIndex = targetFiles.indexOf(old_c_file_id);
											targetFiles.add(c_fileIndex, c_file_id); // replace with new c_file_id
										}
										targetFiles.set(c_fileIndex, p_file_id);
										String postfix = isPackageNameHasPostfix(p_file_id);
										if (postfix != null){
                                            						String header = this.rootPackageName.replace(".", "/");
											p_file_id = p_file_id.replace(header + postfix, header);
											//*
											System.out.println("WHILE PUTTING NEW ONE: ALTERED FILEID: " + p_file + " becomes " + p_file_id);  // logging
										}
                                        					this.methodCodeAndChanges.put(p_file_id, getMethodCodeAndChanges(c_file_id)); // do not have to worry about not-detected renamining
                                        					this.methodCodeAndChanges.remove(c_file_id);
															//*
                                        					System.out.println("Now new key set");  // logging
										c_file_id = p_file_id;
									}
                                    					else if (!compareTwoFile(c_file_id, p_file_id)){
                                        					throw new RuntimeException();
                                    					}
								}
								else{
									throw new RuntimeException();
								}
							}
							cmd = "git --work-tree=" + this.output_dir + "/prev checkout " + p_rev + " " + p_file;
							execCmd(cmd).waitFor();
						} catch(Exception e){
                            				System.out.println(e);
							if (e instanceof RuntimeException){
								System.out.println("Wrong diff results: diff btwn " + c_file + " and " + p_file);
								System.exit(0);
							}
						}

						/* reg ex --> extract filename*/
						if (new File(this.output_dir + "/prev/" + p_file).isFile()){
							newFileDiff = true;
							File f = new File(this.output_dir + "/prev/" + p_file);
							/*Parsing using java parser --> must be done for the p_file*/
							//Updated current collected infos -> should be updated before gnerating new p_file_declaredMethods  && need to process if it is the last one --> if not, missed updaet
							//System.out.println("c_rev: " + c_rev + " and p_rev: " + p_rev);  // logging
							//below will be compared with this.methodCodeAndChanges.get(c_file_id)	
							p_file_declaredMethods = parser.getDeclaredMethods(this.output_dir + "/prev/" + p_file);
	
							c_modeLines = new ArrayList<Integer>();
							p_modeLines = new ArrayList<Integer>();
							foundP = null;
						}
						else{
							newFileDiff = false;
						}
						in = false;
					}
					continue;
				}
		

				if (newFileDiff){
					//case for c_file is already checked when assigning value of newFileDiff
					if (foundP == null){//not meet yet
						foundP = isFileExist(p_file, aline);
						continue;
					}
					else if (foundP){
						Matcher m_getRangeP = getRangeP.matcher(aline);
						if (m_getRangeP.find()){
							in = true;
							c_startNum = Integer.parseInt(m_getRangeP.group(1));
							c_lineNum = c_startNum;
							c_range = Integer.parseInt(m_getRangeP.group(2));
							p_startNum = Integer.parseInt(m_getRangeP.group(3));
							p_lineNum = p_startNum;
							p_range = Integer.parseInt(m_getRangeP.group(4));
							continue;//meet new range --> working on next line
						}
						
					}
					else{//1)foundP is null --> not meet the checking point, 2)foudnP is false --> c_file no longer exists
						//since there is no corresponding p_file, p_declaredMethods is null & in is still false
						p_file_declaredMethods = null;
						in = false;
						continue;
					}
				}

				if (in && (c_lineNum <= c_startNum + c_range || p_lineNum <= p_startNum + p_range)){
					//Line matching for extract
					if (aline.startsWith("-")){
						try{
							if(c_lineNum <= c_startNum + c_range){
								c_modeLines.add(c_lineNum);
							}
							else{
								throw new RuntimeException();
							}
						}
						catch (RuntimeException e){
							System.out.println("Out of bounds while processing c_file:" + Integer.toString(c_lineNum) + " vs " + Integer.toString(c_startNum + c_range));
						}
						c_lineNum++;
					}
					else if (aline.startsWith("+")){
						try{
							if(p_lineNum <= p_startNum + p_range){
								p_modeLines.add(p_lineNum);
							}
							else{
								throw new RuntimeException();
							}
						}
						catch (RuntimeException e){
							System.out.println("Out of bounds while processing p_file:" + Integer.toString(p_lineNum) + " vs " + Integer.toString(p_startNum + p_range));
						}
						p_lineNum++;
					}
					else{
						c_lineNum++;
						p_lineNum++;
					}			 
				}
			}
			if (br_log != null){
				br_log.close();
			}
			} catch(IOException e){
				System.out.println(e);
			}
			//Update the last result of churns
			if (c_file_id != null){//if none of the modified files are the targets, no need to update
				updateMethodCodeAndChangeMs(c_file_id, p_file_declaredMethods, p_modeLines, c_modeLines, examinedVersions);
			}
			//prepare for the next step
			c_rev = p_rev;
			p_rev = getRevs(++num);
			in = false;
			newFileDiff = false;
			examinedVersions++;
			//For new comparision, set p_file_declaredMethods as null
			p_file_declaredMethods = null;
		}
		//set numExist value for remaining method
		setNumExistForRemained(examinedVersions);
		return examinedVersions;
	}

	public static void main(String[] args){
		String src_dir = args[0];
		//full path to class_lst_file
		String javaFile_lst = args[1];
		String workDir = args[2];
		String rootPackageName = args[3];
		String output_dir = args[4];
		String projectName = args[5];
		String bugNum = args[6];
		String D4J_HOME = args[7];
		if (output_dir.endsWith("/")){
			output_dir = output_dir.substring(0, output_dir.length() - 1);
		}
		File prevDir = new File(output_dir + "/prev");
		if (!prevDir.exists()){
			prevDir.mkdirs();
			System.out.println(prevDir.toString() + " is created successfully");
		}
		if (workDir.endsWith("/")){
			workDir = workDir.substring(0, workDir.length() - 1);
		}
		//check if current directory is same with a given workDir
			
		GenMethodCodeAndChange genMethodCodeAndChange = new GenMethodCodeAndChange(rootPackageName, workDir, output_dir, D4J_HOME, projectName, Integer.parseInt(bugNum));
		int examinedVersions = genMethodCodeAndChange.GenerateMethodCodeAndChageMs(src_dir, javaFile_lst);
		genMethodCodeAndChange.printOut(examinedVersions);
	}


}
