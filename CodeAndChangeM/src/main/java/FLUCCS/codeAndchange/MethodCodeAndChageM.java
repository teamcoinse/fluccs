package FLUCCS.codeAndchange;

import java.nio.file.Paths;
import java.nio.file.Path;

import java.util.ArrayList;
import java.util.HashMap;

public class MethodCodeAndChageM{
	private MethodProp methodProp;
	//private String methodDesc;
	private int numChanges;
	private String ID;
	private int NumExist;
	private int firstChange; 

	public MethodCodeAndChageM(MethodProp methodProp, String ID){
		this.methodProp = methodProp;
		this.numChanges = 0;
		this.ID = ID;
		this.NumExist = 0;
		this.firstChange = 0;
	}

	public void increaseNumChanges(int NumExist){
		//increase the numChanges only if NumExist is not set yet.
		if (this.NumExist == 0){
			this.numChanges++;
			//System.out.println("in increase: " + this.methodProp.getMethodId() + "," + Integer.toString(this.numChanges)); // logging
			//if this is the first change, record it
			if (this.firstChange == 0){
				this.firstChange = NumExist;
				//System.out.println("First change " + this.methodProp.getMethodId() + "," + Integer.toString(this.firstChange)); // logging
			}

		}
	}

	public String getID(){
		return this.ID;
	}

	public int getNumChanges(){
		return this.numChanges;
	}
	
	public MethodProp getMethodProp(){
		return this.methodProp;
	}

	public int getNumExist(){
		return this.NumExist;
	}

	public int getFirstChange(){
		return this.firstChange;
	}

	public void setNumExist(int NumExist){
		if (this.NumExist == 0 && NumExist != 0){
			this.NumExist = NumExist;
			//System.out.println("set num exist for " + this.methodProp.getDeclaredClass() + "," + this.methodProp.getMethodId() + ":" + Integer.toString(this.NumExist)); // logging
			if (this.firstChange == 0){//
				this.firstChange = NumExist;
				//System.out.println("First change " + this.methodProp.getMethodId() + "," + Integer.toString(this.firstChange)); // logging
			}
		}
	}

	//replace MethodProp with a new methodProp
	public void replaceMethodProp(MethodProp methodProp){
		this.methodProp = methodProp;
	}
	
}
