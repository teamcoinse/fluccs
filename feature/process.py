import os, sys, pickle
import genNewFeature
import numpy as np
import csv


#spilt a given argument datas to suspDatas, headers(method ID list), ccMs(CC metrics), and fault indice
def splitDatas(targetDatas):
	(faultIndice, _datas) = targetDatas
	datas = np.asarray([np.float32(adata[1:34]) for adata in _datas])
	headers = [adata[0] for adata in _datas]
	ccMs = [np.float32(adata[34:]) for adata in _datas]	

	return (datas, headers, ccMs, faultIndice)


#applt PCA to similar features, which have been obtained by looking at
#(coefficient|p-value) of (Pearson|Spearman) correlations
def applyPCAbtwnSimilars(clusterInfoFile, _datas):#, _headers = None, _ccMetrics = None, _faultIndice = None):
	(datas, headers, ccMetrics, faultIndice) = splitDatas(_datas)
	featureClusterIndice = genNewFeature.getFeatureInfo(clusterInfoFile)
	newDatas = genNewFeature.applyPCA(datas, featureClusterIndice)
	newDatas = genNewFeature.postProcess(newDatas, featureClusterIndice)

	return newDatas 

