import os, sys
import numpy as np
import csv
import pickle
import re
import argparse
from sklearn.decomposition import PCA, SparsePCA, TruncatedSVD

from sklearn.model_selection import StratifiedKFold
from sklearn.feature_selection import SelectFromModel, RFE, RFECV
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, ExtraTreesClassifier

names = ["Nearest Neighbors", "Linear SVM", "RBF SVM", "Gaussian Procss", "Decision Tree", "Random Forest", "AdaBoost", "Naive Bayes"]

features = ["ochiai", "jaccard", "gp13", "wong1", "wong2", "wong3", "tarantula", "ample",\
	"RussellRao", "SorensenDice", "Kulczynski1", "SimpleMatching", "M1", "RogersTanimoto",\
	"Hamming", "Ochiai2", "Hamann", "dice", "Kulczynski2", "Sokal", "M2", "Goodman", "Euclid",\
	"Anderberg", "Zoltar", "ER1a", "ER1b", "ER5a", "ER5b", "ER5c", "gp02", "gp03", "gp19"]


def loadData(dataDir, fileName, indice = None):
	if (dataDir not in fileName):
		fileName = os.path.join(dataDir, fileName)
	
	datas = None; headers = None; labels = None
	#read data
	(faultIndice, tempDatas) = pickle.load(open(fileName))
	datas = np.asarray([np.float32(adata[1:34]) for adata in tempDatas])
	headers = [adata[0] for adata in tempDatas]
	ccMs = [np.float32(adata[34:]) for adata in tempDatas]

	return (datas, headers, ccMs, faultIndice)	


#return featureClusterIndice which contains index of feature included in each feature cluster
def getFeatureInfo(fileName):
	global features

	csvReader = csv.reader(open(fileName))
	featureClusters = [row for row in csvReader]		
	featureClusterIndice = []
	for featureCluster in featureClusters:
		featureClusterIndice.append([])
		for _feature in featureCluster:
			featureClusterIndice[-1].append(features.index(_feature))
		
	return featureClusterIndice

#return pca applied new data
def applyPCA(datas, featureClusterIndice):
	print "in apply PCA"
	newDatas = [[] for i in range(len(datas))]
	for indice in featureClusterIndice:
		print "Indice are "
		print indice
		currentDatas = list()
		for data in datas:
			row = []
			for index in indice:			
				row.append(data[index])
			currentDatas.append(row)

		k = np.int32(np.ceil(len(indice) / 2.0)) if len(indice) > 1 else 1 
		print "k is " + str(k)
		
		if (len(indice) > 1):
			pca = PCA(n_components = k)
			pca.fit(currentDatas)
			#print pca.components_
			#print pca.components_.shape
			#print "Explained variance"
			#print pca.explained_variance_
			topKMs = [(comp, var) for comp, var in zip(pca.components_, pca.explained_variance_)]
			reduced_currentDatas = pca.transform(currentDatas)
			#print reduced_currentDatas
			#print reduced_currentDatas.shape
			#for (newData, reduced_currentData) in zip(newDatas, reduced_currentDatas):
			#	newData.extend(reduced_currentData[iIdx])
		else:
			reduced_currentDatas = currentDatas
		print "reduced currentDatas shape: " + str(np.asarray(reduced_currentDatas).shape)
		for idx in range(len(reduced_currentDatas)):
			newDatas[idx].extend(reduced_currentDatas[idx])

	#print newDatas
	newDatas = np.asarray(newDatas)
	return newDatas

#return the target featureInfoFile based on valType, corrType, dataType, processType
def getFeatureInfoFile(featureInfoDir, valType, corrType, dataType, processType):
	featureInfoFile = os.path.join(featureInfoDir, "/".join([valType, ".".join([dataType, processType, corrType, "feature.cluster.csv"])]))
	print featureInfoFile
	return featureInfoFile


#return datas prepend with headers and append with ccMetrics 
def postProcess(headers, datas, ccMetrics):
	integratedDatas = list()
	for (header, data, ccMetric) in zip(headers, datas, ccMetrics):
		data = data.tolist()
		integratedData = [header]
		integratedData.extend(data)
		integratedData.extend(ccMetric)
		integratedDatas.append(integratedData)
	return integratedDatas


#write new(reduced) data
def writeNewData(dest, fileName, newData, faultIndice):
	print "in writeNewData"
	print fileName, len(newData)
	if (dest not in fileName):
		fileName = os.path.join(dest, fileName)
	print fileName
	pickle.dump((faultIndice, newData), open(fileName, "w"))


if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("-dataDir", action = "store", default = None)
	parser.add_argument("-dest", action = "store", default = ".")
	parser.add_argument("-featureInfoDir", action = "store", default = "output")
	parser.add_argument("-valType", action = "store", default = "pval", help = "pval, coeff")
	parser.add_argument("-corrType", action = "store", default = "Spear", help = "Pear(Pearson), Spear(Spearman)")
	parser.add_argument("-dataType", action = "store", default = "PerFault", help = "PerFault, Whole")
	parser.add_argument("-processType", action = "store", default = "top10", help = "top10, all")
	
	args = parser.parse_args()

	dest = os.path.join(args.dest, "/".join([args.dataType, args.valType, args.processType, args.corrType]))
	if (not os.path.exists(dest)):
		dest = args.dest
		if (not os.path.exists(args.dest)):
			os.mkdir(args.dest)
			dest = args.dest
		for dirName in [args.dataType, args.valType, args.processType, args.corrType]:
			dest = os.path.join(dest, dirName)
			if (not os.path.exists(dest)):
				os.mkdir(dest)

	#get featureInfoDir which contains the target feature cluster information	
	featureInfoFile = getFeatureInfoFile(args.featureInfoDir,args.valType, args.corrType, args.dataType, args.processType)
	featureClusterIndice = getFeatureInfo(featureInfoFile) 

	#get all target data
	fileNames = [os.path.join(args.dataDir, fileName) for fileName in os.listdir(args.dataDir)]

	pattern = "(.*)\.dat"
	if (args.dataType == "PerFault"):
		for fileName in fileNames:
			fileId = re.search(pattern, fileName[len(args.dataDir) + 1:]).group(1)
			(originalDatas, headers, ccMetrics, faultIndice) = loadData(args.dataDir, fileName)
			print originalDatas.shape
			newDatas = applyPCA(originalDatas, featureClusterIndice)
			print newDatas.shape
			newDatas = postProcess(headers, newDatas, ccMetrics)
			writeNewData(dest, fileId + ".dat", newDatas, faultIndice)
	else:#Whole --> a single big file
		originalDatas = []
		faultIndiceLst = []; headersLst = []; ccMetricsLst = []
		fileLengths = []
		for fileName in fileNames:
			(datas, headers, ccMetrics, faultIndice) = loadData(args.dataDir, fileName)
			faultIndiceLst.append(faultIndice)
			originalDatas.extend(datas)
			headersLst.extend(headers)
			ccMetricsLst.extend(ccMetrics)
			fileId = re.search(pattern, fileName[len(args.dataDir) + 1:]).group(1)
			fileLengths.append((fileId, len(datas)))
			
		#print originalDatas.shape

		newDatas = applyPCA(originalDatas, featureClusterIndice)
		#print "========================="
		#print newDatas
		newDatas = postProcess(headersLst, newDatas, ccMetricsLst)		
		#print "========================="
		#print newDatas
		prevLength = 0
		for ((fileId, length), faultIndice) in zip(fileLengths, faultIndiceLst) :
			writeNewData(dest, fileId + ".dat", newDatas[prevLength:prevLength + length], faultIndice)
			prevLength = length






	

	






	

	
