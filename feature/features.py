import csv
import os, sys
import numpy as np

class Features:
	def __init__(self, clusterInfoFile = None, k = 1, defined_features = None):
		if (defined_features is not None):
			self.isClustered = False
			if (type(defined_features) == tuple):
				(self.suspFeatures, self.ccFeatures) = defined_features
			else:
				print "Feature definition is wrong: should be tuple (susps, ccms)"
				sys.exit()
		else:
			if (clusterInfoFile is None):
				self.isClustered = False
				self.suspFeatures = ["ochiai", "jaccard", "gp13", "wong1", "wong2", "wong3",\
					"tarantula", "ample", "RussellRao", "SorensenDice", "Kulczynski1",\
					"SimpleMatching", "M1", "RogersTanimoto", "Hamming", "Ochiai2",\
					"Hamann", "dice", "Kulczynski2", "Sokal", "M2", "Goodman",\
					"Euclid", "Anderberg", "Zoltar", "ER1a", "ER1b", "ER5a", "ER5b",\
					"ER5c", "gp02", "gp03", "gp19"]
			else:
				self.isClustered = True
				self.suspFeatures = []
				csvReader = csv.reader(open(clusterInfoFile))
				for (idx, row) in enumerate(csvReader):
					if (idx < 10):
						strIdx = "0" + str(idx)
					else:
						strIdx = str(idx)
					for fidx in range(k):
						if (fidx < 10):
							strFidx = "0" + str(fidx)
						else:
							strFidx = str(fidx)
						self.suspFeatures.append("F_" + strIdx + "_" + strFidx)

			self.ccFeatures = ["churn", "max_age", "min_age","num_args", "num_vars", "b_length", "loc"]

	def getSuspFeatures(self):
		return self.suspFeatures

	def getCCFeatures(self):
		return self.ccFeatures

	def getAllFeatures(self):
		allFeatures = []
		allFeatures.extend(self.suspFeatures)
		allFeatures.extend(self.ccFeatures)
		return allFeatures
	
	def getSuspFeatureSize(self):
		return len(self.suspFeatures)

	def getCCFeatureSize(self):
		return len(self.ccFeatures)

	def getSize(self):
		return self.getSuspFeatureSize() + self.getCCFeatureSize()
								
