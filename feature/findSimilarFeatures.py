import os, sys
import csv
import numpy as np
import copy

def loadVals(fileName):
	vals = dict()
	csvReader = csv.reader(open(fileName))
	for row in csvReader:
		ofeature = row[0]
		vals[ofeature] = dict()
		line = "".join(row[1:])
		line = line[2:-2]#remove '"[ and ]"'
		line = line.replace(" ", "")
		line = line.replace("'", "")
		_vals = line.split(",")
		for _val in _vals:
			(ifeature, val) = _val.split("::")#afeature::val
			val = np.float32(val)
			vals[ofeature][ifeature] = val
	return vals


def clusterFeature(featureClusters, usePval, compVal, Afeature, Bfeature, vals):
	print "for " + Afeature + " and " + Bfeature
	if (len(featureClusters) == 0):#empty
		cluster = set([Afeature, Bfeature])
		featureClusters.append(cluster)
	else:
		included = False; bothIncluded = False
		for featureCluster in featureClusters:
			if (Afeature in featureCluster and Bfeature in featureCluster):
				bothIncluded = True
			#shoud look at all feature clustera
			print "looking at cluster:"
			size = len(featureCluster)
			countA = 0; countB = 0
			for _feature in featureCluster:
				if (usePval):
					if (_feature != Afeature and vals[Afeature][_feature] == compVal):
						#featureCluster.add(Afeature)
						countA += 1
					if (_feature != Bfeature and vals[Bfeature][_feature] == compVal):
						#featureCluster.add(Bfeature)
						countB += 1
				else:
					if (_feature != Afeature and vals[Afeature][_feature] >= compVal):
						#featureCluster.add(Afeature)
						countA += 1
					if (_feature != Bfeature and vals[Bfeature][_feature] >= compVal):
						#featureCluster.add(Bfeature)
						countB += 1
			if (countA == size):
				featureCluster.add(Afeature)
			if (countB == size):
				featureCluster.add(Bfeature)
			#check whether afther processing, the current featureCluster includes both
			#Afeature and Bfeature
			if (Afeature in featureCluster and Bfeature in featureCluster):
				bothIncluded = True
			print "after"
			print featureCluster
		#if there is no feature cluster which can include both Afeature and Bfeature,
		#then create one
		if (not bothIncluded):
			cluster = set([Afeature, Bfeature])
			featureClusters.append(cluster)
					
	return featureClusters


#compare vals and if they are similar wtih 
#an argument vals is dictionary
def findSimilarFeatures(vals, usePval = False):
	features = vals.keys()
	featureClusters = list()
	if (usePval):
		#look at both mean and median pvals and if both values are 0.0, then recorded as similar
		for ofeature in features:
			for ifeature in features:
				if (ofeature != ifeature and vals[ofeature][ifeature] == 0):
					featureClusters = clusterFeature(featureClusters, usePval, 0, ofeature, ifeature, vals)
	else:
		for ofeature in features:
			for ifeature in features:
				if (ofeature != ifeature and vals[ofeature][ifeature] >= 0.99):
					featureClusters = clusterFeature(featureClusters, usePval, 0.99, ofeature, ifeature, vals)

	return featureClusters

#writer features which are not in any of the cluster
def postprocess(featureClusters, features):
	tempFeatureClusters = copy.deepcopy(featureClusters)
	for feature in features:
		inAnyCluster = False
		for featureCluster in tempFeatureClusters:
			if (feature in featureCluster):
				inAnyCluster = True
				break
		if (not inAnyCluster):
			cluster = set([feature])
			featureClusters.append(cluster)

	return featureClusters


#record featureClusters result to dest
def record(featureClusters, dest, header, perFault, top10):
	dataType = "PerFault" if perFault else "Whole"
	preprocess = "top10" if top10 else "all"

	csvWriter = csv.writer(open(os.path.join(dest, dataType + "." + preprocess + "." + header + ".feature.cluster.csv"), "w"))
	for featureCluster in featureClusters:
		csvWriter.writerow(list(featureCluster))


if __name__ == "__main__":
	dataDir = sys.argv[1]
	perFault = bool(int(sys.argv[2]))
	top10 = bool(int(sys.argv[3]))
	usePval = bool(int(sys.argv[4]))
	header = sys.argv[5]
	dest = sys.argv[6]

#	if (usePval):# we use perfault data
	if (perFault):
		vals = dict()
		meanVals = loadVals(os.path.join(dataDir, header + ".feature.pvals.mean.csv"))	
		medianVals = loadVals(os.path.join(dataDir, header + ".feature.pvals.median.csv"))
		features = meanVals.keys()
		for ofeature in features:
			vals[ofeature] = dict()
			for ifeature in features:
				if (ofeature != ifeature):
					vals[ofeature][ifeature] = np.min(meanVals[ofeature][ifeature] + medianVals[ofeature][ifeature]) # 0 only if both values are 0
	else:#whole
		vals = loadVals(os.path.join(dataDir, header + ".feature.pvals.csv"))
	
	featureClusters = findSimilarFeatures(vals, usePval)
	features = vals.keys()
	featureClusters = postprocess(featureClusters, features)	
	#record
	record(featureClusters, dest, header, perFault, top10)




