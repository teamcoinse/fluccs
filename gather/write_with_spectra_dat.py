#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------
"""
write final data file pid_bid.csv which contains both metric vectors and indice of faulty methods.
"""
import sys
import csv
import pickle
import os
import argparse

parent_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir)
sys.path.insert(0, os.path.join(parent_dir, 'python'))
import parser_check

script_dir = os.path.dirname(os.path.abspath(__file__))

#write pid_bid.dat to resultdir
#pid_bid.dat composes of tuple (buggy_method_identifier, method_metrics_data)
def dat_write(filename, bugdir, resultdir, pid, bid):
	global script_dir
	handler = open(filename, "r")
	csvReader = csv.reader(handler, delimiter = ",")
	datas = list(tuple(row) for row in csvReader)
	handler.close()

	#read file where bug index list for pid + bid is written
	handler = open(os.path.join(bugdir, pid + "/buggy_methods_" + pid + "_" +  bid + "_list.csv"))
	bug_list = []
	while True:
		line = handler.readline()
		if not line: break
		bug_list.append(line.strip())
	handler.close()
	dataset = (bug_list, datas)
	pickle.dump(dataset, open(os.path.join(resultdir, pid + "_" + bid + ".SP.dat"), "w"))


def main(bugdir, datadir, resultdir, pid, bid):
	filename = os.path.join(datadir, "method_all.csv")
	dat_write(filename, bugdir, resultdir, pid, bid)


if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("-datadir", action = "store", default = None)
	parser.add_argument("-resultdir", action = "store", default = None)
	parser.add_argument("-pid", action = "store", default = None)
	parser.add_argument("-bid", action = "store", default = None)

	args = parser.parse_args()

	parser_check.argument_check(parser, args_name_list = ["datadir", "pid", "bid", "resultdir"], args_val_list = [args.datadir, args.pid, args.bid, args.resultdir])

	bugdir = os.path.join(os.path.abspath(os.path.join(script_dir, os.pardir)), "fault_list")
	main(bugdir, args.datadir, args.resultdir, args.pid, args.bid)
