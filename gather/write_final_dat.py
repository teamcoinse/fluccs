#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------
"""
Calculate SBFL risk formula features for each data file in raw data directory.
Each target file in the raw data directory is a set of rows, which contain coverage(spectrum) and 
cod and change metric features for each method in the SUT.
All computation is conducted at float32.
This fining process has two mode: with and without normalization of feature values, indicated by input argument(use_norm).
"""

import pickle, sys, os
import re
from deap import gp, tools, creator, base
import numpy, argparse

parent_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir)
sys.path.insert(0, os.path.join(parent_dir, 'python'))
sys.path.insert(0, os.path.join(parent_dir, 'sbfl_metrics'))
import comp_sbfl_metrics
import find_fault
import parser_check

class Tool:
	def __init__(self, use_norm, use_gpu):
		self.use_gpu = use_gpu
		if (self.use_gpu): self.local_workgroup_size = 512
		else: self.local_workgroup_size = None
		self.use_norm = use_norm
		#normChurn, normMaxAge, normMinAge, num_args, num_vars, byte_length, loc
		self.numNotSpectra = 7

	#exclude part which have not been covered		
	#testing for ep,np,ef,nf
	def exclude(self,dataset):
		(faulty_method, vectors) = dataset
		length = 0; v_lengths = []
		for v in vectors:
			length += len(v) - (self.numNotSpectra + 1); v_lengths.append(len(v) - (self.numNotSpectra + 1))
		length /= 4

		eps = numpy.zeros(length, dtype=numpy.float32)
		nps = numpy.zeros(length, dtype=numpy.float32)
		efs = numpy.zeros(length, dtype=numpy.float32)
		nfs = numpy.zeros(length, dtype=numpy.float32)

		index = 0
		for v in vectors:
			for i in range(len(v) - (self.numNotSpectra + 1)):
				if ((i+1) % 4 == 1):
					eps[index] = numpy.float32(v[i+1])
				elif ((i+1) % 4 == 2):
					 nps[index] = numpy.float32(v[i+1])
				elif ((i+1) % 4 == 3):
					efs[index] = numpy.float32(v[i+1])
				else:
					nfs[index] = numpy.float32(v[i+1])
					index += 1

		index = 0
		#new vectors
		new_vectors = []
		for i in range(len(v_lengths)):
			new_vector = [vectors[i][0],]; other_part = vectors[i][-self.numNotSpectra:]

			temp = []
			for j in range(v_lengths[i]/4):
				k = j + index
				if (not(eps[k] == 0 and efs[k] == 0)):
					temp.extend([eps[k], nps[k], efs[k], nfs[k]])			
				
			if (not (len(temp) == 0)):#have remains
				new_vector.extend(temp); new_vector.extend(other_part)
				new_vectors.append(new_vector)

			index += v_lengths[i]/4
	
		return new_vectors		

	#Calculate score for the metric and normalize them
	def metric(self, target_metric, dataset):
		(faulty_method, vectors) = dataset
		total_sum = 0
		length = 0; v_lengths = []
		for v in vectors:
			length += len(v) - (self.numNotSpectra + 1); v_lengths.append(len(v) - (self.numNotSpectra + 1))
		length /= 4
		
		if (self.use_gpu):
			padded_length = (length / self.local_workgroup_size + 1) * self.local_workgroup_size
			grid_x = padded_length / self.local_workgroup_size
		else: padded_length = length

		eps = numpy.zeros(padded_length, dtype=numpy.float32)
		nps = numpy.zeros(padded_length, dtype=numpy.float32)
		efs = numpy.zeros(padded_length, dtype=numpy.float32)
		nfs = numpy.zeros(padded_length, dtype=numpy.float32)

		index = 0
		for v in vectors:
			for i in range(len(v) - (self.numNotSpectra + 1)):#length of spectra vector
				if ((i+1) % 4 == 1):
					eps[index] = numpy.float32(v[i+1])	
				elif ((i+1) % 4 == 2):
					nps[index] = numpy.float32(v[i+1])
				elif ((i+1) % 4 == 3):
					efs[index] = numpy.float32(v[i+1])
				else:	
					nfs[index] = numpy.float32(v[i+1])
					index += 1
		
		if (self.use_gpu):
			sps = numpy.array([0.0] * padded_length, dtype=numpy.float32)
			cudaFileDir = os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir), "sbfl_metrics")
			kernel_code = "".join(open(os.path.join(cudaFileDir, "kernel_template_metrics.cuda"), "r").readlines())
			mod = SourceModule(kernel_code)
			metric_kernel = mod.get_function(target_metric)
			metric_kernel(drv.In(eps), drv.In(efs), drv.In(nps), drv.In(nfs), drv.Out(sps), block=(512,1,1), grid=(grid_x, 1))
			new_sps = sps[:length]
		else:
			target_method = getattr(comp_sbfl_metrics, target_metric)
			new_sps = target_method(eps, efs, nps, nfs, length)

		sps_per_method = numpy.zeros(len(vectors), dtype=numpy.float32)
		
		index = 0
		#select representative value for the method
		#print "total length : " + str(length) + "," + str(len(v_lengths)) # logging
		min_val = None; max_val = None; total_vals = []; valid_vals = []
		exception_index = []
		for i in range(len(v_lengths)):
			values = list(numpy.float32(new_sps[index + i]) for i in range(v_lengths[i]/4))
			val = numpy.max(values)
			
			if (numpy.isnan(val) or not numpy.isfinite(val)):
				cand = []
				for v in values:
					if (not numpy.isnan(v) and numpy.isfinite(val)):
						cand.append(v)
				if(len(cand) == 0):
					val = numpy.float32('NaN') if self.use_norm == 1 else numpy.float32(0)			
				else:
					val = numpy.max(cand)
					if self.use_norm == 1:
						valid_vals.append(val)
			else:
				if self.use_norm == 1:
					valid_vals.append(val)
			
			index += v_lengths[i]/4
			sps_per_method[i] = val

		#normalizing process: use only if use_norm == 1
		if self.use_norm == 1:
			min_val = numpy.min(valid_vals); max_val = numpy.max(valid_vals)
			range_val = max_val - min_val
			#print "range_val " + target_metric + ":"  + str(range_val) + ", " + str(max_val) + "," + str(min_val) # logging
			#For using raw score
			if (range_val == 0):
				for i in range(len(v_lengths)):
					sps_per_method[i] = numpy.float32(1)/numpy.float32(len(v_lengths))
			else:
				for i in range(len(v_lengths)):
					if (numpy.isnan(sps_per_method[i])):
						sps_per_method[i] = 0.0
					else:
						sps_per_method[i] = (sps_per_method[i] - min_val) / range_val
		

		return sps_per_method

	def normalise(self, vectors):
		min_val = numpy.min(vectors)
		max_val = numpy.max(vectors)
		range_val = max_val - min_val
		if(range_val == 0):#all of them are 0, this is needed, because without them 'nan' is occurred
			for i in range(len(vectors)):
				vectors[i] = numpy.float32(1)/numpy.float32(len(vectors))
		else:
			for i in range(len(vectors)):
				vectors[i] = (vectors[i] - min_val) / range_val
		return vectors

	#core function
	def core(self, dataset, data_name):
		(fault_method, vectors) = dataset	
		vectors = self.exclude(dataset)	
		#change it to new feature vectors
		dataset = (fault_method, vectors)
		fault_index = find_fault.find_faulty_index(dataset, data_name)
		if (not bool(fault_index)):
			print "Excluded:"
			data_name = re.search("([^/]*)\.SP\.dat", data_name).group(1)
			print "\t" + data_name + " is excluded because the faulty method(s) has(have) not been recorded as covered"
			f = open(os.path.join(os.path.join(parent_dir,"fault_list"), "Excluded.list"), "a+")
			f.seek(0)
			lines = f.read().splitlines()
			if (not data_name in lines):
				f.write(data_name + "\n")
			f.close()
			return None
		
		length = 0
		length = len(vectors)
		
		v_lengths = []
		for v in vectors:
			v_lengths.append(len(v) - (self.numNotSpectra + 1))
		
		#caculate metrics
		ochiais = self.metric("ochiai", dataset)
		jaccards = self.metric("jaccard", dataset)
		gp13s = self.metric("gp13", dataset)
		wong1s = self.metric("wong1", dataset)
		wong2s = self.metric("wong2", dataset)
		wong3s = self.metric("wong3", dataset)
		tarantulas = self.metric("tarantula", dataset)
		amples = self.metric("ample", dataset)
		RussellRaos = self.metric("RussellRao", dataset)
		SorensenDices = self.metric("SorensenDice", dataset)
		Kulczynski1s = self.metric("Kulczynski1", dataset)
		SimpleMatchings = self.metric("SimpleMatching", dataset)
		M1s = self.metric("M1", dataset)
		RogersTanimotos = self.metric("RogersTanimoto", dataset)
		Hammings = self.metric("Hamming", dataset)
		Ochiai2s = self.metric("Ochiai2", dataset)
		Hamanns = self.metric("Hamann", dataset)
		dices = self.metric("dice", dataset)
		Kulczynski2s = self.metric("Kulczynski2", dataset)
		Sokals = self.metric("Sokal", dataset)
		M2s = self.metric("M2", dataset)
		Goodmans = self.metric("Goodman", dataset)
		Euclids = self.metric("Euclid", dataset)
		Anderbergs = self.metric("Anderberg", dataset)
		Zoltars = self.metric("Zoltar", dataset)
		ER1as = self.metric("ER1a", dataset)
		ER1bs = self.metric("ER1b", dataset)
		ER5as = self.metric("ER5a", dataset)
		ER5bs = self.metric("ER5b", dataset)
		ER5cs = self.metric("ER5c", dataset)
		gp02s = self.metric("gp02", dataset)
		gp03s = self.metric("gp03", dataset)
		gp19s = self.metric("gp19", dataset)

		churns = [numpy.float32(v[-7]) for v in vectors]
		churns = self.normalise(churns)
		max_ages = [numpy.float32(v[-6]) for v in vectors]
		max_ages = self.normalise(max_ages)
		min_ages = [numpy.float32(v[-5]) for v in vectors]
		min_ages = self.normalise(min_ages)
		num_args = [numpy.float32(v[-4]) for v in vectors]
		num_args = self.normalise(num_args)
		num_vars = [numpy.float32(v[-3]) for v in vectors]
		num_vars = self.normalise(num_vars)
		b_lengths = [numpy.float32(v[-2]) for v in vectors]
		b_lengths = self.normalise(b_lengths)
		locs = [numpy.float32(v[-1]) for v in vectors]
		locs = self.normalise(locs)
	
		#make new vector
		final_vectors = []
		for i in range(len(vectors)):
			v = [vectors[i][0],] #method name
			v.extend([ochiais[i], jaccards[i], gp13s[i], wong1s[i], wong2s[i], wong3s[i], tarantulas[i], amples[i], RussellRaos[i], SorensenDices[i], Kulczynski1s[i], SimpleMatchings[i], M1s[i], RogersTanimotos[i], Hammings[i], Ochiai2s[i], Hamanns[i], dices[i], Kulczynski2s[i], Sokals[i], M2s[i], Goodmans[i], Euclids[i], Anderbergs[i], Zoltars[i], ER1as[i], ER1bs[i], ER5as[i], ER5bs[i], ER5cs[i], gp02s[i], gp03s[i], gp19s[i]]) #spectra scores
			v.extend([churns[i], max_ages[i], min_ages[i], num_args[i], num_vars[i], b_lengths[i], locs[i]])
			final_vectors.append(v)
	
		revised_vectors = (fault_index, final_vectors)
		
		return revised_vectors

	#the main function
	def main(self, data_dir, dest, afile, resultFile):
		if (afile is not None):
			datafiles = [os.path.join(data_dir, afile)]
			target_files = [os.path.join(dest, resultFile)]
		else:
			datafiles = [os.path.join(data_dir, x) for x in os.listdir(data_dir)]
			target_files = [os.path.join(dest, x) for x in os.listdir(data_dir)]

		for i in range(len(datafiles)):
			dataset = pickle.load(open(datafiles[i], "r"))
			print datafiles[i]
			revised_dataset = tool.core(dataset, datafiles[i])
			if (revised_dataset is not None):
				pickle.dump(revised_dataset, open(target_files[i], "w"))
				print "\tFile Location: " + target_files[i]

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("-datadir", action = "store", default = None)
	parser.add_argument("-usenorm", action = "store", default = 1, type = int)
	parser.add_argument("-dest", action = "store", default = "Final_data_w_norm")
	parser.add_argument("-afile", action = "store", default = None, help = "given if target is a single file else None")
	parser.add_argument("-resultFile", action = "store", default = None, help = "name of the final data file")
	parser.add_argument("-usegpu", action = "store", default = 0, type = int)
	args = parser.parse_args()
#	parser_check.argument_check(parser, args_name_list = ["datadir", "dest", "afile", "resultFile"], args_val_list = [args.datadir, args.dest, args.afile, args.resultFile])
	if (args.usegpu):
		import pycuda.autoinit
		import pycuda.driver as drv
		from pycuda.compiler import SourceModule
	
	tool = Tool(args.usenorm, args.usegpu)
	tool.main(args.datadir, args.dest, args.afile, args.resultFile)
