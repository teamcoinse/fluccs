if [ ! -d "bcel/Project_Jar" ]
then
	7zr x -obcel/ bcel/Project_Jar.7z
fi

for projectName in Lang Math Time Closure Chart Mockito
do
	if [ ! -d "bcel/output/$projectName" ]
	then
		7zr x -obcel/output/ bcel/output/$projectName.7z
	fi
done

if [ ! -d "method_stmt" ]
then
	mkdir method_stmt
	for projectName in Lang Math Time Closure Chart Mockito
	do
		mkdir "method_stmt/$projectName"
		mkdir "method_stmt/$projectName/Methods"
	done
fi

cd CodeAndChangeM
mvn clean install
cd ../

cp defects4j-fluccs ../
cp modifiedD4js/Coverage.pm ../../core/
cp modifiedD4js/d4j-coverage ../d4j/
cp modifiedD4js/Project.pm ../../core/
