#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------
"""
methods of target sbfl risk evaluation formulas
"""
import math, numpy

def gp_add(a, b): return a+b
def gp_sub(a, b): return a-b
def gp_mul(a, b): return a*b
def gp_div(a, b): return 1 if b == 0 else a/b
def gp_sqrt(a): return math.sqrt(abs(a))
def gp_neg(a): return -a
def gp_iftelse(a, b, c, d): return c if a < b else d

def ochiai(ep, ef, np, nf, length):
	susp = numpy.array([0.0] * length, dtype=numpy.float32) 
	for i in range(length):	
		#susp[i] = ef[i] / math.sqrt((ef[i] + nf[i]) * (ef[i] + ep[i]))
		susp[i] = gp_div(ef[i], math.sqrt((ef[i] + nf[i]) * (ef[i] + ep[i])))
	return susp

def jaccard(ep, ef, np, nf, length):
	susp = numpy.array([0.0] * length, dtype=numpy.float32)
	for i in range(length):
		if(ef[i] + nf[i] + ep[i] == 0):
			susp[i] = 0.0
		else: 
			susp[i] = ef[i] / (ef[i] + nf[i] + ep[i])
	return susp

def wong1(ep, ef, np, nf, length):
	susp = numpy.array([0.0] * length, dtype=numpy.float32)
	for i in range(length):
		susp[i] = ef[i]
	return susp

def wong2(ep, ef, np, nf, length):
	susp = numpy.array([0.0] * length, dtype=numpy.float32)
	for i in range(length):
		susp[i] = ef[i] - ep[i];
	return susp

def wong3(ep, ef, np, nf, length):
	susp = numpy.array([0.0] * length, dtype=numpy.float32)
	for i in range(length):
		h = 0.0
		if(ep[i] <= 2):
			h = ep[i]
		elif (ep[i] > 2 and ep[i] <= 10):
			h = 2 + 0.1 * (ep[i] - 2)
		else:
			h = 2.8 + 0.001 * (ep[i] - 10)
		susp[i] = ef[i] - h	
	return susp

def tarantula(ep, ef, np, nf, length):
	susp = numpy.array([0.0] * length, dtype=numpy.float32)
	for i in range(length):
		ff = ef[i]; pp = ep[i]
		if(ef[i] + nf[i] > 0): 
			ff /= (ef[i] + nf[i])
		if(ep[i] + np[i] > 0): 
			pp /= (ep[i] + np[i])
		if(pp + ff > 0): 
			#susp[i] = ff / (pp + ff)
			susp[i] = gp_div(ff, (pp + ff))
		else: susp[i] = 0.0
	return susp

def ample(ep, ef, np, nf, length):
	susp = numpy.array([0.0] * length, dtype=numpy.float32)
	for i in range(length):
		susp[i] = abs(ef[i]/(ef[i] + nf[i]) - ep[i]/(ep[i] + np[i]))
	return susp

def gp13(ep, ef, np, nf, length):
	susp = numpy.array([0.0] * length, dtype=numpy.float32)
	for i in range(length):
		susp[i] = ef[i] * (1 + gp_div(1, (2 * ep[i] + ef[i])))
	return susp

def RussellRao(ep, ef, np, nf, length):
	susp = numpy.array([0.0] * length, dtype=numpy.float32)
	for i in range(length):
		susp[i] = ef[i]/(ef[i] + ep[i] + nf[i] + np[i])
	return susp

def SorensenDice(ep, ef, np, nf, length):
	susp = numpy.array([0.0] * length, dtype=numpy.float32)
	for i in range(length):
		susp[i] = 2.0*ef[i]/(2.0*ef[i] + ep[i] + nf[i])
	return susp

def Kulczynski1(ep, ef, np, nf, length):
	susp = numpy.array([0.0] * length, dtype=numpy.float32)
	for i in range(length):
		#susp[i] = ef[i]/(nf[i] + ep[i])
		susp[i] = gp_div(ef[i], (nf[i] + ep[i]))
	return susp

def SimpleMatching(ep, ef, np, nf, length):
	susp = numpy.array([0.0] * length, dtype=numpy.float32)
	for i in range(length):
		susp[i] = (ef[i] + np[i])/(ef[i] + ep[i] + nf[i] + np[i])
	return susp

def M1(ep, ef, np, nf, length):
	susp = numpy.array([0.0] * length, dtype=numpy.float32)
	for i in range(length):
		#susp[i] = (ef[i] + np[i])/(nf[i] + ep[i])
		susp[i] = gp_div((ef[i] + np[i]), (nf[i] + ep[i]))
	return susp

def RogersTanimoto(ep, ef, np, nf, length):
	susp = numpy.array([0.0] * length, dtype=numpy.float32)
	for i in range(length):
		susp[i] = (ef[i] + np[i])/(ef[i] + np[i] + 2.0*nf[i] + 2.0*ep[i])
	return susp

def Hamming(ep, ef, np, nf, length):
	susp = numpy.array([0.0] * length, dtype=numpy.float32)
	for i in range(length):
		susp[i] = ef[i] + np[i]
	return susp

def Ochiai2(ep, ef, np, nf, length):
	susp = numpy.array([0.0] * length, dtype=numpy.float32)
	for i in range(length):
		#susp[i] = ef[i]*np[i] / math.sqrt((ef[i] + ep[i]) * (nf[i] + np[i]) * (ef[i] + np[i]) * (ep[i] + nf[i]))
		susp[i] = gp_div(ef[i]*np[i], math.sqrt((ef[i] + ep[i]) * (nf[i] + np[i]) * (ef[i] + np[i]) * (ep[i] + nf[i])))
	return susp

def Hamann(ep, ef, np, nf, length):
	susp = numpy.array([0.0] * length, dtype=numpy.float32)
	for i in range(length):
		susp[i] = (ef[i] + np[i] - ep[i] - nf[i])/(ef[i] + ep[i] + nf[i] + np[i])
	return susp

def dice(ep, ef, np, nf, length):
	susp = numpy.array([0.0] * length, dtype=numpy.float32)
	for i in range(length):
		susp[i] = 2.0 * ef[i]/(ef[i] + ep[i] + nf[i])
	return susp

def Kulczynski2(ep, ef, np, nf, length):
	susp = numpy.array([0.0] * length, dtype=numpy.float32)
	for i in range(length):
		susp[i] = (1.0/2.0) * (ef[i]/(ef[i] + nf[i]) + gp_div(ef[i],(ef[i] + ep[i])))
	return susp

def Sokal(ep, ef, np, nf, length):
	susp = numpy.array([0.0] * length, dtype=numpy.float32)
	for i in range(length):
		susp[i] = (2.0*ef[i] + 2.0*np[i])/(2.0*ef[i] + 2.0*np[i] + nf[i] + ep[i])
	return susp

def M2(ep, ef, np, nf, length):
	susp = numpy.array([0.0] * length, dtype=numpy.float32)
	for i in range(length):
		susp[i] = ef[i]/(ef[i] + np[i] + 2.0*nf[i] + 2.0*ep[i])
	return susp

def Goodman(ep, ef, np, nf, length):
	susp = numpy.array([0.0] * length, dtype=numpy.float32)
	for i in range(length):
		susp[i] = (2.0*ef[i] - nf[i] - ep[i])/(2.0*ef[i] + nf[i] + ep[i])
	return susp

def Euclid(ep, ef, np, nf, length):
	susp = numpy.array([0.0] * length, dtype=numpy.float32)
	for i in range(length):
		susp[i] = math.sqrt(ef[i] + np[i])
	return susp

def Anderberg(ep, ef, np, nf, length):
	susp = numpy.array([0.0] * length, dtype=numpy.float32)
	for i in range(length):
		susp[i] = ef[i]/(ef[i] + 2.0*ep[i] + 2.0*nf[i])
	return susp

def Zoltar(ep, ef, np, nf, length):
	susp = numpy.array([0.0] * length, dtype=numpy.float32)
	for i in range(length):
		susp[i] = ef[i]/(ef[i] + ep[i] + nf[i] + gp_div(10000.0*nf[i]*ep[i],ef[i]))
	return susp

def ER1a(ep, ef, np, nf, length):
	susp = numpy.array([0.0] * length, dtype=numpy.float32)
	for i in range(length):
		if (ef[i] < (ef[i] + nf[i])):
			susp[i] = -1
		else:
			susp[i] = np[i]
	return susp

def ER1b(ep, ef, np, nf, length):
	susp = numpy.array([0.0] * length, dtype=numpy.float32)
	for i in range(length):
		susp[i] = ef[i] - ep[i]/(ep[i] + np[i] + 1)
	return susp

def ER5a(ep, ef, np, nf, length):
	susp = numpy.array([0.0] * length, dtype=numpy.float32)
	for i in range(length):
		susp[i] = ef[i]
	return susp

def ER5b(ep, ef, np, nf, length):
	susp = numpy.array([0.0] * length, dtype=numpy.float32)
	for i in range(length):
		susp[i] = ef[i]/(ef[i] + nf[i] + ep[i] + np[i])
	return susp

def ER5c(ep, ef, np, nf, length):
	susp = numpy.array([0.0] * length, dtype=numpy.float32)
	for i in range(length):
		if (ef[i] < ef[i] + nf[i]):
			susp[i] = 0.0
		else:
			susp[i] = 1.0
	return susp
	
def gp02(ep, ef, np, nf, length):
	susp = numpy.array([0.0] * length, dtype=numpy.float32)
	for i in range(length):
		susp[i] = 2.0*(ef[i] + math.sqrt(ep[i] + np[i])) + math.sqrt(ep[i])
	return susp

def gp03(ep, ef, np, nf, length):
	susp = numpy.array([0.0] * length, dtype=numpy.float32)
	for i in range(length):
		susp[i] = math.sqrt(abs(ef[i]*ef[i] - math.sqrt(ep[i])))
	return susp

def gp19(ep, ef, np, nf, length):
	susp = numpy.array([0.0] * length, dtype=numpy.float32)
	for i in range(length):
		susp[i] = ef[i] * math.sqrt(abs(nf[i] - np[i]))
	return susp
