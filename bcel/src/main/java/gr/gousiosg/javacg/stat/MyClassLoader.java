package gr.gousiosg.javacg.stat;
import java.net.URL;
import java.net.URLClassLoader;

public class MyClassLoader extends URLClassLoader{
	public MyClassLoader(URL[] urls){
		super(urls);
	}
	
	public void addURL(URL url){
		super.addURL(url);
	}
}
