/*
 * Copyright (c) 2011 - Georgios Gousios <gousiosg@gmail.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package gr.gousiosg.javacg.stat;

import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.ConstantPushInstruction;
import org.apache.bcel.generic.EmptyVisitor;
import org.apache.bcel.generic.INVOKEINTERFACE;
import org.apache.bcel.generic.INVOKESPECIAL;
import org.apache.bcel.generic.INVOKESTATIC;
import org.apache.bcel.generic.INVOKEVIRTUAL;

import org.apache.bcel.classfile.ClassParser;

import org.apache.bcel.generic.Instruction;
import org.apache.bcel.generic.InstructionConstants;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.ReturnInstruction;

import org.apache.bcel.generic.Type;

import java.util.ArrayList;
import java.util.List;
import org.apache.bcel.classfile.Method;
import java.lang.reflect.*;

import java.util.HashMap;
import java.util.Map;
/**
 * The simplest of method visitors, prints any invoked method
 * signature for all method invocations.
 * 
 * Class copied with modifications from CJKM: http://www.spinellis.gr/sw/ckjm/
 */
public class MethodVisitor extends EmptyVisitor {

    JavaClass visitedClass;
    private MethodGen mg;
    private ConstantPoolGen cp;
    private String format;

    //To remove reflection case
    private ArrayList<String> global_class_list;
    private ArrayList<String> global_method_list;
    private MyClassLoader my_loader;
    private String prefix;
    ///
    private String zipfile;

    public MethodVisitor(MethodGen m, JavaClass jc, ArrayList<String> global_class_list, ArrayList<String> global_method_list, 
        MyClassLoader my_loader, String prefix, String zipfile) {

        visitedClass = jc;
        mg = m;
        cp = mg.getConstantPool();

        this.global_class_list = global_class_list;
        this.global_method_list = global_method_list;
        this.my_loader = my_loader;
        this.prefix = prefix;
        ///
        this.zipfile = zipfile;

        String arguments = "<";
        int count = 1;

        for (Type type : mg.getArgumentTypes()){
            if (count == mg.getArgumentTypes().length){
                arguments += type.toString();
            }
            else{
                arguments += type.toString() + ";";
                count ++;
            }
        }


        arguments += ">";
        format = visitedClass.getClassName() + "," + mg.getName() 
            + "," + arguments + "," + "%s,%s,%s";
    }


    public void start() {
        if (mg.isAbstract() || mg.isNative())
            return;
        for (InstructionHandle ih = mg.getInstructionList().getStart(); 
            ih != null; ih = ih.getNext()) {
            Instruction i = ih.getInstruction();
            
            if (!visitInstruction(i)){
                i.accept(this);
            }
        }
    }

    private boolean visitInstruction(Instruction i) {
        short opcode = i.getOpcode();

        return ((InstructionConstants.INSTRUCTIONS[opcode] != null)
                && !(i instanceof ConstantPushInstruction) 
                && !(i instanceof ReturnInstruction));
    }


    public String args_return(Method m){
        String arguments = "<";
        int  count = 1;
        for (Type type: m.getArgumentTypes()){
            if (count == m.getArgumentTypes().length){
                arguments += type.toString();
            }
            else{
                arguments += type.toString() + ";";
            }
        }
        arguments += ">";
        return arguments;
    }


    @Override
    public void visitINVOKEVIRTUAL(INVOKEVIRTUAL i) {
        //to check reflection
        if (!i.getMethodName(cp).contains("access$")){
            String class_name = i.getReferenceType(cp).toString();
        	String target = class_name + "$" + i.getMethodName(cp);

            String methodname = i.getMethodName(cp);
            String methodsig = i.getSignature(cp);
            String parentClass = i.getReferenceType(cp).toString();

        	String arguments = "<";
        	int count = 1;
        	for (Type type : i.getArgumentTypes(cp)){
            		if (count == i.getArgumentTypes(cp).length){
                		arguments += type.toString();
            		}
            		else{
                		arguments += type.toString() + ";";
                		count ++;
            		}
        	}

        	arguments = arguments.substring(0,arguments.length());
        	arguments += ">";

        	target = target + "$" + arguments;

    		if (global_method_list.contains(target)){
                System.out.println(String.format(format, class_name, i.getMethodName(cp), arguments));
            }
            else{//first search for parents
                if (class_name.contains(prefix)){
				    if(class_name.contains("[]")){
					   int pos = class_name.lastIndexOf('[');
					   class_name = class_name.substring(0, pos); 
				    }
				    try{
                        int find = 0;
                        Class parent = my_loader.loadClass(class_name);
                        for (parent = parent.getSuperclass(); parent != null && find == 0 && parent.getName().contains(prefix); parent = parent.getSuperclass()){
                            String parent_classfile = parent.getName().replace(".", "/") + ".class";

                            if (global_class_list.contains(parent_classfile)){
                                JavaClass jc = new ClassParser(zipfile, parent_classfile).parse();
                                Method[] methods = jc.getMethods();
                            
                                String comp_method = i.getMethodName(cp) + "$" + arguments;
                                for (Method method: methods){
                                    String declared_method = method.getName() + "$" + args_return(method);
                                    if (declared_method.equals(comp_method)){
                                        find = 1;
                                        System.out.println(String.format(format, parent.getName(), method.getName(), arguments));
                                        break;
                                    }
                                }
                            }
					   }
				    } catch(Exception e){
					   e.printStackTrace();
				    }
                }
            }
        }
        
    }

    @Override
    public void visitINVOKEINTERFACE(INVOKEINTERFACE i) {
	if (!i.getMethodName(cp).contains("access$")){
        	String class_name = i.getReferenceType(cp).toString();
        	String target = class_name + "$" + i.getMethodName(cp);
        	String arguments = "<";
        	int count = 1;

        	for (Type type : i.getArgumentTypes(cp)){
            		if (count == i.getArgumentTypes(cp).length){
                		arguments += type.toString();
            		}
            		else{
                		arguments += type.toString() + ";";
                		count ++;
            		}
        	}

        	arguments = arguments.substring(0,arguments.length());
        	arguments += ">";

        	target = target + "$" + arguments;

        	if (global_method_list.contains(target)){
                	System.out.println(String.format(format, class_name, i.getMethodName(cp), arguments));
        	}
        	else{//first search for parents
			if (class_name.contains(prefix)){
				if(class_name.contains("[]")){
                                	int pos = class_name.lastIndexOf('[');
                                	class_name = class_name.substring(0, pos);
                        	}
				try{
                			Class parent = my_loader.loadClass(class_name);
                			int find = 0;
                			for (parent = parent.getSuperclass(); parent != null && find == 0 && parent.getName().contains(prefix); parent = parent.getSuperclass()){
                                String parent_classfile = parent.getName().replace(".", "/") + ".class";

                                if (global_class_list.contains(parent_classfile)){
                                    JavaClass jc = new ClassParser(zipfile, parent_classfile).parse();
                                    Method[] methods = jc.getMethods();
                            
                                    String comp_method = i.getMethodName(cp) + "$" + arguments;
                                    for (Method method: methods){
                                        String declared_method = method.getName() + "$" + args_return(method);
                                        if (declared_method.equals(comp_method)){
                                            find = 1;
                                            System.out.println(String.format(format, parent.getName(), method.getName(), arguments));
                                            break;
                                        }
                                    }
                                }
                			}	
				}	
                		catch(Exception e){
                        		e.printStackTrace();
                		}
			}

        	}
	}
    }
    
    @Override
    public void visitINVOKESPECIAL(INVOKESPECIAL i) {
        //System.out.println("+++++visitINVOKESPECIAL");
        if (!i.getMethodName(cp).contains("access$")){
        	String class_name = i.getReferenceType(cp).toString();
        	String target = class_name + "$" + i.getMethodName(cp);
        	String arguments = "<";
        	int count = 1;

        	for (Type type : i.getArgumentTypes(cp)){
            		if (count == i.getArgumentTypes(cp).length){
                		arguments += type.toString();
            		}
            		else{
                		arguments += type.toString() + ";";
                		count ++;
            		}
        	}

        	arguments = arguments.substring(0,arguments.length());
        	arguments += ">";

        	target = target + "$" + arguments;

            //System.out.println(target);
        	if (global_method_list.contains(target)){
                System.out.println(String.format(format, class_name, i.getMethodName(cp), arguments));
        	}
        	else{//first search for parents
                //System.out.println("Not in global method list");
                //System.out.println(class_name);
                //System.out.println(prefix);
                if (class_name.contains(prefix)){
                    if(class_name.contains("[]")){
                        //System.out.println("---HERE");
                        int pos = class_name.lastIndexOf('[');
                        class_name = class_name.substring(0, pos);
                    }
				    try{
                        //System.out.println("---Contain prefix");
                		Class parent = my_loader.loadClass(class_name);
                        //System.out.println(class_name);
                		int find = 0;
                		for (parent = parent.getSuperclass(); parent != null && find == 0 && parent.getName().contains(prefix); parent = parent.getSuperclass()){
                            //System.out.println("**===========");

                            String parent_classfile = parent.getName().replace(".", "/") + ".class";
                            //System.out.println(parent_classfile); 

                            //System.out.println("+++++");
                            //System.out.println(parent_classfile);
                            //System.out.println("+++++");

                            if (global_class_list.contains(parent_classfile)){
                                JavaClass jc = new ClassParser(zipfile, parent_classfile).parse();
                                Method[] methods = jc.getMethods();
                            
                                String comp_method = i.getMethodName(cp) + "$" + arguments;
                                //System.out.println(comp_method);
                                for (Method method: methods){
                                    String declared_method = method.getName() + "$" + args_return(method);
                                    if (declared_method.equals(comp_method)){
                                        find = 1;
                                        System.out.println(String.format(format, parent.getName(), method.getName(), arguments));
                                        break;
                                    }
                                }                                    


                        		/*	java.lang.reflect.Method[] declareds = parent.getDeclaredMethods();
                        			String comp_method = i.getMethodName(cp) + "$" + arguments;
                        			for (java.lang.reflect.Method m : declareds){
                                			String declared_method = m.getName() + "$" + args_return(m);
                                			if (declared_method.equals(comp_method)){
                                        			find = 1;
                                        			System.out.println(String.format(format, parent.getName(), m.getName(), arguments));
                                        			break;
                                			}
                        			}*/
                            }
                		}
                	}catch(Exception e){
                        e.printStackTrace();
                	}
                }
                //System.out.println("DOES NOT CONTAIN PREFIX");
            }
        }
        //System.out.println("+++++OUT visitINVOKESPECIAL");
    }

    @Override
    public void visitINVOKESTATIC(INVOKESTATIC i) {
        //System.out.println("+++++visitINVOKESTATIC");
        //to check reflection
	if (!i.getMethodName(cp).contains("access$")){
        	String class_name = i.getReferenceType(cp).toString();
        	String target = class_name + "$" + i.getMethodName(cp);
            //System.out.println(i.getMethodName(cp));
        	String arguments = "<";
        	int count = 1;

        	for (Type type : i.getArgumentTypes(cp)){
            		if (count == i.getArgumentTypes(cp).length){
                		arguments += type.toString();
            		}
            		else{
                        arguments += type.toString() + ";";
                        count ++;
            		}
        	}

        	//arguments = arguments.substring(0,arguments.length());
        	arguments += ">";

        	target = target + "$" + arguments;
            //System.out.println(target);

        	if (global_method_list.contains(target)){
                    //System.out.println(target + " contained in global_method_list");
                	System.out.println(String.format(format, class_name, i.getMethodName(cp), arguments));
        	}
        	else{//first search for parents
                //System.out.println(target + " is not contained in global_method_list");
                if (class_name.contains(prefix)){
                    //System.out.println("contain prefix");
                    if(class_name.contains("[]")){
                        int pos = class_name.lastIndexOf('[');
                        class_name = class_name.substring(0, pos);
                    }

				    try{
                        //System.out.println(class_name);

                		Class parent = my_loader.loadClass(class_name);

                		int find = 0;
                		for (parent = parent.getSuperclass(); parent != null && find == 0 
                            && parent.getName().contains(prefix); parent = parent.getSuperclass()){
                            
                            //System.out.println("**===========");
                            String parent_classfile = parent.getName().replace(".", "/") + ".class";
                            //System.out.println(parent_classfile); 

                            //System.out.println("====" + parent_classfile);
                            if (global_class_list.contains(parent_classfile)){
                                JavaClass jc = new ClassParser(zipfile, parent_classfile).parse();
                                Method[] methods = jc.getMethods();
                            
                                String comp_method = i.getMethodName(cp) + "$" + arguments;
                                //System.out.println(comp_method);
                                for (Method method: methods){
                                    String declared_method = method.getName() + "$" + args_return(method);
                                    if (declared_method.equals(comp_method)){
                                        find = 1;
                                        System.out.println(String.format(format, parent.getName(), method.getName(), arguments));
                                        break;
                                    }
                                }

                                /*

                                java.lang.reflect.Method[] declareds = parent.getDeclaredMethods();

                                System.out.println("===========");
                                //}catch(Exception e){
                                //    System.out.println("error while declaring methods");
                                 //e.printStackTrace();
                                //}
                            
                    		 String comp_method = i.getMethodName(cp) + "$" + arguments;

                             System.out.println(comp_method);
                   			  for (java.lang.reflect.Method m : declareds){
                    			String declared_method = m.getName() + "$" + args_return(m);
                                if (declared_method.equals(comp_method)){
                                    find = 1;
                                    System.out.println(String.format(format, parent.getName(), m.getName(), arguments));
                                    break;
                                }
                        	}*/
                            }
                        }        		
				    }catch(Exception e){
                        e.printStackTrace();
                	}
                }
        	}
            //System.out.println("**************************");
        }
    }
}
