/*
 * Copyright (c) 2011 - Georgios Gousios <gousiosg@gmail.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package gr.gousiosg.javacg.stat;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.apache.bcel.classfile.ClassParser;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import java.lang.Class;
import org.apache.bcel.util.ClassPath;

import java.lang.ClassLoader;
import java.net.URL;
import java.net.URLClassLoader;
/**
 * Constructs a callgraph out of a JAR archive. Can combine multiple archives
 * into a single call graph.
 * 
 * @author Georgios Gousios <gousiosg@gmail.com>
 * 
 */


public class JCallGraph {

    public static void main(String[] args) {
        ClassParser cp;
        URLClassLoader loader = (URLClassLoader)ClassLoader.getSystemClassLoader();
        MyClassLoader my_loader = new MyClassLoader(loader.getURLs());
        MyClassLoader my_loader_all = new MyClassLoader(loader.getURLs());

        URL url, url_all;
        String prefix = args[1];
	    try {
            url = new URL("jar:file:"+args[0]+"!/");
            url_all = new URL("jar:file:" + args[2] + "!/");

            my_loader.addURL(url);
            my_loader_all.addURL(url_all);

            File f = new File(args[0]);

            if (!f.exists()) {
                System.err.println("Jar file " + args[0] + " does not exist");
            }
                
            JarFile jar = new JarFile(f);

            //Preprocess to deal with reflection case 
            //Create global_method_list
            ArrayList<String> global_method_list = new ArrayList<String>();
            ArrayList<String> global_class_list = new ArrayList<String>();

            Enumeration<JarEntry> pre_process_entries = jar.entries();

            while (pre_process_entries.hasMoreElements()) {
                JarEntry pre_process_entry = pre_process_entries.nextElement();
                if (pre_process_entry.isDirectory())
                    continue;

                if (!pre_process_entry.getName().endsWith(".class"))
                    continue;
                global_class_list.add(pre_process_entry.getName());
                //wrap class that parses a given Java.class file: String zip_file(tar), String file_name
                cp = new ClassParser(args[0], pre_process_entry.getName());
                //parse the given class file and return an object(JavaClass) that represents the contained data
                // i.e. constants, methods, fields, and commands
                ClassVisitor pre_visitor = new ClassVisitor(cp.parse());
                //the number of local variables, number of method arguments, instruction byte length --> computed here
                pre_visitor.pre_process();
                global_method_list.addAll(pre_visitor.global_method_list);
            }

            Enumeration<JarEntry> entries = jar.entries();
            while (entries.hasMoreElements()) {
                JarEntry entry = entries.nextElement();
                if (entry.isDirectory())
                    continue;

                if (!entry.getName().endsWith(".class"))
                    continue;

                cp = new ClassParser(args[0], entry.getName());
                ClassVisitor visitor = new ClassVisitor(cp.parse(), global_class_list, global_method_list, my_loader_all, prefix, args[0]);
                visitor.start();
            }
        } catch (Exception e) {
            System.err.println("Error while processing jar: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
