#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------
"""
retrieve test classes in specified test suites.
"""
import os,sys, re
import subprocess
import argparse

def get_stdout(cmd):
	proc = subprocess.Popen(cmd, stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell = True)
	(std_out, std_err) = proc.communicate()
	if (not (not std_err)): print std_err; sys.exit()
	outs = std_out.split("\n")
	
	return outs

def get_testsuite_list(datadir, testsuite):
	cmd = "ant get-test-home"
	outs = get_stdout(cmd)
	src_home = None
	for out in outs:
		if ("[echo]" in out):
			out = out.rstrip()
			src_home = re.search("\[echo\]\s*(.*)", out).group(1)
			print src_home + "\n"
			break

	if (src_home is None): print "error in retrieving source home\n"; sys.exit()
	filename = os.path.join(datadir, os.path.join(src_home, testsuite.replace(".", "/")))

	cmd = "cat " + filename + ".java";# +  " | grep 'suite.addTest('"
	print cmd + "\n"
	outs = get_stdout(cmd)
	test_list = list()
	package = re.search("(.*)\.[^\.]+", testsuite).group(1)
	for out in outs:
		out = out.rstrip()
		if not out: continue
		if ("addTestSuite(" in out or "addTest(" in out):
			test_list.append(package + "." + re.search("addTest[^(]*\((.*)\.", out).group(1))

	return test_list

def write_tests(resultdir, test_list):
	handler = open(os.path.join(resultdir, "testclasses_list.txt"), "w")
	for test in test_list:
		handler.write(test + "\n")
	handler.close()

def read_testsuites(datadir, filename):
	handler = open(os.path.join(datadir, filename))
	testsuites = list()
	while True:
		line = handler.readline().rstrip()
		if not line: break
		testsuites.append(line)
	return testsuites

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("-datadir", action = "store", default = None)
	parser.add_argument("-filename", action = "store", default = None)
	parser.add_argument("-resultdir", action = "store", default = ".")
	args = parser.parse_args()

	if (args.datadir == None or args.filename == None):
		print args; args.print_help(); sys.exit()
	test_list = list()
	testsuites = read_testsuites(args.datadir, args.filename)
	for testsuite in testsuites:
		test_list.extend(get_testsuite_list(args.datadir, testsuite))
	write_tests(args.resultdir, test_list)	
	
