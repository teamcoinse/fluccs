tar xvf cobertura.tar
build_file="build.xml"
if [ -f "$build_file" ]
then
	mv cobertura/build.xml cobertura/build_support.xml
fi
lib_dir="lib"
if [ -d "$lib_dir" ]
then
	mv cobertura/lib/* $lib_dir/
	rmdir cobertura/lib
fi
mv cobertura/* .
rm -Rf cobertura
