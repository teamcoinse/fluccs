#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------
"""
get revision id for the fault which is specified by pid(project id) and bid(bug id).
"""
import os, sys
import subprocess

def get_buggy_revision(pid, bid):
	D4J_HOME = os.getenv("D4J_HOME")
	D4J_COMMIT_DB = D4J_HOME + "/framework/projects/" + pid + "/commit-db"
	cmd = "grep \"^" + str(bid) +",\" " + D4J_COMMIT_DB
	proc = subprocess.Popen(cmd.rstrip(), stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell = True)
	(std_out, std_err) = proc.communicate()
	if (not (not std_err)): print "problem in executing " + cmd; sys.exit()
	rev_id = std_out.split(',')[1]
	return rev_id
