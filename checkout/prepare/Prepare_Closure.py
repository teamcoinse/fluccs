#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------
"""
project Closure Compiler specific preparation.
"""
import Mod_XML
import os, sys
import xml.etree.ElementTree as ET
import subprocess

def modify_XML(datadir, filename, bid):
	doc = ET.parse(filename)
	(jar_node_name, jarfile_id, new_jarfile_val) = Mod_XML.get_name("Closure", bid)
	target_jar, _ = Mod_XML.find_node(doc, "target", ("name", jar_node_name), doc.getroot())
	jar_nodes = target_jar.findall("jar")
	jarfile_val = jar_nodes[0].attrib[jarfile_id]
	Mod_XML.add_node("copy", add_attribs = [("file", jarfile_val), ("tofile", new_jarfile_val)], parent = target_jar)
	
	doc.write(filename)

#previous version is for 1-30 bugs, now for entire 133 bugs
#1~83 and 107~133 use allclasspath.path for path's refid in build.xml and 84~106 use classpath.path
def prepare(bid, datadir):
	current_dir = os.getcwd()
	exec_dir = os.path.dirname(os.path.abspath(__file__))
	os.chdir(datadir)
	if (bid <= 83 or (bid >= 107 and bid <= 133)):
		#XML process and rewrite build.xml
		modify_XML(datadir, "build.xml", bid)
		#Prepare
		try:
			shellfile = os.path.join(exec_dir, 'useJacoco/prepare_closure_allclasspath.sh')
			ret = subprocess.call([shellfile], shell = True)#ret should be 0
			if (ret): raise ValueError('wrong return value')
		except ValueError as e:
			print("shell script 'prepare_closure_allclasspath.sh' execution error", e)
			os.chdir(current_dir)
			sys.exit()
	elif (bid >= 84 and bid <= 106):#this is where classpath is used
		modify_XML(datadir, "build.xml", bid)
		try:
			shellfile = os.path.join(exec_dir, 'useJacoco/prepare_closure_classpath.sh')
			ret = subprocess.call([shellfile], shell = True)#ret should be 0
			if (ret): raise ValueError('wrong return values') 
		except ValueError as e:
			print("shell script 'prepare_closure_allclasspath.sh' execution error", e)
			os.chdir(current_dir)
			sys.exit()
	else:
		print "Project Closure: wrong bug id: " + str(bid)
		os.chdir(current_dir)
		sys.exit()
	os.chdir(current_dir)
