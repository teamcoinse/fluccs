tar xvf Mockito_1_38.tar
build_file="build.xml"
if [ -f "$build_file" ]
then
	mv 1_38/build.xml 1_38/build_support.xml
fi
mv 1_38/* .
rm -Rf 1_38
mv jacocoant.jar extract_filter.jar filter.jar junit-4.12.jar lib/
ant init
