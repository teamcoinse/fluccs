#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------
"""
project jfreeChart specific preparation.
"""

import Mod_XML
import sys, os
import xml.etree.ElementTree as ET
import subprocess


def modify_XML(datadir, filename, new_filename = None):
	#for chart 1~(26)?
	doc = ET.parse(filename)
	(init_node, _) = Mod_XML.find_node(doc, "property", ("name", "builddir"), doc.getroot())

	compile_node = {"node_name":"target", "identifier":("name", "compile"), "parent":doc.getroot()}
	compile_node_exp = {"node_name":"target", "identifier":("name", "compile-experimental"), "parent":doc.getroot()}
	Mod_XML.delete_node("delete", ("dir", init_node.attrib["value"]), doc = doc, parent = compile_node)
	Mod_XML.delete_node("delete", ("dir", "${basedir}/build"), doc = doc, parent = compile_node_exp)
	#Modify base directory
	Mod_XML.mod_attrib(("basedir", "."), doc = doc, target_node = doc.getroot())

	if (new_filename is None):
		doc.write(filename)
	else:
		doc.write(new_filename)

#INITIAL --> should check for EACH BUG(26) version 
def prepare(bid, datadir):
	current_dir = os.getcwd()
	exec_dir = os.path.dirname(os.path.abspath(__file__))
	os.chdir(datadir)
	if (bid >=1 and bid <= 26):
		modify_XML(datadir, "ant/build.xml", "build_support.xml")
		try:
			shellfile = os.path.join(exec_dir, "useJacoco/prepare_chart_1_26.sh")
			ret = subprocess.call([shellfile], shell = True)
			if (ret): raise ValueError	
		except ValueError as e:
			print ("shell script 'prepare_chart_1_26.sh' execution error", e)
		os.chdir(current_dir)
	else:
		print "Project Chart: wrong bug id: " + str(bid)
		sys.exit()

